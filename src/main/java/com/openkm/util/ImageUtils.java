/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2017 Paco Avila & Josep Llort
 * <p>
 * No bytes were intentionally harmed during the development of this application.
 * <p>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package com.openkm.util;

import com.openkm.bean.ExecutionResult;
import com.openkm.core.Config;
import com.openkm.core.DatabaseException;
import com.openkm.core.MimeTypeConfig;
import freemarker.template.TemplateException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

/**
 * ImageUtil
 *
 * @author jllort
 *
 * @see http://www.thebuzzmedia.com/software/imgscalr-java-image-scaling-library/
 * @see http://marvinproject.sourceforge.net/en/index.html
 */
public class ImageUtils {
	private static Logger log = LoggerFactory.getLogger(ImageUtils.class);

	/**
	 * cloneImage
	 */
	public static BufferedImage clone(BufferedImage source) {
		BufferedImage img = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics g = img.getGraphics();
		g.drawImage(source, 0, 0, null);
		g.dispose();
		return img;
	}

	/**
	 * pointWithinRange
	 */
	public static boolean pointWithinRange(Point p, BufferedImage img) {
		return !(p.x < 0 || p.y < 0 || p.x >= img.getWidth() || p.y >= img.getHeight());
	}

	/**
	 * crop
	 */
	public static byte[] crop(byte[] img, int x, int y, int width, int height) throws RuntimeException {
		log.debug("crop({}, {}, {}, {}, {})", img.length, x, y, width, height);
		ByteArrayInputStream bais = new ByteArrayInputStream(img);
		byte[] imageInByte;

		try {
			BufferedImage image = ImageIO.read(bais);
			BufferedImage croppedImage = image.getSubimage(x, y, width, height);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(croppedImage, "png", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
			IOUtils.closeQuietly(baos);
		} catch (IOException e) {
			log.error(e.getMessage());
			throw new RuntimeException("IOException: " + e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(bais);
		}

		log.debug("crop: {}", imageInByte.length);
		return imageInByte;
	}

	/**
	 * Rotate an image.
	 *
	 * @param input Image to rotate.
	 * @param output Image rotated.
	 * @param angle Rotation angle.
	 */
	public static void rotate(File input, File output, double angle) throws RuntimeException {
		log.debug("rotate({}, {}, {})", input, output, angle);
		String params = "-rotate " + angle + " ${fileIn} ${fileOut}";
		ImageMagickConvert(input, output, params);
	}

	/**
	 * Rotate an image.
	 *
	 * @param img Image to rotate.
	 * @param angle Rotation angle.
	 * @return the image rotated.
	 */
	public static byte[] rotate(byte[] img, double angle) throws RuntimeException {
		log.debug("rotate({}, {})", img.length, angle);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileOutputStream fos = null;
		FileInputStream fis = null;
		byte[] ret = new byte[]{};
		File tmpFileIn = null;

		try {
			// Save to disk
			tmpFileIn = FileUtils.createTempFileFromMime(MimeTypeConfig.MIME_PNG);
			fos = new FileOutputStream(tmpFileIn);
			IOUtils.write(img, fos);
			IOUtils.closeQuietly(fos);

			// Rotate
			rotate(tmpFileIn, tmpFileIn, angle);

			// Read from disk
			fis = new FileInputStream(tmpFileIn);
			IOUtils.copy(fis, baos);
			IOUtils.closeQuietly(fis);
			ret = baos.toByteArray();
		} catch (DatabaseException | IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			FileUtils.deleteQuietly(tmpFileIn);
			IOUtils.closeQuietly(baos);
			IOUtils.closeQuietly(fos);
			IOUtils.closeQuietly(fis);
		}

		log.debug("crop: {}", ret.length);
		return ret;
	}

	/**
	 * Create image thumbnail.
	 */
	public static void createThumbnail(File input, String size, File output) throws RuntimeException {
		log.debug("createThumbnail({}, {}, {})", input, size, output);
		String params = "-thumbnail " + size + " -background white -flatten ${fileIn} ${fileOut}";
		ImageMagickConvert(input, output, params);
	}

	/**
	 * Resize image.
	 */
	public static void resize(File input, String size, File output) throws RuntimeException {
		log.debug("resize({}, {}, {})", input, size, output);
		String params = "-resize " + size + " ${fileIn} ${fileOut}";
		ImageMagickConvert(input, output, params);
	}

	/**
	 * Execute ImageMagick convert with parameters.
	 */
	public static void ImageMagickConvert(File input, File output, String params) {
		ImageMagickConvert(input.getPath(), output.getPath(), params);
	}

	/**
	 * Execute ImageMagick convert with parameters.
	 */
	public static void ImageMagickConvert(String input, String output, String params) {
		log.debug("ImageMagickConvert({}, {}, {})", input, output, params);
		String cmd = null;

		try {
			HashMap<String, Object> hm = new HashMap<>();
			hm.put("fileIn", input);
			hm.put("fileOut", output);
			String tpl = Config.SYSTEM_IMAGEMAGICK_CONVERT + " " + params;
			cmd = TemplateUtils.replace("IMAGE_CONVERT", tpl, hm);
			ExecutionResult er = ExecutionUtils.runCmd(cmd);

			if (er.getExitValue() != 0) {
				throw new RuntimeException(er.getStderr());
			}
		} catch (SecurityException e) {
			throw new RuntimeException("Security exception executing command: " + cmd, e);
		} catch (InterruptedException e) {
			throw new RuntimeException("Interrupted exception executing command: " + cmd, e);
		} catch (IOException e) {
			throw new RuntimeException("IO exception executing command: " + cmd, e);
		} catch (TemplateException e) {
			throw new RuntimeException("Template exception", e);
		}
	}

	/**
     * Execute ghostscript command
     */
    public static void ghostscript(String input, String output, String params) {
        log.debug("ghostscript({}, {}, {})", input, output, params);
        String cmd = null;

        if (params == null || params.isEmpty()) {
            throw new RuntimeException("Ghostcript parameters are empty");
        }

        if (Config.SYSTEM_GHOSTSCRIPT.isEmpty()) {
            throw new RuntimeException(Config.SYSTEM_GHOSTSCRIPT + " not configured");
        }

        try {
            HashMap<String, Object> hm = new HashMap<>();
            hm.put("fileIn", input);
            hm.put("fileOut", output);
            String tpl = Config.SYSTEM_GHOSTSCRIPT + " " + params;
            cmd = TemplateUtils.replace("SYSTEM_GHOSTSCRIPT", tpl, hm);
            ExecutionResult er = ExecutionUtils.runCmd(cmd);

            if (er.getExitValue() != 0) {
                throw new RuntimeException(er.getStderr());
            }
        } catch (SecurityException e) {
            throw new RuntimeException("Security exception executing command: " + cmd, e);
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted exception executing command: " + cmd, e);
        } catch (IOException e) {
            throw new RuntimeException("IO exception executing command: " + cmd, e);
        } catch (TemplateException e) {
            throw new RuntimeException("Template exception", e);
        }
    }

	public static String getFormatMimeType(byte[] img) throws IOException {
		String formatName = getFormatName(img);
		if (formatName.toUpperCase().equals("PNG")) {
			formatName = "image/png";
		} else if (formatName.toUpperCase().equals("JPEG")) {
			formatName = "image/jpeg";
		} else if (formatName.toUpperCase().equals("GIF")) {
			formatName = "image/gif";
		} else if (formatName.toUpperCase().equals("BMP")) {
			formatName = "image/bmp";
		} else if (formatName.toUpperCase().equals("TIF")) {
			formatName = "image/tiff";
		}

		return formatName;
	}
	public static String getFormatName(byte[] img) throws IOException {
		String formatName = "image/tiff";
		ImageInputStream iis = ImageIO.createImageInputStream(new ByteArrayInputStream(img));
		Iterator iter = ImageIO.getImageReaders(iis);
		if (iter.hasNext()) {
			ImageReader reader = (ImageReader) iter.next();
			reader.setInput(iis);
			formatName = reader.getFormatName();
		}

		return formatName;
	}

	public static byte[] custom(byte[] img, String mimeType, String params) throws DatabaseException, IOException {
		if (params != null && !params.isEmpty()) {
			ArrayList list = new ArrayList();
			StringTokenizer st = new StringTokenizer(params, "\t\n\r\f");

			while (st.hasMoreTokens()) {
				String tk = st.nextToken().trim();
				if (tk != null && !tk.isEmpty()) {
					list.add(tk);
				}
			}

			Iterator tk1 = list.iterator();

			while (tk1.hasNext()) {
				String custom = (String) tk1.next();
				ByteArrayInputStream bais = new ByteArrayInputStream(img);
				File tmp = FileUtils.createTempFileFromMime(mimeType);
				File tmpOptimized = FileUtils.createTempFileFromMime(mimeType);
				FileOutputStream fos = null;
				FileInputStream fis = null;
				ByteArrayOutputStream baos = null;

				try {
					fos = new FileOutputStream(tmp);
					IOUtils.copy(bais, fos);
					processCustomCmd(tmp, tmpOptimized, custom);
					fis = new FileInputStream(tmpOptimized);
					BufferedImage optimizedImage = ImageIO.read(fis);
					baos = new ByteArrayOutputStream();
					ImageIO.write(optimizedImage, getFormatName(img), baos);
					baos.flush();
					img = baos.toByteArray();
				} finally {
					IOUtils.closeQuietly(fos);
					IOUtils.closeQuietly(fis);
					IOUtils.closeQuietly(baos);
					FileUtils.deleteQuietly(tmp);
					FileUtils.deleteQuietly(tmpOptimized);
				}
			}
		}

		return img;
	}

	public static void processCustomCmd(String input, String output, String custom) {
		log.debug("processCustomCmd({}, {}, {})", new Object[]{input, output, custom});
		String cmd = null;
		if (custom == null || custom.isEmpty()) {
			custom = "${fileIn} ${fileOut}";
		}

		try {
			HashMap e = new HashMap();
			e.put("fileIn", input);
			e.put("fileOut", output);
			e.put("SYSTEM_IMAGEMAGICK_CONVERT", Config.SYSTEM_IMAGEMAGICK_CONVERT);
			cmd = TemplateUtils.replace("IMAGE_CUSTOM", custom, e);
			ExecutionResult er = ExecutionUtils.runCmd(cmd);
			if (er.getExitValue() != 0) {
				throw new RuntimeException(er.getStderr());
			}
		} catch (SecurityException var6) {
			throw new RuntimeException("Security exception executing command: " + cmd, var6);
		} catch (InterruptedException var7) {
			throw new RuntimeException("Interrupted exception executing command: " + cmd, var7);
		} catch (IOException var8) {
			throw new RuntimeException("IO exception executing command: " + cmd, var8);
		} catch (TemplateException var9) {
			throw new RuntimeException("Template exception", var9);
		}
	}

	public static void processCustomCmd(File input, File output, String custom) {
		processCustomCmd(input.getPath(), output.getPath(), custom);
	}


	public static byte[] convert(byte[] img, String mimeType) {
		return convert(img, mimeType, "${fileIn} ${fileOut}");
	}

	public static byte[] convert(byte[] img, String mimeType, String params) {
		log.debug("convert({}, {}, {})", new Object[]{Integer.valueOf(img.length), mimeType, params});
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileOutputStream fos = null;
		FileInputStream fis = null;
		File tmpFileIn = null;
		File tmpFileOut = null;

		byte[] ret;
		try {
			tmpFileIn = FileUtils.createTempFileFromMime(getFormatMimeType(img));
			tmpFileOut = FileUtils.createTempFileFromMime(mimeType);
			fos = new FileOutputStream(tmpFileIn);
			IOUtils.write(img, fos);
			IOUtils.closeQuietly(fos);
			ImageMagickConvert(tmpFileIn, tmpFileOut, params);
			fis = new FileInputStream(tmpFileOut);
			IOUtils.copy(fis, baos);
			IOUtils.closeQuietly(fis);
			ret = baos.toByteArray();
		} catch (DatabaseException var14) {
			throw new RuntimeException(var14.getMessage(), var14);
		} catch (IOException var15) {
			throw new RuntimeException(var15.getMessage(), var15);
		} finally {
			FileUtils.deleteQuietly(tmpFileOut);
			FileUtils.deleteQuietly(tmpFileIn);
			IOUtils.closeQuietly(baos);
			IOUtils.closeQuietly(fos);
			IOUtils.closeQuietly(fis);
		}

		log.debug("convert: {}", Integer.valueOf(ret.length));
		return ret;
	}

}
