//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.util.tags;

import com.openkm.core.DatabaseException;
import com.openkm.dao.LanguageDAO;
import com.openkm.util.OKMDateUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.servlet.jsp.tagext.TagSupport;

public class FormatDateTag extends TagSupport {
	private static final String MODULE = "frontend";
	private static final String KEY_PATTERN = "general.date.pattern";
	private Date date;
	private Calendar calendar;
	private String pattern;
	private boolean persian;

	public FormatDateTag() {
	}

	public int doStartTag() {
		try {
			Locale e = this.pageContext.getRequest().getLocale();
			String lang = e.getLanguage() + "-" + e.getCountry();
			String pattern = "";
			String str = "";
			if (this.pattern != null && !this.pattern.isEmpty()) {
				pattern = this.pattern;
			} else {
				pattern = LanguageDAO.getTranslation(lang, MODULE, KEY_PATTERN);
			}

			if (pattern == null || pattern.isEmpty()) {
				pattern = LanguageDAO.getTranslation("en-GB", "frontend", "general.date.pattern");
			}

			if (this.date != null) {
				if (persian) {
					str = OKMDateUtil.gregorianToJalali(date);
				} else {
					str = (new SimpleDateFormat(pattern)).format(this.date);
				}
			} else if (this.calendar != null) {
				if (persian) {
					str = OKMDateUtil.gregorianToJalali(calendar.getTime())+" "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE);
				} else {
					str = (new SimpleDateFormat(pattern)).format(this.calendar.getTime());
				}
			}

			this.pageContext.getOut().write(str);
		} catch (DatabaseException var5) {
			var5.printStackTrace();
		} catch (IOException var6) {
			var6.printStackTrace();
		}

		return 0;
	}

	public void release() {
		super.release();
		this.date = null;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Calendar getCalendar() {
		return this.calendar;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	public String getPattern() {
		return this.pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public boolean isPersian() {
		return persian;
	}

	public void setPersian(boolean persian) {
		this.persian = persian;
	}
}
