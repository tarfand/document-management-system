//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.util;

public class AlgorithmUtils {
    public AlgorithmUtils() {
    }

    private static int minimum(int a, int b, int c) {
        return a <= b && a <= c?a:(b <= a && b <= c?b:c);
    }

    public static int computeLevenshteinDistance(String str1, String str2) {
        return computeLevenshteinDistance(str1.toCharArray(), str2.toCharArray());
    }

    private static int computeLevenshteinDistance(char[] str1, char[] str2) {
        int[][] distance = new int[str1.length + 1][str2.length + 1];

        int i;
        for(i = 0; i <= str1.length; distance[i][0] = i++) {
            ;
        }

        for(i = 0; i <= str2.length; distance[0][i] = i++) {
            ;
        }

        for(i = 1; i <= str1.length; ++i) {
            for(int j = 1; j <= str2.length; ++j) {
                distance[i][j] = minimum(distance[i - 1][j] + 1, distance[i][j - 1] + 1, distance[i - 1][j - 1] + (str1[i - 1] == str2[j - 1]?0:1));
            }
        }

        return distance[str1.length][str2.length];
    }

    public static int computeLevenshteinDistance(String str1, String str2, int add, int delete, int substitute) {
        return computeLevenshteinDistance(str1.toCharArray(), str2.toCharArray(), add, delete, substitute);
    }

    private static int computeLevenshteinDistance(char[] str1, char[] str2, int insert, int delete, int substitute) {
        int[][] distance = new int[str1.length + 1][str2.length + 1];

        int i;
        for(i = 0; i <= str1.length; ++i) {
            distance[i][0] = i * delete;
        }

        for(i = 0; i <= str2.length; ++i) {
            distance[0][i] = i * insert;
        }

        for(i = 1; i <= str1.length; ++i) {
            for(int j = 1; j <= str2.length; ++j) {
                distance[i][j] = minimum(distance[i - 1][j] + delete, distance[i][j - 1] + insert, distance[i - 1][j - 1] + (str1[i - 1] == str2[j - 1]?0:substitute));
            }
        }

        return distance[str1.length][str2.length];
    }
}
