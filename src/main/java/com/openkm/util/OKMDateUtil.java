package com.openkm.util;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.text.SimpleDateFormat;
import java.util.*;


public class OKMDateUtil {
    private static ScriptEngineManager manager = new ScriptEngineManager();
    private static ScriptEngine engine;
    private static String script;
    public static Map<Date, String> gregorianToJalaliMap;
    public static Map<String, String> gregorianToJalaliMapString;

    public OKMDateUtil() {
    }

    public static String jalaliToGregorian(String jalaliDate) {
        return jalaliDate != null ? jalaliToGregorian(jalaliDate, false) : null;
    }

    public static String jalaliToGregorian(String jalaliDate, boolean withoutSlash) {
        if (jalaliDate != null && !jalaliDate.equals("")) {
            try {
                Invocable ex = (Invocable) engine;
                Object obj = engine.get("JalaliDate");
                String[] jalaliArr = jalaliDate.split("/");
                String gregorianDate = ((String) ex.invokeMethod(obj, "jalaliToGregorianWithSlash", jalaliArr[0], removeZero(jalaliArr[1]), removeZero(jalaliArr[2])));
                String[] gregorianDateArr = gregorianDate.split("/");
                if (withoutSlash) {
                    return gregorianDateArr[0] + gregorianDateArr[1] + gregorianDateArr[2];
                }

                return gregorianDateArr[0] + "/" + gregorianDateArr[1] + "/" + gregorianDateArr[2];
            } catch (Exception var6) {
                var6.printStackTrace();
            }
        }

        return jalaliDate;
    }

    public static Calendar jalaliToGregorianDate(String jalaliDate) {
        if (jalaliDate != null && !jalaliDate.equals("")) {
            try {
                Invocable ex = (Invocable) engine;
                Object obj = engine.get("JalaliDate");
                String[] jalaliArr = jalaliDate.split("/");
                String[] gregorianDate = (String[]) ((String[]) ex.invokeMethod(obj, "jalaliToGregorian", new Object[]{jalaliArr[0], removeZero(jalaliArr[1]), removeZero(jalaliArr[2])}));
                String year = gregorianDate[0];
                String month = addZero(gregorianDate[1]);
                String day = addZero(gregorianDate[2]);
                return new GregorianCalendar(Integer.valueOf(year).intValue(), Integer.valueOf(month).intValue() - 1, Integer.valueOf(day).intValue());
            } catch (Exception var8) {
                var8.printStackTrace();
            }
        }

        return null;
    }

    public static String gregorianToJalali(String gregorianDate) {
        if (gregorianDate != null && !gregorianDate.equals("")) {
            try {
                Invocable ex = (Invocable) engine;
                String year = "";
                String month = "";
                String day = "";
                Object obj = engine.get("JalaliDate");
                String jalaliDate;
                String[] gregorianDateArr;
                if (gregorianDate.contains("/")) {
                    gregorianDateArr = gregorianDate.split("/");
                    year = gregorianDateArr[0];
                    month = gregorianDateArr[1];
                    day = gregorianDateArr[2];
                } else {
                    year = gregorianDate.substring(0, 4);
                    month = gregorianDate.substring(4, 6);
                    day = gregorianDate.substring(6, 8);
                }

                jalaliDate = (String) ex.invokeMethod(obj, "gregorianToJalaliWithSlash", year, removeZero(month), removeZero(day));
                String[] jalaliDateArr = jalaliDate.split("/");

                return jalaliDateArr[0] + "/" + jalaliDateArr[1] + "/" + jalaliDateArr[2];
            } catch (Exception var7) {
                var7.printStackTrace();
            }
        }

        return gregorianDate;
    }

    public static String getJalaliCurrentYear() {
        Date currentDate = new Date(System.currentTimeMillis());
        String[] jalaliDate = getJalaliDate(currentDate).split(" ")[0].split("/");
        return jalaliDate[0];
    }

    public static String getJalaliCurrentMonth() {
        Date currentDate = new Date(System.currentTimeMillis());
        String[] jalaliDate = getJalaliDate(currentDate).split(" ")[0].split("/");
        return jalaliDate[1];
    }

    public static String getJalaliCurrentDay() {
        Date currentDate = new Date(System.currentTimeMillis());
        String[] jalaliDate = getJalaliDate(currentDate).split(" ")[0].split("/");
        return jalaliDate[2];
    }

    public static String gregorianToJalali(Date gregorianDate) {
        if (gregorianDate != null) {
            SimpleDateFormat dtf = new SimpleDateFormat("yyyy/MM/dd");
            String gregorianDateStr = dtf.format(gregorianDate);
            return gregorianToJalali(gregorianDateStr);
        } else {
            return "";
        }
    }

    public static String getJalaliDate(Date gregorianDate) {
        SimpleDateFormat dtf = new SimpleDateFormat("HH:mm:ss");
        return gregorianToJalali(gregorianDate) + " " + dtf.format(gregorianDate);
    }

    private static String removeZero(String number) {
        return number.startsWith("0") ? number.charAt(1) + "" : number;
    }

    private static String addZero(String number) {
        return number.length() == 1 ? "0" + number : number;
    }

    static {
        engine = manager.getEngineByName("JavaScript");
        script =
                "JalaliDate = {\n" +
                        "    g_days_in_month: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],\n" +
                        "    j_days_in_month: [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29]\n" +
                        "};\n" +
                        "\n" +
                        "JalaliDate.jalaliToGregorian = function (j_y, j_m, j_d) {\n" +
                        "    j_y = parseInt(j_y);\n" +
                        "    j_m = parseInt(j_m);\n" +
                        "    j_d = parseInt(j_d);\n" +
                        "    var jy = j_y - 979;\n" +
                        "    var jm = j_m - 1;\n" +
                        "    var jd = j_d - 1;\n" +
                        "\n" +
                        "    var j_day_no = 365 * jy + parseInt(jy / 33) * 8 + parseInt((jy % 33 + 3) / 4);\n" +
                        "    for (var i = 0; i < jm; ++i) j_day_no += JalaliDate.j_days_in_month[i];\n" +
                        "\n" +
                        "    j_day_no += jd;\n" +
                        "\n" +
                        "    var g_day_no = j_day_no + 79;\n" +
                        "\n" +
                        "    var gy = 1600 + 400 * parseInt(g_day_no / 146097);\n" +
                        "    /* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */\n" +
                        "    g_day_no = g_day_no % 146097;\n" +
                        "\n" +
                        "    var leap = true;\n" +
                        "    if (g_day_no >= 36525) /* 36525 = 365*100 + 100/4 */\n" +
                        "    {\n" +
                        "        g_day_no--;\n" +
                        "        gy += 100 * parseInt(g_day_no / 36524);\n" +
                        "        /* 36524 = 365*100 + 100/4 - 100/100 */\n" +
                        "        g_day_no = g_day_no % 36524;\n" +
                        "\n" +
                        "        if (g_day_no >= 365)\n" +
                        "            g_day_no++;\n" +
                        "        else\n" +
                        "            leap = false;\n" +
                        "    }\n" +
                        "\n" +
                        "    gy += 4 * parseInt(g_day_no / 1461);\n" +
                        "    /* 1461 = 365*4 + 4/4 */\n" +
                        "    g_day_no %= 1461;\n" +
                        "\n" +
                        "    if (g_day_no >= 366) {\n" +
                        "        leap = false;\n" +
                        "\n" +
                        "        g_day_no--;\n" +
                        "        gy += parseInt(g_day_no / 365);\n" +
                        "        g_day_no = g_day_no % 365;\n" +
                        "    }\n" +
                        "\n" +
                        "    for (var i = 0; g_day_no >= JalaliDate.g_days_in_month[i] + (i == 1 && leap); i++)\n" +
                        "        g_day_no -= JalaliDate.g_days_in_month[i] + (i == 1 && leap);\n" +
                        "    var gm = i + 1;\n" +
                        "    var gd = g_day_no + 1;\n" +
                        "\n" +
                        "    return [gy, gm, gd];\n" +
                        "}\n" +
                        "JalaliDate.jalaliToGregorianWithSlash = function (j_y, j_m, j_d) {\n" +
                        "    j_y = parseInt(j_y);\n" +
                        "    j_m = parseInt(j_m);\n" +
                        "    j_d = parseInt(j_d);\n" +
                        "    var jy = j_y - 979;\n" +
                        "    var jm = j_m - 1;\n" +
                        "    var jd = j_d - 1;\n" +
                        "\n" +
                        "    var j_day_no = 365 * jy + parseInt(jy / 33) * 8 + parseInt((jy % 33 + 3) / 4);\n" +
                        "    for (var i = 0; i < jm; ++i) j_day_no += JalaliDate.j_days_in_month[i];\n" +
                        "\n" +
                        "    j_day_no += jd;\n" +
                        "\n" +
                        "    var g_day_no = j_day_no + 79;\n" +
                        "\n" +
                        "    var gy = 1600 + 400 * parseInt(g_day_no / 146097);\n" +
                        "    /* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */\n" +
                        "    g_day_no = g_day_no % 146097;\n" +
                        "\n" +
                        "    var leap = true;\n" +
                        "    if (g_day_no >= 36525) /* 36525 = 365*100 + 100/4 */\n" +
                        "    {\n" +
                        "        g_day_no--;\n" +
                        "        gy += 100 * parseInt(g_day_no / 36524);\n" +
                        "        /* 36524 = 365*100 + 100/4 - 100/100 */\n" +
                        "        g_day_no = g_day_no % 36524;\n" +
                        "\n" +
                        "        if (g_day_no >= 365)\n" +
                        "            g_day_no++;\n" +
                        "        else\n" +
                        "            leap = false;\n" +
                        "    }\n" +
                        "\n" +
                        "    gy += 4 * parseInt(g_day_no / 1461);\n" +
                        "    /* 1461 = 365*4 + 4/4 */\n" +
                        "    g_day_no %= 1461;\n" +
                        "\n" +
                        "    if (g_day_no >= 366) {\n" +
                        "        leap = false;\n" +
                        "\n" +
                        "        g_day_no--;\n" +
                        "        gy += parseInt(g_day_no / 365);\n" +
                        "        g_day_no = g_day_no % 365;\n" +
                        "    }\n" +
                        "\n" +
                        "    for (var i = 0; g_day_no >= JalaliDate.g_days_in_month[i] + (i == 1 && leap); i++)\n" +
                        "        g_day_no -= JalaliDate.g_days_in_month[i] + (i == 1 && leap);\n" +
                        "    var gm = i + 1;\n" +
                        "    var gd = g_day_no + 1;\n" +
                        "    if (gm.toString().length == 1){ gm = '0' + gm;}\n" +
                        "    if (gd.toString().length == 1) {gd = '0' + gd;}\n" +
                        "\n" +
                        "    return gy + '/' + gm + '/' + gd;\n" +
                        "}\n" +
                        "\n" +
                        "JalaliDate.checkDate = function (j_y, j_m, j_d) {\n" +
                        "    return !(j_y < 0 || j_y > 32767 || j_m < 1 || j_m > 12 || j_d < 1 || j_d >\n" +
                        "    (JalaliDate.j_days_in_month[j_m - 1] + (j_m == 12 && !((j_y - 979) % 33 % 4))));\n" +
                        "}\n" +
                        "\n" +
                        "JalaliDate.gregorianToJalali = function (g_y, g_m, g_d) {\n" +
                        "    g_y = parseInt(g_y);\n" +
                        "    g_m = parseInt(g_m);\n" +
                        "    g_d = parseInt(g_d);\n" +
                        "    var gy = g_y - 1600;\n" +
                        "    var gm = g_m - 1;\n" +
                        "    var gd = g_d - 1;\n" +
                        "\n" +
                        "    var g_day_no = 365 * gy + parseInt((gy + 3) / 4) - parseInt((gy + 99) / 100) + parseInt((gy + 399) / 400);\n" +
                        "\n" +
                        "    for (var i = 0; i < gm; ++i)\n" +
                        "        g_day_no += JalaliDate.g_days_in_month[i];\n" +
                        "    if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)))\n" +
                        "    /* leap and after Feb */\n" +
                        "        ++g_day_no;\n" +
                        "    g_day_no += gd;\n" +
                        "\n" +
                        "    var j_day_no = g_day_no - 79;\n" +
                        "\n" +
                        "    var j_np = parseInt(j_day_no / 12053);\n" +
                        "    j_day_no %= 12053;\n" +
                        "\n" +
                        "    var jy = 979 + 33 * j_np + 4 * parseInt(j_day_no / 1461);\n" +
                        "\n" +
                        "    j_day_no %= 1461;\n" +
                        "\n" +
                        "    if (j_day_no >= 366) {\n" +
                        "        jy += parseInt((j_day_no - 1) / 365);\n" +
                        "        j_day_no = (j_day_no - 1) % 365;\n" +
                        "    }\n" +
                        "\n" +
                        "    for (var i = 0; i < 11 && j_day_no >= JalaliDate.j_days_in_month[i]; ++i) {\n" +
                        "        j_day_no -= JalaliDate.j_days_in_month[i];\n" +
                        "    }\n" +
                        "    var jm = i + 1;\n" +
                        "    var jd = j_day_no + 1;\n" +
                        "\n" +
                        "\n" +
                        "    return [jy, jm, jd];\n" +
                        "}\n" +
                        "JalaliDate.gregorianToJalaliWithSlash = function (g_y, g_m, g_d) {\n" +
                        "    g_y = parseInt(g_y);\n" +
                        "    g_m = parseInt(g_m);\n" +
                        "    g_d = parseInt(g_d);\n" +
                        "    var gy = g_y - 1600;\n" +
                        "    var gm = g_m - 1;\n" +
                        "    var gd = g_d - 1;\n" +
                        "\n" +
                        "    var g_day_no = 365 * gy + parseInt((gy + 3) / 4) - parseInt((gy + 99) / 100) + parseInt((gy + 399) / 400);\n" +
                        "\n" +
                        "    for (var i = 0; i < gm; ++i)\n" +
                        "        g_day_no += JalaliDate.g_days_in_month[i];\n" +
                        "    if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)))\n" +
                        "    /* leap and after Feb */\n" +
                        "        ++g_day_no;\n" +
                        "    g_day_no += gd;\n" +
                        "\n" +
                        "    var j_day_no = g_day_no - 79;\n" +
                        "\n" +
                        "    var j_np = parseInt(j_day_no / 12053);\n" +
                        "    j_day_no %= 12053;\n" +
                        "\n" +
                        "    var jy = 979 + 33 * j_np + 4 * parseInt(j_day_no / 1461);\n" +
                        "\n" +
                        "    j_day_no %= 1461;\n" +
                        "\n" +
                        "    if (j_day_no >= 366) {\n" +
                        "        jy += parseInt((j_day_no - 1) / 365);\n" +
                        "        j_day_no = (j_day_no - 1) % 365;\n" +
                        "    }\n" +
                        "\n" +
                        "    for (var i = 0; i < 11 && j_day_no >= JalaliDate.j_days_in_month[i]; ++i) {\n" +
                        "        j_day_no -= JalaliDate.j_days_in_month[i];\n" +
                        "    }\n" +
                        "    var jm = i + 1;\n" +
                        "    var jd = j_day_no + 1;\n" +
                        "    if (jm.toString().length == 1){ jm = '0' + jm;}\n" +
                        "    if (jd.toString().length == 1) {jd = '0' + jd;}\n" +
                        "\n" +
                        "    return jy + '/' + jm + '/' + jd;\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.setJalaliFullYear = function (y, m, d) {\n" +
                        "    var gd = this.getDate();\n" +
                        "    var gm = this.getMonth();\n" +
                        "    var gy = this.getFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    if (y < 100) y += 1300;\n" +
                        "    j[0] = y;\n" +
                        "    if (m != undefined) {\n" +
                        "        if (m > 11) {\n" +
                        "            j[0] += Math.floor(m / 12);\n" +
                        "            m = m % 12;\n" +
                        "        }\n" +
                        "        j[1] = m + 1;\n" +
                        "    }\n" +
                        "    if (d != undefined) j[2] = d;\n" +
                        "    var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);\n" +
                        "    return this.setFullYear(g[0], g[1] - 1, g[2]);\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.setJalaliMonth = function (m, d) {\n" +
                        "    var gd = this.getDate();\n" +
                        "    var gm = this.getMonth();\n" +
                        "    var gy = this.getFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    if (m > 11) {\n" +
                        "        j[0] += math.floor(m / 12);\n" +
                        "        m = m % 12;\n" +
                        "    }\n" +
                        "    j[1] = m + 1;\n" +
                        "    if (d != undefined) j[2] = d;\n" +
                        "    var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);\n" +
                        "    return this.setFullYear(g[0], g[1] - 1, g[2]);\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.setJalaliDate = function (d) {\n" +
                        "    var gd = this.getDate();\n" +
                        "    var gm = this.getMonth();\n" +
                        "    var gy = this.getFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    j[2] = d;\n" +
                        "    var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);\n" +
                        "    return this.setFullYear(g[0], g[1] - 1, g[2]);\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.getJalaliFullYear = function () {\n" +
                        "    var gd = this.getDate();\n" +
                        "    var gm = this.getMonth();\n" +
                        "    var gy = this.getFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    return j[0];\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.getJalaliMonth = function () {\n" +
                        "    var gd = this.getDate();\n" +
                        "    var gm = this.getMonth();\n" +
                        "    var gy = this.getFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    return j[1] - 1;\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.getJalaliDate = function () {\n" +
                        "    var gd = this.getDate();\n" +
                        "    var gm = this.getMonth();\n" +
                        "    var gy = this.getFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    return j[2];\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.getJalaliDay = function () {\n" +
                        "    var day = this.getDay();\n" +
                        "    day = (day + 1) % 7;\n" +
                        "    return day;\n" +
                        "}\n" +
                        "\n" +
                        "\n" +
                        "/**\n" +
                        " * Jalali UTC functions\n" +
                        " */\n" +
                        "\n" +
                        "Date.prototype.setJalaliUTCFullYear = function (y, m, d) {\n" +
                        "    var gd = this.getUTCDate();\n" +
                        "    var gm = this.getUTCMonth();\n" +
                        "    var gy = this.getUTCFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    if (y < 100) y += 1300;\n" +
                        "    j[0] = y;\n" +
                        "    if (m != undefined) {\n" +
                        "        if (m > 11) {\n" +
                        "            j[0] += Math.floor(m / 12);\n" +
                        "            m = m % 12;\n" +
                        "        }\n" +
                        "        j[1] = m + 1;\n" +
                        "    }\n" +
                        "    if (d != undefined) j[2] = d;\n" +
                        "    var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);\n" +
                        "    return this.setUTCFullYear(g[0], g[1] - 1, g[2]);\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.setJalaliUTCMonth = function (m, d) {\n" +
                        "    var gd = this.getUTCDate();\n" +
                        "    var gm = this.getUTCMonth();\n" +
                        "    var gy = this.getUTCFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    if (m > 11) {\n" +
                        "        j[0] += math.floor(m / 12);\n" +
                        "        m = m % 12;\n" +
                        "    }\n" +
                        "    j[1] = m + 1;\n" +
                        "    if (d != undefined) j[2] = d;\n" +
                        "    var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);\n" +
                        "    return this.setUTCFullYear(g[0], g[1] - 1, g[2]);\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.setJalaliUTCDate = function (d) {\n" +
                        "    var gd = this.getUTCDate();\n" +
                        "    var gm = this.getUTCMonth();\n" +
                        "    var gy = this.getUTCFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    j[2] = d;\n" +
                        "    var g = JalaliDate.jalaliToGregorian(j[0], j[1], j[2]);\n" +
                        "    return this.setUTCFullYear(g[0], g[1] - 1, g[2]);\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.getJalaliUTCFullYear = function () {\n" +
                        "    var gd = this.getUTCDate();\n" +
                        "    var gm = this.getUTCMonth();\n" +
                        "    var gy = this.getUTCFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    return j[0];\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.getJalaliUTCMonth = function () {\n" +
                        "    var gd = this.getUTCDate();\n" +
                        "    var gm = this.getUTCMonth();\n" +
                        "    var gy = this.getUTCFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    return j[1] - 1;\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.getJalaliUTCDate = function () {\n" +
                        "    var gd = this.getUTCDate();\n" +
                        "    var gm = this.getUTCMonth();\n" +
                        "    var gy = this.getUTCFullYear();\n" +
                        "    var j = JalaliDate.gregorianToJalali(gy, gm + 1, gd);\n" +
                        "    return j[2];\n" +
                        "}\n" +
                        "\n" +
                        "Date.prototype.getJalaliUTCDay = function () {\n" +
                        "    var day = this.getUTCDay();\n" +
                        "    day = (day + 1) % 7;\n" +
                        "    return day;\n" +
                        "}";


        try {
            engine.eval(script);
        } catch (ScriptException var1) {
            var1.printStackTrace();
        }

        gregorianToJalaliMap = new HashMap() {
            public String get(Object o) {
                return OKMDateUtil.gregorianToJalali((Date) o);
            }
        };
        gregorianToJalaliMapString = new HashMap() {
            public String get(Object o) {
                return OKMDateUtil.gregorianToJalali((String) o);
            }
        };
    }
}
