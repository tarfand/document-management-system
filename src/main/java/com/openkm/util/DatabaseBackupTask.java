package com.openkm.util;

import com.openkm.dao.HibernateUtil;
import com.openkm.module.db.stuff.DbSessionManager;
import com.openkm.spring.PrincipalUtils;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Settings;
import org.hibernate.impl.SessionFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.*;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.openkm.core.Config.REPOSITORY_HOME;

@Component
public class DatabaseBackupTask implements ApplicationContextAware {

	private static final Logger log = LoggerFactory.getLogger(DatabaseBackupTask.class);
	private static final String OS = System.getProperty("os.name").toLowerCase();
	private static volatile boolean running = false;
	private static ApplicationContext context;

	public static synchronized void backupDb() {
		Authentication auth = null, oldAuth = null;
		String systemToken = DbSessionManager.getInstance().getSystemToken();

		if (running) {
			log.warn("*** Database Backup already running ***");
		} else {
			running = true;
			log.info("*** Begin backing up the Database ***");

			try {
				if (systemToken == null) {
					auth = PrincipalUtils.getAuthentication();
				} else {
					oldAuth = PrincipalUtils.getAuthentication();
					auth = PrincipalUtils.getAuthenticationByToken(systemToken);
				}
				java.lang.Runtime rt = java.lang.Runtime.getRuntime();
				String username = null;
				String dbname = null;
				String backupFilename = null;
				String backupDirPath;
				String pgDumpExePath = null;
				String backupPath;
				String databaseVendor = "";
				SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
				Settings settings = ((SessionFactoryImpl) sessionFactory).getSettings();
				BasicDataSource basicDataSource = (BasicDataSource) context.getBean("dataSource");
				String password = basicDataSource.getPassword();
				String port = null;
				Integer databaseMinorVersion = null;
				Integer databaseMajorVersion = null;
				if (settings != null) {
					Connection connection = settings.getConnectionProvider().getConnection();
					DatabaseMetaData metaData = connection.getMetaData();
					String dbURL = metaData.getURL();
					username = metaData.getUserName();
					if (username.contains("@")) {
						username = username.split("@")[0];
					}
					String[] urlArray = dbURL.split(":");
					databaseVendor = urlArray[1];
					dbname = urlArray[urlArray.length - 1];
					if (databaseVendor.equals("mysql")) {
						dbname = dbname.split("\\?")[0];
					}
					if (dbname.contains("/")) {
						port = dbname.split("/")[0];
						dbname = dbname.split("/")[1];
					}
					backupFilename = databaseVendor + "-" + dbname + "-" + new Timestamp(System.currentTimeMillis()).getTime();
					databaseMajorVersion = metaData.getDriverMajorVersion();
					databaseMinorVersion = metaData.getDriverMinorVersion();
					connection.close();
				}

				if (databaseMajorVersion == null) {
					throw new RuntimeException("The database Version cannot be found");
				}
				boolean windows = OS.contains("windows");
				boolean postgres = databaseVendor.equals("postgresql");
				boolean mysql = databaseVendor.equals("mysql");
				log.info("database vendor: " + databaseVendor);
				log.info("os: " + OS);
				if (!postgres && !mysql) {
					throw new RuntimeException("This version of is database neither postgres nor mysql.");
				}
				if (postgres) {
					pgDumpExePath = "pg_dump";
					try {
						rt.exec(pgDumpExePath);
					} catch (IOException e) {
						if (windows) {
							pgDumpExePath = "C:\\Program Files\\PostgreSQL\\" + databaseMajorVersion + "\\bin\\pg_dump.exe";
						} else {
							pgDumpExePath = "/usr/bin/pg_dump";
						}
					}
					log.info("pg_dump path: " + pgDumpExePath);
				} else if (mysql) {
					pgDumpExePath = "mysqldump";
					try {
						rt.exec(pgDumpExePath);
					} catch (IOException e) {
						if (windows) {
							pgDumpExePath = "C:\\Program Files\\MySQL\\MySQL Server " + databaseMajorVersion + "." + databaseMinorVersion + "\\bin\\mysqldump.exe";
						} else {
							pgDumpExePath = "/usr/bin/mysqldump";
						}
					}
					log.info("pg_dump path: " + pgDumpExePath);
				}

				if (windows) {
					backupDirPath = REPOSITORY_HOME + "\\sima_backups\\";
				} else {
					backupDirPath = REPOSITORY_HOME + "/sima_backups/";
				}
				backupPath = backupDirPath + backupFilename;
				log.info("backup path: " + backupPath);
				File backupDir = new File(backupDirPath);
				if (!backupDir.exists()) {
					if (!backupDir.mkdir()) {
						throw new RuntimeException("Error creating backup directory .." + backupDirPath);
					}
				}
				Process p;
				ProcessBuilder pb;
				if (postgres) {
					pb = new ProcessBuilder(
							pgDumpExePath,
							"--host", "localhost",
							"--port", port,
							"--username", username,
							"--no-password",
							"--format", "custom",
							"--blobs",
							"--verbose", "--file", backupPath + "_custom", dbname);
					final Map<String, String> env = pb.environment();
					env.put("PGPASSWORD", password);
					p = pb.start();
					final BufferedReader r = new BufferedReader(
							new InputStreamReader(p.getErrorStream()));
					String s;
					while ((s = r.readLine()) != null) {
						log.info(s);
					}
					r.close();
					p.waitFor();
					log.info("The exit value of database backup runner:" + p.exitValue());
				} else if (mysql) { // mysql
					List<String> args = new ArrayList<String>();
					args.add(pgDumpExePath);
					args.add("-u");
					args.add(username);
					args.add("-p" + password);
					args.add("--default-character-set=utf8");
					args.add("-B");
					args.add(dbname);
					args.add("-r");
					args.add(backupPath + "_sql");

					pb = new ProcessBuilder(args);
					p = pb.start();

					InputStream is = p.getInputStream();
					int in;
					while ((in = is.read()) != -1) {
						System.out.print((char) in);
					}
				} else {
					//app goes never here
				}
			} catch (
					Exception e) {
				throw new RuntimeException(e.getMessage());
			} finally {
				if (systemToken != null) {
					PrincipalUtils.setAuthentication(oldAuth);
				}
				running = false;
			}
			log.info("*** End of database backup task ***");
		}
		log.debug("backupDb() as  : void");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		DatabaseBackupTask.context = applicationContext;
	}
}
