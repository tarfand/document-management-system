//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.extension.frontend.client.widget.textextracted;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.openkm.frontend.client.Main;
import com.openkm.frontend.client.extension.comunicator.GeneralComunicator;
import com.openkm.frontend.client.extension.comunicator.TabDocumentComunicator;
import com.openkm.frontend.client.extension.event.HasDocumentEvent;
import com.openkm.frontend.client.extension.event.HasDocumentEvent.DocumentEventConstant;
import com.openkm.frontend.client.extension.event.handler.DocumentHandlerExtension;
import com.openkm.frontend.client.extension.widget.tabdocument.TabDocumentExtension;
import com.openkm.frontend.client.service.OKMDocumentService;
import com.openkm.frontend.client.service.OKMDocumentServiceAsync;

public class TabTextExtracted extends TabDocumentExtension implements DocumentHandlerExtension {
    private final OKMDocumentServiceAsync documentService = (OKMDocumentServiceAsync)GWT.create(OKMDocumentService.class);
    private String title = "";
    private String docUuid;
    private ScrollPanel scrollPanel;
    private HorizontalPanel vPanel;
    private HTML text;

    public TabTextExtracted() {
        this.title = GeneralComunicator.i18nExtension("text.extracted.tab.document.title");
        this.vPanel = new HorizontalPanel();
        this.vPanel.setHeight("100%");
        this.scrollPanel = new ScrollPanel(this.vPanel);
        this.initWidget(this.scrollPanel);
    }

    public String getTabText() {
        return this.title;
    }

    public void langRefresh() {
        this.title = GeneralComunicator.i18nExtension("text.extracted.tab.document.title");
    }

    public void onChange(DocumentEventConstant event) {
        if(event.equals(HasDocumentEvent.DOCUMENT_CHANGED)) {
            this.set(TabDocumentComunicator.getDocument().getUuid());
        }

    }

    private void set(String uuid) {
        this.docUuid = uuid;
        this.vPanel.clear();
        HTML space = new HTML("&nbsp;");
        this.vPanel.add(space);
        this.vPanel.addStyleName("okm-EnableSelect");
        this.documentService.isExtractedText(this.docUuid, new AsyncCallback<Boolean>() {
            public void onSuccess(Boolean extracted) {
                if(extracted.booleanValue()) {
                    TabTextExtracted.this.documentService.getDocumentExtractedText(TabTextExtracted.this.docUuid, new AsyncCallback<String>() {
                        public void onSuccess(String result) {
                            TabTextExtracted.this.text = new HTML(result);
                            TabTextExtracted.this.vPanel.add(TabTextExtracted.this.text);
                        }

                        public void onFailure(Throwable caught) {
                            Main.get().showError("set", caught);
                        }
                    });
                } else {
                    TabTextExtracted.this.text = new HTML(GeneralComunicator.i18nExtension("text.extracted.tab.document.pending"));
                    TabTextExtracted.this.vPanel.add(TabTextExtracted.this.text);
                }

            }

            public void onFailure(Throwable caught) {
                Main.get().showError("set", caught);
            }
        });
    }
}
