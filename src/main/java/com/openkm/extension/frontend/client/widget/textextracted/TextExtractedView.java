//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.extension.frontend.client.widget.textextracted;

import com.google.gwt.user.client.ui.Widget;
import com.openkm.frontend.client.extension.event.HasLanguageEvent;
import com.openkm.frontend.client.extension.event.HasLanguageEvent.LanguageEventConstant;
import com.openkm.frontend.client.extension.event.handler.LanguageHandlerExtension;

import java.util.ArrayList;
import java.util.List;

public class TextExtractedView implements LanguageHandlerExtension {
    public static final String UUID = "7b08995d-fa1a-4ebf-98f3-a3ef322aa4d5";
    public static TextExtractedView singleton;
    private TabTextExtracted tabTextExtracted;

    public TextExtractedView(List<String> uuidList) {
        if(isRegistered(uuidList)) {
            singleton = this;
            this.tabTextExtracted = new TabTextExtracted();
        }

    }

    public static TextExtractedView get() {
        return singleton;
    }

    public Widget getWidgetTab() {
        return this.tabTextExtracted;
    }

    public void onChange(LanguageEventConstant event) {
        if(event.equals(HasLanguageEvent.LANGUAGE_CHANGED)) {
            this.tabTextExtracted.langRefresh();
        }

    }

    public List<Object> getExtensions() {
        ArrayList extensions = new ArrayList();
        extensions.add(singleton);
        extensions.add(this.tabTextExtracted);
        return extensions;
    }

    public static boolean isRegistered(List<String> uuidList) {
        return uuidList.contains("7b08995d-fa1a-4ebf-98f3-a3ef322aa4d5");
    }
}
