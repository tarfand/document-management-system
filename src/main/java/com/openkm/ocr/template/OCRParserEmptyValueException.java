//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.ocr.template;

import com.openkm.core.OKMException;

public class OCRParserEmptyValueException extends OKMException {
    private static final long serialVersionUID = 1L;

    public OCRParserEmptyValueException() {
    }

    public OCRParserEmptyValueException(String arg0) {
        super(arg0);
    }

    public OCRParserEmptyValueException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public OCRParserEmptyValueException(Throwable arg0) {
        super(arg0);
    }
}
