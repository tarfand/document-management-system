package com.openkm.ocr.template.parser;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.openkm.dao.bean.OCRTemplateField;
import com.openkm.ocr.template.OCRParserEmptyValueException;
import com.openkm.ocr.template.OCRTemplateException;
import com.openkm.ocr.template.OCRTemplateParser;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import net.xeoh.plugins.base.annotations.PluginImplementation;

@PluginImplementation
public class DateParser implements OCRTemplateParser {
    public DateParser() {
    }

    public Object parse(OCRTemplateField otf, String text) throws OCRTemplateException, OCRParserEmptyValueException {
        if(text != null && !text.equals("")) {
            if(otf.getPattern() != null && !otf.getPattern().equals("")) {
                try {
                    SimpleDateFormat e = new SimpleDateFormat(otf.getPattern());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(e.parse(text));
                    return cal;
                } catch (IllegalArgumentException var5) {
                    throw new OCRTemplateException(var5);
                } catch (ParseException var6) {
                    throw new OCRTemplateException(var6);
                }
            } else {
                throw new OCRTemplateException("Pattern is mandatory for date fields");
            }
        } else {
            throw new OCRParserEmptyValueException("Empty value");
        }
    }

    public String getName() {
        return "Date";
    }

    public boolean isPatternRequired() {
        return true;
    }
}
