//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.ocr.template.parser;

import com.openkm.dao.bean.OCRTemplateField;
import com.openkm.ocr.template.OCRParserEmptyValueException;
import com.openkm.ocr.template.OCRTemplateException;
import com.openkm.ocr.template.OCRTemplateParser;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.xeoh.plugins.base.annotations.PluginImplementation;

@PluginImplementation
public class BarcodeParser implements OCRTemplateParser {
    public BarcodeParser() {
    }

    public Object parse(OCRTemplateField otf, String text) throws OCRTemplateException, OCRParserEmptyValueException {
        if(text != null && !text.equals("")) {
            if(otf.getPattern() != null && !otf.getPattern().equals("")) {
                Pattern pattern = Pattern.compile(otf.getPattern(), 64);
                Matcher matcher = pattern.matcher(text);
                if(matcher.matches()) {
                    return text != null?text.trim():null;
                } else {
                    throw new OCRTemplateException("Bad format, parse exception");
                }
            } else {
                return text != null?text.trim():null;
            }
        } else {
            throw new OCRParserEmptyValueException("Empty value");
        }
    }

    public String getName() {
        return "Barcode";
    }

    public boolean isPatternRequired() {
        return false;
    }
}
