package com.openkm.ocr.template.parser;
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.openkm.dao.bean.OCRTemplateField;
import com.openkm.ocr.template.OCRParserEmptyValueException;
import com.openkm.ocr.template.OCRTemplateException;
import com.openkm.ocr.template.OCRTemplateParser;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.xeoh.plugins.base.annotations.PluginImplementation;

@PluginImplementation
public class NumberParser implements OCRTemplateParser {
    public NumberParser() {
    }

    public Object parse(OCRTemplateField otf, String text) throws OCRTemplateException, OCRParserEmptyValueException {
        if(text != null && !text.equals("")) {
            if(otf.getPattern() != null && !otf.getPattern().equals("")) {
                DecimalFormat format1 = new DecimalFormat(otf.getPattern());
                Pattern pattern = Pattern.compile(format1.toPattern(), 64);
                Matcher matcher = pattern.matcher(text);
                if(matcher.matches()) {
                    try {
                        Number e = format1.parse(text);
                        return e;
                    } catch (NumberFormatException var7) {
                        throw new OCRTemplateException(var7);
                    } catch (ParseException var8) {
                        throw new OCRTemplateException(var8);
                    }
                } else {
                    throw new OCRTemplateException("Bad format, parse exception");
                }
            } else {
                try {
                    Number format = NumberFormat.getInstance().parse(text);
                    return format;
                } catch (NumberFormatException var9) {
                    throw new OCRTemplateException(var9);
                } catch (ParseException var10) {
                    throw new OCRTemplateException(var10);
                }
            }
        } else {
            throw new OCRParserEmptyValueException("Empty value");
        }
    }

    public String getName() {
        return "Number";
    }

    public boolean isPatternRequired() {
        return false;
    }
}
