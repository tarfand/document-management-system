//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.ocr.template.parser;

import com.openkm.core.AccessDeniedException;
import com.openkm.core.DatabaseException;
import com.openkm.core.PathNotFoundException;
import com.openkm.dao.NodeDocumentVersionDAO;
import com.openkm.dao.bean.OCRTemplateField;
import com.openkm.ocr.template.OCRParserEmptyValueException;
import com.openkm.ocr.template.OCRTemplateException;
import com.openkm.ocr.template.OCRTemplateParser;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@PluginImplementation
public class StringParser implements OCRTemplateParser {

    public StringParser() {
    }

    public Object parse(OCRTemplateField otf, String text) throws OCRTemplateException, OCRParserEmptyValueException {
        if (text != null && !text.equals("")) {
            String output = text;
            if (otf.getPattern() != null && !otf.getPattern().equals("")) {
                Pattern pattern = Pattern.compile("(" + otf.getPattern() + ")", 64);
                Matcher matcher = pattern.matcher(text);
                if (matcher.find() && matcher.groupCount() == 1) {
                    output = matcher.group();
                }
            }
            String filterPattern = otf.getFilterPattern();
            if (filterPattern != null && !filterPattern.isEmpty()) {
                output = output.replaceAll(filterPattern, "");
            }
            String useWordListUUId = otf.getWordListUUID();
            if (useWordListUUId != null && !useWordListUUId.isEmpty()) {
                output = spellCheckerSentence(output, useWordListUUId);
            }
            if (output != null) {
                output = output.trim();
            }
            return output;
        } else {
            throw new OCRParserEmptyValueException("Empty value");
        }
    }

    public String getName() {
        return "String";
    }

    public boolean isPatternRequired() {
        return false;
    }

    /**
     * Obtain lock token from node id
     */

    private String spellCheckerSentence(String input, String useWordListUUId) {

        File useWordListFile = null;
        if (useWordListUUId != null && !useWordListUUId.isEmpty()) {
            try {
                useWordListFile = NodeDocumentVersionDAO.getInstance().getCurrentFileByUuid(useWordListUUId);
            } catch (PathNotFoundException e) {
                e.printStackTrace();
            } catch (AccessDeniedException e) {
                e.printStackTrace();
            } catch (DatabaseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (useWordListFile != null) {
            StringTokenizer st = new StringTokenizer(input);
            while (st.hasMoreTokens()) {
                String w = st.nextToken();
                try {
                    try (BufferedReader br = new BufferedReader(new FileReader(useWordListFile))) {
                        String line;
                        int distance = Integer.MAX_VALUE;
                        String c = " ";
                        while ((line = br.readLine()) != null) {
                            int d = StringUtils.getLevenshteinDistance(line, w);
                            if (d < distance) {
                                distance = d;
                                c = line;
                            }
                        }
                        input = input.replace(w, c);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            return input;
        }
        return null;
    }

}
