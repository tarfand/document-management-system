//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.ocr.template.bean;

import com.openkm.ocr.template.bean.OCRConfidenceLevel;
import java.io.Serializable;
import java.util.List;

public class OCRRecognise implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int STATUS_NOT_RECOGNIZED = 0;
    public static final int STATUS_RECOGNIZED = 1;
    public static final int STATUS_SEVERAL_RECOGNIZED = 2;
    private List<OCRConfidenceLevel> confidences;
    private int status;

    public OCRRecognise() {
    }

    public List<OCRConfidenceLevel> getConfidences() {
        return this.confidences;
    }

    public void setConfidences(List<OCRConfidenceLevel> confidences) {
        this.confidences = confidences;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
