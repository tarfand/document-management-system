//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.ocr.template;

import com.openkm.core.OKMException;

public class OCRTemplateException extends OKMException {
    private static final long serialVersionUID = 1L;

    public OCRTemplateException() {
        this.setErrorCode("052");
    }

    public OCRTemplateException(String arg0) {
        super(arg0);
        this.setErrorCode("052");
    }

    public OCRTemplateException(String arg0, Throwable arg1) {
        super(arg0, arg1);
        this.setErrorCode("052");
    }

    public OCRTemplateException(Throwable arg0) {
        super(arg0);
        this.setErrorCode("052");
    }
}
