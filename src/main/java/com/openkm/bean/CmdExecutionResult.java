//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.bean;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(
        name = "cmdExecutionResult"
)
public class CmdExecutionResult {
    private int exitValue = -1;
    private String stderr;
    private String stdout;

    public CmdExecutionResult() {
    }

    public String getStdout() {
        return this.stdout;
    }

    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    public String getStderr() {
        return this.stderr;
    }

    public void setStderr(String stderr) {
        this.stderr = stderr;
    }

    public int getExitValue() {
        return this.exitValue;
    }

    public void setExitValue(int exitValue) {
        this.exitValue = exitValue;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        sb.append("exitValue=").append(this.exitValue);
        sb.append(", stderr=").append(this.stderr);
        sb.append(", stdout=").append(this.stdout);
        sb.append("}");
        return sb.toString();
    }
}
