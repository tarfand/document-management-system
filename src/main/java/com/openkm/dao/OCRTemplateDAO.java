//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.dao;

import com.openkm.core.DatabaseException;
import com.openkm.dao.bean.OCRTemplate;
import com.openkm.dao.bean.OCRTemplateControlField;
import com.openkm.dao.bean.OCRTemplateField;
import com.openkm.ocr.template.OCRTemplateControlParser;
import com.openkm.ocr.template.OCRTemplateException;
import com.openkm.ocr.template.OCRTemplateParser;
import com.openkm.spring.PrincipalUtils;
import com.openkm.util.PluginUtils;
import net.xeoh.plugins.base.Plugin;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class OCRTemplateDAO {
    private static Logger log = LoggerFactory.getLogger(OCRTemplateDAO.class);
    private static OCRTemplateDAO single = new OCRTemplateDAO();
    private static List<OCRTemplateParser> parsersList;
    private static List<OCRTemplateControlParser> controlParsersList;
    public static final String PLUGIN_URI = "classpath://com.openkm.ocr.template.controlparser.**";
    public static final String PLUGIN_URI_PARSER = "classpath://com.openkm.ocr.template.parser.**";

    private OCRTemplateDAO() {
    }

    public static OCRTemplateDAO getInstance() {
        return single;
    }

    public long create(OCRTemplate ot) throws DatabaseException {
        log.debug("create({})", ot);
        Session session = null;
        Transaction tx = null;

        long var7;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            Long e = (Long)session.save(ot);
            HibernateUtil.commit(tx);
            log.debug("create: {}", e);
            var7 = e.longValue();
        } catch (HibernateException var12) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var12.getMessage(), var12);
        } finally {
            HibernateUtil.close(session);
        }

        return var7;
    }

    public void update(OCRTemplate ot) throws DatabaseException {
        log.debug("update({})", ot);
        String qs = "select ot.imageContent, ot.imageMime from OCRTemplate ot where ot.id=:id";
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            if(ot.getImageContent() == null || ot.getImageContent().length == 0) {
                Query e = session.createQuery(qs);
                e.setParameter("id", Long.valueOf(ot.getId()));
                Object[] data = (Object[])((Object[])e.setMaxResults(1).uniqueResult());
                ot.setImageContent((byte[])((byte[])data[0]));
                ot.setImageMime((String)data[1]);
            }
            session.update(ot);
            HibernateUtil.commit(tx);
        } catch (HibernateException var12) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var12.getMessage(), var12);
        } finally {
            HibernateUtil.close(session);
        }

        log.debug("update: void");
    }

    public void updateField(OCRTemplateField otf) throws DatabaseException {
        log.debug("updateField({})", otf);
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.update(otf);
            HibernateUtil.commit(tx);
        } catch (HibernateException var8) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var8.getMessage(), var8);
        } finally {
            HibernateUtil.close(session);
        }

        log.debug("updateField: void");
    }

    public void updateControlField(OCRTemplateControlField otcf) throws DatabaseException {
        log.debug("updateField({})", otcf);
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.update(otcf);
            HibernateUtil.commit(tx);
        } catch (HibernateException var8) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var8.getMessage(), var8);
        } finally {
            HibernateUtil.close(session);
        }

        log.debug("updateControlField: void");
    }

    public void delete(long otId) throws DatabaseException {
        log.debug("delete({})", Long.valueOf(otId));
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            OCRTemplate e = (OCRTemplate)session.load(OCRTemplate.class, Long.valueOf(otId));
            session.delete(e);
            HibernateUtil.commit(tx);
        } catch (HibernateException var9) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var9.getMessage(), var9);
        } finally {
            HibernateUtil.close(session);
        }

        log.debug("delete: void");
    }

    public void deleteField(long otfId) throws DatabaseException {
        log.debug("deleteField({})", Long.valueOf(otfId));
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            OCRTemplateField e = (OCRTemplateField)session.load(OCRTemplateField.class, Long.valueOf(otfId));
            session.delete(e);
            HibernateUtil.commit(tx);
        } catch (HibernateException var9) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var9.getMessage(), var9);
        } finally {
            HibernateUtil.close(session);
        }

        log.debug("deleteField: void");
    }

    public void deleteControlField(long otcfId) throws DatabaseException {
        log.debug("deleteControlField({})", Long.valueOf(otcfId));
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            OCRTemplateControlField e = (OCRTemplateControlField)session.load(OCRTemplateControlField.class, Long.valueOf(otcfId));
            session.delete(e);
            HibernateUtil.commit(tx);
        } catch (HibernateException var9) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var9.getMessage(), var9);
        } finally {
            HibernateUtil.close(session);
        }

        log.debug("deleteControlField: void");
    }

    public OCRTemplate findByPk(long otId) throws DatabaseException {
        log.debug("findByPk({})", Long.valueOf(otId));
        String qs = "from OCRTemplate ot where ot.id=:id";
        Session session = null;
        Transaction tx = null;

        OCRTemplate var10;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            Query e = session.createQuery(qs);
            e.setLong("id", otId);
            OCRTemplate ret = (OCRTemplate)e.setMaxResults(1).uniqueResult();
            this.initializeOCRTemplate(ret);
            HibernateUtil.commit(tx);
            log.debug("findByPk: {}", ret);
            var10 = ret;
        } catch (HibernateException var14) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var14.getMessage(), var14);
        } finally {
            HibernateUtil.close(session);
        }

        return var10;
    }

    public OCRTemplateField findFieldByPk(long otfId) throws DatabaseException {
        log.debug("findFieldByPk({})", Long.valueOf(otfId));
        String qs = "from OCRTemplateField otf where otf.id=:id";
        Session session = null;
        Object tx = null;

        OCRTemplateField var8;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query e = session.createQuery(qs);
            e.setLong("id", otfId);
            OCRTemplateField ret = (OCRTemplateField)e.setMaxResults(1).uniqueResult();
            log.debug("findFieldByPk: {}", ret);
            var8 = ret;
        } catch (HibernateException var12) {
            HibernateUtil.rollback((Transaction)tx);
            throw new DatabaseException(var12.getMessage(), var12);
        } finally {
            HibernateUtil.close(session);
        }

        return var8;
    }

    public OCRTemplateControlField findControlFieldByPk(long otcfId) throws DatabaseException {
        log.debug("findControlFieldByPk({})", Long.valueOf(otcfId));
        String qs = "from OCRTemplateControlField otcf where otcf.id=:id";
        Session session = null;
        Object tx = null;

        OCRTemplateControlField var8;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query e = session.createQuery(qs);
            e.setLong("id", otcfId);
            OCRTemplateControlField ret = (OCRTemplateControlField)e.setMaxResults(1).uniqueResult();
            log.debug("findControlFieldByPk: {}", ret);
            var8 = ret;
        } catch (HibernateException var12) {
            HibernateUtil.rollback((Transaction)tx);
            throw new DatabaseException(var12.getMessage(), var12);
        } finally {
            HibernateUtil.close(session);
        }

        return var8;
    }

    public synchronized List<OCRTemplateParser> findParsers(boolean reload) throws URISyntaxException {
        log.debug("findParsers({})", Boolean.valueOf(reload));
        if(parsersList == null || reload) {
            if(parsersList == null) {
                parsersList = new ArrayList();
            }

            parsersList.clear();
            URI uri = new URI("classpath://com.openkm.ocr.template.parser.**");
            Iterator var3 = PluginUtils.getPlugins(uri, OCRTemplateParser.class).iterator();

            while(var3.hasNext()) {
                Plugin plg = (Plugin)var3.next();
                parsersList.add((OCRTemplateParser)plg);
            }

            Collections.sort(parsersList, new OCRTemplateParserComparator());
        }

        return parsersList;
    }

    public synchronized List<OCRTemplateControlParser> findControlParsers(boolean reload) throws URISyntaxException {
        log.debug("findControlParsers({})", Boolean.valueOf(reload));
        if(controlParsersList == null || reload) {
            if(controlParsersList == null) {
                controlParsersList = new ArrayList();
            }

            controlParsersList.clear();
            URI uri = new URI("classpath://com.openkm.ocr.template.controlparser.**");
            Iterator var3 = PluginUtils.getPlugins(uri, OCRTemplateControlParser.class).iterator();

            while(var3.hasNext()) {
                Plugin plg = (Plugin)var3.next();
                controlParsersList.add((OCRTemplateControlParser)plg);
            }

            Collections.sort(controlParsersList, new OCRTemplateControlParserComparator());
        }

        return controlParsersList;
    }

    public OCRTemplateParser findParserByClassName(String className) throws OCRTemplateException {
        log.debug("findParsersByClassName({})", className);
        Iterator var2 = parsersList.iterator();

        OCRTemplateParser otp;
        do {
            if(!var2.hasNext()) {
                throw new OCRTemplateException("Class not found exception: " + className);
            }

            otp = (OCRTemplateParser)var2.next();
        } while(!otp.getClass().getName().equals(className));

        return otp;
    }

    public OCRTemplateControlParser findControlParserByClassName(String className) throws OCRTemplateException {
        log.debug("findControlParserByClassName({})", className);
        Iterator var2 = controlParsersList.iterator();

        OCRTemplateControlParser otcp;
        do {
            if(!var2.hasNext()) {
                throw new OCRTemplateException("Class not found exception: " + className);
            }

            otcp = (OCRTemplateControlParser)var2.next();
        } while(!otcp.getClass().getName().equals(className));

        return otcp;
    }

    public List<OCRTemplate> findAll() throws DatabaseException {
        log.debug("findAll()");
        String qs = "from OCRTemplate ot order by ot.id";
        Session session = null;
        Transaction tx = null;

        List var8;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            Query e = session.createQuery(qs).setCacheable(true);
            List ret = e.list();
            HibernateUtil.commit(tx);
            log.debug("findAll: {}", ret);
            var8 = ret;
        } catch (HibernateException var12) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var12.getMessage(), var12);
        } finally {
            HibernateUtil.close(session);
        }

        return var8;
    }

    public List<OCRTemplate> findAllActive() throws DatabaseException {
        log.debug("findAll()");
        String qs = "from OCRTemplate ot where ot.active=:active order by ot.id";
        Session session = null;
        Transaction tx = null;

        List var8;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            Query e = session.createQuery(qs).setCacheable(true);
            e.setBoolean("active", true);
            List ret = e.list();
            this.initializeOCRTemplate(ret);
            HibernateUtil.commit(tx);
            log.debug("findAll: {}", ret);
            var8 = ret;
        } catch (HibernateException var12) {
            HibernateUtil.rollback(tx);
            throw new DatabaseException(var12.getMessage(), var12);
        } finally {
            HibernateUtil.close(session);
        }

        return var8;
    }

    private void initializeOCRTemplate(List<OCRTemplate> oTemplates) {
        Iterator var2 = oTemplates.iterator();

        while(var2.hasNext()) {
            OCRTemplate oTemplate = (OCRTemplate)var2.next();
            this.initializeOCRTemplate(oTemplate);
        }

    }

    private void initializeOCRTemplate(OCRTemplate oTemplate) {
        if(oTemplate != null) {
            Hibernate.initialize(oTemplate);
            Hibernate.initialize(oTemplate.getFields());
            Hibernate.initialize(oTemplate.getControlFields());
            Hibernate.initialize(oTemplate.getProperties());
            Collections.sort(oTemplate.getControlFields(), new OCRTemplateControlFieldComparator());
        }

    }

    private class OCRTemplateControlFieldComparator implements Comparator<OCRTemplateControlField> {
        private OCRTemplateControlFieldComparator() {
        }

        public int compare(OCRTemplateControlField arg0, OCRTemplateControlField arg1) {
            return arg0.getOrder() - arg1.getOrder();
        }
    }

    private class OCRTemplateControlParserComparator implements Comparator<OCRTemplateControlParser> {
        private OCRTemplateControlParserComparator() {
        }

        public int compare(OCRTemplateControlParser arg0, OCRTemplateControlParser arg1) {
            return arg0.getName().compareTo(arg1.getName());
        }
    }

    private class OCRTemplateParserComparator implements Comparator<OCRTemplateParser> {
        private OCRTemplateParserComparator() {
        }

        public int compare(OCRTemplateParser arg0, OCRTemplateParser arg1) {
            return arg0.getName().compareTo(arg1.getName());
        }
    }
}
