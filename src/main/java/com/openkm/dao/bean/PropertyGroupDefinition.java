//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.dao.bean;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(
        name = "OKM_PROPERTY_GROUP_DEF"
)
@Cache(
        usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE
)
public class PropertyGroupDefinition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(
            name = "PGD_ID"
    )
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private long id;
    @Column(
            name = "PGD_DEFINITION"
    )
    @Lob
    @Type(
            type = "org.hibernate.type.StringClobType"
    )
    private String definition;


    @Column(
            name = "PGD_NAME",
            length = 64
    )
    private String name;

    public PropertyGroupDefinition() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDefinition() {
        return this.definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("id=").append(this.id);
        sb.append("name=").append(this.name);
        sb.append(", definition=").append(this.definition);
        sb.append("}");
        return sb.toString();
    }
}
