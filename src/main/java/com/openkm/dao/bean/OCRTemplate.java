//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.dao.bean;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(
        name = "OKM_OCR_TEMPLATE"
)
public class OCRTemplate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(
            name = "OT_ID"
    )
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private long id;
    @Column(
            name = "OT_NAME",
            length = 255
    )
    private String name;
    @Column(
            name = "OT_IMAGE_CONTENT"
    )
    @Lob
    private byte[] imageContent;
    @Column(
            name = "OT_IMAGE_MIME",
            length = 32
    )
    private String imageMime;
    @Column(
            name = "OT_ACTIVE",
            nullable = false
    )
    @Type(
            type = "true_false"
    )
    private Boolean active;
    @OrderBy("id ASC")
    @OneToMany(
            cascade = {CascadeType.ALL}
    )
    @JoinColumn(
            name = "OTF_TEMPLATE",
            referencedColumnName = "OT_ID"
    )
    private List<OCRTemplateField> fields;
    @OrderBy("id ASC")
    @OneToMany(
            cascade = {CascadeType.ALL}
    )
    @JoinColumn(
            name = "OTCF_TEMPLATE",
            referencedColumnName = "OT_ID"
    )
    private List<OCRTemplateControlField> controlFields;
    @OrderBy("id ASC")
    @OneToMany(
            cascade = {CascadeType.ALL}
    )
    @JoinColumn(
            name = "OTP_TEMPLATE",
            referencedColumnName = "OT_ID"
    )
    private List<OCRTemplateProperty> properties;

    public OCRTemplate() {
        this.active = Boolean.FALSE;
        this.fields = new ArrayList();
        this.controlFields = new ArrayList();
        this.properties = new ArrayList();
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImageContent() {
        return this.imageContent;
    }

    public void setImageContent(byte[] imageContent) {
        this.imageContent = imageContent;
    }

    public String getImageMime() {
        return this.imageMime;
    }

    public void setImageMime(String imageMime) {
        this.imageMime = imageMime;
    }

    public Boolean getActive() {
        return this.active == null?Boolean.valueOf(false):this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<OCRTemplateField> getFields() {
        return this.fields;
    }

    public void setFields(List<OCRTemplateField> fields) {
        this.fields = fields;
    }

    public List<OCRTemplateControlField> getControlFields() {
        return this.controlFields;
    }

    public void setControlFields(List<OCRTemplateControlField> controlFields) {
        this.controlFields = controlFields;
    }

    public List<OCRTemplateProperty> getProperties() {
        return this.properties;
    }

    public void setProperties(List<OCRTemplateProperty> properties) {
        this.properties = properties;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("id=").append(this.id);
        sb.append(", name=").append(this.name);
        sb.append(", imageContent=").append("[BIG]");
        sb.append(", imageMime=").append(this.imageMime);
        sb.append(", active=").append(this.active);
        sb.append(", fields=").append(this.fields);
        sb.append(", controlFields=").append(this.controlFields);
        sb.append(", properties=").append(this.properties);
        sb.append("}");
        return sb.toString();
    }
}
