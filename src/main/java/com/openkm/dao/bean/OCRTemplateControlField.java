//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.dao.bean;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(
        name = "OKM_OCR_TEMPLATE_CONTROL_FIELD"
)
public class OCRTemplateControlField implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(
            name = "OTCF_ID"
    )
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private long id;
    @Column(
            name = "OTCF_NAME"
    )
    private String name;
    @Column(
            name = "OTCF_CLASS_NAME"
    )
    private String className;
    @Column(
            name = "OTCF_X1"
    )
    private int x1;
    @Column(
            name = "OTCF_Y1"
    )
    private int y1;
    @Column(
            name = "OTCF_X2"
    )
    private int x2;
    @Column(
            name = "OTCF_Y2"
    )
    private int y2;
    @Column(
            name = "OTCF_PATTERN"
    )
    private String pattern;
    @Column(
            name = "OTCF_VALUE"
    )
    private String value;
    @Column(
            name = "OTCF_ROTATION"
    )
    private int rotation;
    @Column(
            name = "OTCF_CUSTOM"
    )
    @Lob
    @Type(
            type = "org.hibernate.type.StringClobType"
    )
    private String custom;
    @Column(
            name = "OTCF_OCR"
    )
    private String ocr;
    @Column(
            name = "OTCF_ORDER"
    )
    private int order;
    @Column(
            name = "OTCF_EMPTY_ALLOWED"
    )
    @Type(
            type = "true_false"
    )
    private Boolean emptyAllowed;

    @Column(
            name = "OTCF_FILTER_PATTERN"
    )
    private String filterPattern;

    public OCRTemplateControlField() {
        this.emptyAllowed = Boolean.FALSE;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getX1() {
        return this.x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return this.y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return this.x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return this.y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public String getPattern() {
        return this.pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getRotation() {
        return this.rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public String getCustom() {
        return this.custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public String getOcr() {
        return this.ocr;
    }

    public void setOcr(String ocr) {
        this.ocr = ocr;
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Boolean getEmptyAllowed() {
        return this.emptyAllowed == null?Boolean.valueOf(false):this.emptyAllowed;
    }

    public void setEmptyAllowed(Boolean emptyAllowed) {
        this.emptyAllowed = emptyAllowed;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("id=").append(this.id);
        sb.append(", className=").append(this.className);
        sb.append(", name=").append(this.name);
        sb.append(", x1=").append(this.x1);
        sb.append(", y1=").append(this.y1);
        sb.append(", x2=").append(this.x2);
        sb.append(", y2=").append(this.y2);
        sb.append(", pattern=").append(this.pattern);
        sb.append(", value=").append(this.value);
        sb.append(", rotation=").append(this.rotation);
        sb.append(", custom=").append(this.custom);
        sb.append(", ocr=").append(this.ocr);
        sb.append(", order=").append(this.order);
        sb.append(", emptyAllowed=").append(this.emptyAllowed);
        sb.append(", filterPattern=").append(this.filterPattern);
        sb.append("}");
        return sb.toString();
    }

    public String getFilterPattern() {
        return filterPattern;
    }

    public void setFilterPattern(String filterPattern) {
        this.filterPattern = filterPattern;
    }
}
