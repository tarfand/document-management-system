//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.dao.bean;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(
        name = "OKM_OCR_TEMPLATE_PROPERTY"
)
public class OCRTemplateProperty implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(
            name = "OTP_ID"
    )
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private long id;
    @Column(
            name = "OTP_NAME"
    )
    private String name;
    @Column(
            name = "OTP_VALUE"
    )
    private String value;

    public OCRTemplateProperty() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("id=").append(this.id);
        sb.append(", name=").append(this.name);
        sb.append(", value=").append(this.value);
        sb.append("}");
        return sb.toString();
    }
}
