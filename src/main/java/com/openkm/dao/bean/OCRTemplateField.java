//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.dao.bean;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(
        name = "OKM_OCR_TEMPLATE_FIELD"
)
public class OCRTemplateField implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(
            name = "OTF_ID"
    )
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private long id;
    @Column(
            name = "OTF_NAME"
    )
    private String name;
    @Column(
            name = "OTF_CLASS_NAME"
    )
    private String className;
    @Column(
            name = "OTF_X1"
    )
    private int x1;
    @Column(
            name = "OTF_Y1"
    )
    private int y1;
    @Column(
            name = "OTF_X2"
    )
    private int x2;
    @Column(
            name = "OTF_Y2"
    )
    private int y2;
    @Column(
            name = "OTF_PG_NAME"
    )
    private String propertyGroupName;
    @Column(
            name = "OTF_PATTERN"
    )
    private String pattern;
    @Column(
            name = "OTF_ROTATION"
    )
    private int rotation;
    @Column(
            name = "OTF_CUSTOM"
    )
    @Lob
    @Type(
            type = "org.hibernate.type.StringClobType"
    )
    private String custom;
    @Column(
            name = "OTF_OCR"
    )
    private String ocr;
    @Column(
            name = "OTCF_USE_IN_CONTROL"
    )
    @Type(
            type = "true_false"
    )
    private Boolean control;
    @Column(
            name = "OTF_FILTER_PATTERN"
    )
    private String filterPattern;
    @Column(
            name = "OTF_WORDLIST_UUID"
    )
    private String wordListUUID;

    public OCRTemplateField() {
        this.control = Boolean.FALSE;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getX1() {
        return this.x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return this.y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return this.x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return this.y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public String getPropertyGroupName() {
        return this.propertyGroupName;
    }

    public void setPropertyGroupName(String propertyGroupName) {
        this.propertyGroupName = propertyGroupName;
    }

    public String getPattern() {
        return this.pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public int getRotation() {
        return this.rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public String getCustom() {
        return this.custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public String getOcr() {
        return this.ocr;
    }

    public void setOcr(String ocr) {
        this.ocr = ocr;
    }

    public Boolean getControl() {
        return this.control == null ? null : this.control;
    }

    public void setControl(Boolean control) {
        this.control = control;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("id=").append(this.id);
        sb.append(", className=").append(this.className);
        sb.append(", name=").append(this.name);
        sb.append(", x1=").append(this.x1);
        sb.append(", y1=").append(this.y1);
        sb.append(", x2=").append(this.x2);
        sb.append(", y2=").append(this.y2);
        sb.append(", propertyGroupName=").append(this.propertyGroupName);
        sb.append(", pattern=").append(this.pattern);
        sb.append(", rotation=").append(this.rotation);
        sb.append(", custom=").append(this.custom);
        sb.append(", ocr=").append(this.ocr);
        sb.append(", control=").append(this.control);
        sb.append(", filterPattern=").append(this.filterPattern);
        sb.append(", userWordList=").append(this.wordListUUID);
        sb.append("}");
        return sb.toString();
    }

    public String getFilterPattern() {
        return filterPattern;
    }

    public void setFilterPattern(String filterChars) {
        this.filterPattern = filterChars;
    }

    public String getWordListUUID() {
        return wordListUUID;
    }

    public void setWordListUUID(String userWordList) {
        this.wordListUUID = userWordList;
    }
}
