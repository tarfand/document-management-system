//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.dao;

import com.openkm.core.DatabaseException;
import com.openkm.dao.bean.PropertyGroupDefinition;
import com.openkm.spring.PrincipalUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PropertyGroupDefinitionDAO extends GenericDAO<PropertyGroupDefinition, Long> {
	private static Logger log = LoggerFactory.getLogger(PropertyGroupDefinitionDAO.class);
	private static PropertyGroupDefinitionDAO single = new PropertyGroupDefinitionDAO();

	private PropertyGroupDefinitionDAO() {
	}

	public static PropertyGroupDefinitionDAO getInstance() {
		return single;
	}

	public PropertyGroupDefinition findByPk(long pgdId) throws DatabaseException {
		log.debug("findByPk({})");
		String qs = "from PropertyGroupDefinition pgd where pgd.id = :pgdId";
		Session session = null;
		Object tx = null;

		PropertyGroupDefinition var10;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Query e = session.createQuery(qs).setCacheable(true);
			e.setLong("pgdId", pgdId);
			PropertyGroupDefinition ret = (PropertyGroupDefinition) e.setMaxResults(1).uniqueResult();
			log.debug("findByPk: {}", ret);
			var10 = ret;
		} catch (HibernateException var14) {
			HibernateUtil.rollback((Transaction) tx);
			throw new DatabaseException(var14.getMessage(), var14);
		} finally {
			HibernateUtil.close(session);
		}

		return var10;
	}

	public List<PropertyGroupDefinition> getAll() throws DatabaseException {
		log.debug("getAll({})");
		String qs = "from PropertyGroupDefinition ";
		Session session = null;
		Object tx = null;

		List<PropertyGroupDefinition> var8;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Query e = session.createQuery(qs).setCacheable(true);
			List<PropertyGroupDefinition> ret = e.list();
			log.debug("getAll: {}", ret);
			var8 = ret;
		} catch (HibernateException var12) {
			HibernateUtil.rollback((Transaction) tx);
			throw new DatabaseException(var12.getMessage(), var12);
		} finally {
			HibernateUtil.close(session);
		}

		return var8;
	}

	public void update(PropertyGroupDefinition pgd) throws DatabaseException {
		log.debug("update({})", pgd);
		String qs = "from PropertyGroupDefinition pgd where pgd.id=:id";
		Session session = null;
		Transaction tx = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			Query e = session.createQuery(qs);
			e.setParameter("id", Long.valueOf(pgd.getId()));
			session.update(pgd);
			HibernateUtil.commit(tx);
		} catch (HibernateException var11) {
			HibernateUtil.rollback(tx);
			throw new DatabaseException(var11.getMessage(), var11);
		} finally {
			HibernateUtil.close(session);
		}

		log.debug("update: void");
	}
	public List<PropertyGroupDefinition> findByTenant() throws DatabaseException {
		log.debug("findByTenant({})");
		String qs = "from PropertyGroupDefinition pgd";
		Session session = null;
		Object tx = null;

		List<PropertyGroupDefinition> var8;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Query e = session.createQuery(qs).setCacheable(true);
			List<PropertyGroupDefinition> ret =e.list();
			log.debug("findByTenant: {}", ret);
			var8 = ret;
		} catch (HibernateException var12) {
			HibernateUtil.rollback((Transaction)tx);
			throw new DatabaseException(var12.getMessage(), var12);
		} finally {
			HibernateUtil.close(session);
		}

		return var8;
	}
}
