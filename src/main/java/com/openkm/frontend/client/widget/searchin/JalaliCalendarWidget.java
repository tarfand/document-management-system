//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.frontend.client.widget.searchin;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.openkm.frontend.client.util.OKMDateConverter;

import java.util.Date;

public class JalaliCalendarWidget extends Composite implements HasChangeHandlers {
    private Date now = new Date();
    private final VerticalPanel calendarPanel = new VerticalPanel();

    private String elementId;
    private boolean isSetup = false;
    private String selectedJalaliDate = "";
    private String formatPattern = "%Y-%m-%d";

    public JalaliCalendarWidget(String elementId, String deliminator) {
        initWidget(calendarPanel);
        calendarPanel.getElement().setId(elementId);
        this.elementId = elementId;
        if (!deliminator.equals("")) {
            formatPattern.replace("-", deliminator);
        }
    }

    protected void onAttach() {
        super.onAttach();
        if (!this.isSetup) {
            this.setupCalandar(elementId, formatPattern);
        }

        this.isSetup = true;
    }

    private native void setupCalandar(String elementId, String formatPattern)
            /*-{
                var that = this;

                function dateChanged(calendar) {
                    //do some thing with the selected date
                    var x = calendar.date.print('%Y/%m/%d', 'jalali');
                    that.@com.openkm.frontend.client.widget.searchin.JalaliCalendarWidget::fireE(Ljava/lang/String;)(x);
                }

                $wnd.Calendar.setup({
                    flat: elementId,   // id of the input field
                    dateType: 'jalali',
                    flatCallback: dateChanged,
                    ifFormat: formatPattern
                });

            }-*/;

    public void setNow(Date now) {
        if (now == null) {
            now = new Date();
        }

        this.now = now;
    }

    public void fireE(String jalaliDate) {
        this.selectedJalaliDate = jalaliDate;
        if (getHandlerCount(ChangeEvent.getType()) > 0)
            fireChange();
    }

    private void fireChange() {
        NativeEvent nativeEvent = Document.get().createChangeEvent();
        ChangeEvent.fireNativeEvent(nativeEvent, this);
    }


    public Date getDate() {
        if (this.selectedJalaliDate != null && !this.selectedJalaliDate.equals("")) {
            OKMDateConverter okmDateConverter=new OKMDateConverter();
            this.now=okmDateConverter.jalaliToGregorian(selectedJalaliDate);

        }
        return this.now;
    }

    public String getJalaliDate() {
        return this.selectedJalaliDate;
    }

    public void langRefresh() {
    }

    @Override
    public HandlerRegistration addChangeHandler(ChangeHandler changeHandler) {
        return addDomHandler(changeHandler, ChangeEvent.getType());

    }

    public static String formatToMilitary(String date, String deliminator) {
        String one = "";
        String two = "";
        String three = "";

        one = date.split(deliminator)[0].length() < 2 ? "0" + date.split(deliminator)[0] : date.split(deliminator)[0];
        two = date.split(deliminator)[1].length() < 2 ? "0" + date.split(deliminator)[1] : date.split(deliminator)[1];
        three = date.split(deliminator)[2].length() < 2 ? "0" + date.split(deliminator)[2] : date.split(deliminator)[2];


        return one + deliminator + two + deliminator + three;
    }

}
