//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.frontend.client;

public class GWTException extends Exception {
    private static final long serialVersionUID = -4496726716603093472L;
    private String code;
    private String msg;

    public GWTException() {
    }

    public GWTException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.msg;
    }
}
