package com.openkm.frontend.client.util;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.google.gwt.i18n.client.DateTimeFormat;
import com.openkm.frontend.client.Main;
import java.util.Date;


public class OKMDateConverter {

	public OKMDateConverter() {
	}

	public String format(Date date) {
		return this.format(date, true);
	}


	public String format(Date date, boolean withTime) {

		DateTimeFormat dtf = DateTimeFormat.getFormat(Main.i18n("general.date.pattern"));
		String dateStr = dtf.format(date);

		String timePart = "";
		timePart = dateStr.split(" ")[1];
		String[] dateArr = dateStr.split(" ")[0].split("-");
		String s = withTime ? timePart + " " + gregorianToJalali(new Integer(dateArr[2]), new Integer(dateArr[1]), new Integer(dateArr[0])) : gregorianToJalali(new Integer(dateArr[2]), new Integer(dateArr[1]), new Integer(dateArr[0]));
		return s;
	}

	public static Date jalaliToGregorian(String selectedJalaliDate) {
		String[] jalaliArr = selectedJalaliDate.split("/");


		String gregorianStr = jalaliToGregorian((new Integer(jalaliArr[0])).intValue(), (new Integer(jalaliArr[1])).intValue(), (new Integer(jalaliArr[2])).intValue());
		DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy/MM/dd");
		return dtf.parse(gregorianStr);
	}

	public static String gregorianToJalali(String selectedgregorianDate) {
		String[] gregorianArr = selectedgregorianDate.split("-");
		String jalaliStr = gregorianToJalali((new Integer(gregorianArr[2])).intValue(), (new Integer(gregorianArr[1])).intValue(), (new Integer(gregorianArr[0])).intValue());
		return jalaliStr;
	}

	public static native String gregorianToJalali(int var0, int var1, int var2)/*-{
		return $wnd.JalaliDate.gregorianToJalaliWithSlash(var0, var1, var2);

	}-*/;

	public static native String jalaliToGregorian(int var0, int var1, int var2)/*-{
		return $wnd.JalaliDate.jalaliToGregorianWithSlash(var0, var1, var2);
	}-*/;
}
