package com.openkm.frontend.client.util;

/**
 * Created by vahid on 2/21/2017.
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import java.util.*;

public class FormatUtil {
    public FormatUtil() {
    }

    public static String escapeHtml(String str) {
        return str.replace("<", "&lt;").replace(">", "&gt;").replace("\n", "<br/>");
    }

    public static byte[] fixUTF8(byte[] input) {
        byte[] fixed = new byte[input.length];

        for (int i = 0; i < input.length; ++i) {
            if (input[i] == 0) {
                fixed[i] = 32;
            } else {
                fixed[i] = input[i];
            }
        }

        return fixed;
    }

    public static String fixUTF8(String input) {
        return input.replace('\u0000', ' ');
    }

    public static String fixArabicChars(String input) {
        return input.replace('ى', 'ی').replace('ي', 'ی').replace('ك', 'ک').replace('\u200c', ' ');
    }

    public static Set<String> fixArabicChars(Set<String> input) {
        HashSet tempSet = new HashSet();
        Iterator i$ = input.iterator();

        while (i$.hasNext()) {
            String s = (String) i$.next();
            tempSet.add(fixArabicChars(s));
        }

        return tempSet;
    }

    public static Map<String, String> fixArabicChars(Map<String, String> input) {
        HashMap tempMap = new HashMap();
        Iterator i$ = input.keySet().iterator();

        while (i$.hasNext()) {
            String s = (String) i$.next();
            tempMap.put(s, fixArabicChars((String) input.get(s)));
        }

        return tempMap;
    }

    public static String trimUnicodeSurrogates(String text) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < text.length(); ++i) {
            char ch = text.charAt(i);
            if (!Character.isHighSurrogate(ch) && !Character.isLowSurrogate(ch)) {
                sb.append(ch);
            }
        }

        return sb.toString();
    }

    public static String fixKeyword(String key) {
        key = fixArabicChars(key.trim()).replace(" ", "_");
        return key;
    }

    public static String fixPath(String path) {
        return path == null ? null : path.replace("/", "/&lrm;");
    }

    public static String validate(String input) {
        return input;
    }
}

