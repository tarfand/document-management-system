package com.openkm.automation.validation;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.openkm.automation.AutomationUtils;
import com.openkm.automation.Validation;
import com.openkm.dao.bean.NodeBase;
import com.openkm.dao.bean.NodeDocument;
import com.openkm.ocr.template.OCRTemplateUtils;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@PluginImplementation
public class IsOCRDataCaptureFile implements Validation {
    private static Logger log = LoggerFactory.getLogger(IsOCRDataCaptureFile.class);


    @Override
    public boolean isValid(Map<String, Object> env, Object... params) {
        NodeBase node = AutomationUtils.getNode(env);
        try {
            if (node instanceof NodeDocument) {
                String mimeType = ((NodeDocument) node).getMimeType();
                return OCRTemplateUtils.isMimeTypeSupported(mimeType);
            } else {
                return false;
            }
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return false;
        }
    }

    public boolean hasPost() {
        return true;
    }

    public boolean hasPre() {
        return false;
    }

    public String getName() {
        return "IsOCRDataCaptureFile";
    }

    public String getParamType00() {
        return "";
    }

    public String getParamSrc00() {
        return "";
    }

    public String getParamDesc00() {
        return "";
    }

    public String getParamType01() {
        return "";
    }

    public String getParamSrc01() {
        return "";
    }

    public String getParamDesc01() {
        return "";
    }

    public String getParamType02() {
        return "";
    }

    public String getParamSrc02() {
        return "";
    }

    public String getParamDesc02() {
        return "";
    }



}
