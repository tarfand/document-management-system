//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.automation.action;

import com.openkm.automation.Action;
import com.openkm.automation.AutomationUtils;
import com.openkm.core.*;
import com.openkm.dao.OCRTemplateDAO;
import com.openkm.dao.bean.NodeBase;
import com.openkm.dao.bean.NodeDocument;
import com.openkm.ocr.template.OCRTemplateException;
import com.openkm.ocr.template.OCRTemplateUtils;
import com.openkm.ocr.template.bean.OCRConfidenceLevel;
import com.openkm.ocr.template.bean.OCRRecognise;
import net.xeoh.plugins.base.annotations.PluginImplementation;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

@PluginImplementation
public class OCRDataCapture implements Action {


    public void executePre(Map<String, Object> env, Object... params) {
    }

    public void executePost(Map<String, Object> env, Object... params) throws LockException {
        NodeBase node = AutomationUtils.getNode(env);
        if (node instanceof NodeDocument) {
            NodeDocument doc = (NodeDocument) node;

            try {
                if (OCRTemplateUtils.isMimeTypeSupported(doc.getMimeType()) && OCRTemplateDAO.getInstance().findAllActive().size() > 0) {
                    OCRRecognise recognise = OCRTemplateUtils.recognize(doc.getUuid(), doc.getMimeType(), 3);
                    switch (recognise.getStatus()) {
                        case 0:
                        default:
                            break;
                        case 1:
                        case 2:
                            Iterator var6 = recognise.getConfidences().iterator();

                            while (var6.hasNext()) {
                                OCRConfidenceLevel ocl = (OCRConfidenceLevel) var6.next();
                                if (ocl.isRecognized()) {
                                    OCRTemplateUtils.captureData((String) null, doc.getUuid(), doc.getMimeType(), ocl.getTemplate());
                                }
                            }
                    }
                }
            } catch (DatabaseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (PathNotFoundException e) {
                e.printStackTrace();
            } catch (AccessDeniedException e) {
                e.printStackTrace();
            } catch (RepositoryException e) {
                e.printStackTrace();
            } catch (OCRTemplateException e) {
                e.printStackTrace();
            }
        }

    }


    public boolean hasPost() {
        return true;
    }

    public boolean hasPre() {
        return false;
    }

    public String getName() {
        return "OCRDataCapture";
    }

    public String getParamType00() {
        return "";
    }

    public String getParamSrc00() {
        return "";
    }

    public String getParamDesc00() {
        return "";
    }

    public String getParamType01() {
        return "";
    }

    public String getParamSrc01() {
        return "";
    }

    public String getParamDesc01() {
        return "";
    }

    public String getParamType02() {
        return "";
    }

    public String getParamSrc02() {
        return "";
    }

    public String getParamDesc02() {
        return "";
    }


}
