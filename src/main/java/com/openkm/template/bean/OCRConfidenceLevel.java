//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.template.bean;

import com.openkm.dao.bean.OCRTemplate;

import java.io.Serializable;

public class OCRConfidenceLevel implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int LEVEL_FAST = 1;
    public static final int LEVEL_MEDIUM = 2;
    public static final int LEVEL_SLOW = 3;
    private boolean recognized = false;
    private int cropZones = 0;
    private int cropZonesAvailable = 0;
    private int textExtracted = 0;
    private int textParsed = 0;
    private int percentage = 0;
    private OCRTemplate template;

    public OCRConfidenceLevel() {
    }

    public boolean isRecognized() {
        return this.recognized;
    }

    public void setRecognized(boolean recognized) {
        this.recognized = recognized;
    }

    public int getCropZones() {
        return this.cropZones;
    }

    public void setCropZones(int cropZones) {
        this.cropZones = cropZones;
    }

    public int getCropZonesAvailable() {
        return this.cropZonesAvailable;
    }

    public void setCropZonesAvailable(int cropZonesAvailable) {
        this.cropZonesAvailable = cropZonesAvailable;
    }

    public int getTextExtracted() {
        return this.textExtracted;
    }

    public void setTextExtracted(int textExtracted) {
        this.textExtracted = textExtracted;
    }

    public int getTextParsed() {
        return this.textParsed;
    }

    public void setTextParsed(int textParsed) {
        this.textParsed = textParsed;
    }

    public int getPercentage() {
        return this.percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public OCRTemplate getTemplate() {
        return this.template;
    }

    public void setTemplate(OCRTemplate template) {
        this.template = template;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("recognized=").append(this.recognized);
        sb.append(", cropZones=").append(this.cropZones);
        sb.append(", cropZonesAvailable=").append(this.cropZonesAvailable);
        sb.append(", textExtracted=").append(this.textExtracted);
        sb.append(", textParsed=").append(this.textParsed);
        sb.append(", percentage=").append(this.percentage);
        sb.append(", template=").append(this.template.toString());
        sb.append("}");
        return sb.toString();
    }
}
