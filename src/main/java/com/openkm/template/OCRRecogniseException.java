//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.template;

import com.openkm.core.OKMException;

public class OCRRecogniseException extends OKMException {
    private static final long serialVersionUID = 1L;

    public OCRRecogniseException() {
    }

    public OCRRecogniseException(String arg0) {
        super(arg0);
    }

    public OCRRecogniseException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public OCRRecogniseException(Throwable arg0) {
        super(arg0);
    }
}
