//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.template.controlparser;

import com.openkm.core.DatabaseException;
import com.openkm.dao.ConfigDAO;
import com.openkm.dao.bean.OCRTemplateControlField;
import com.openkm.ocr.template.OCRParserEmptyValueException;
import com.openkm.ocr.template.OCRTemplateControlParser;
import com.openkm.ocr.template.OCRTemplateException;
import com.openkm.util.AlgorithmUtils;
import net.xeoh.plugins.base.annotations.PluginImplementation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@PluginImplementation
public class StringSimilarParser implements OCRTemplateControlParser {
    public StringSimilarParser() {
    }

    public boolean parse(OCRTemplateControlField otf, String text) throws OCRTemplateException, OCRParserEmptyValueException {
        if(text != null && !text.equals("")) {
            if(otf.getPattern() != null && !otf.getPattern().equals("")) {
                Pattern pattern1 = Pattern.compile("(" + otf.getPattern() + ")", 64);
                Matcher matcher = pattern1.matcher(text);
                if(matcher.find() && matcher.groupCount() == 1) {
                    if(otf.getValue().equals("")) {
                        return true;
                    } else {
                        int distance = AlgorithmUtils.computeLevenshteinDistance(matcher.group(), otf.getValue());

                        try {
                            return distance < ConfigDAO.getInteger("ocr.control.similar.distance", 5);
                        } catch (DatabaseException var7) {
                            throw new OCRTemplateException(var7);
                        }
                    }
                } else {
                    return false;
                }
            } else {
                int pattern = AlgorithmUtils.computeLevenshteinDistance(text, otf.getValue());

                try {
                    return pattern < ConfigDAO.getInteger("ocr.control.similar.distance", 5);
                } catch (DatabaseException var8) {
                    throw new OCRTemplateException(var8);
                }
            }
        } else {
            return false;
        }
    }

    public String getName() {
        return "Similar String";
    }

    public boolean isPatternRequired() {
        return false;
    }

    public String info() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empty pattern case: true if extracted text similar field value.\n");
        sb.append("Pattern with value field empty: true if pattern found.\n");
        sb.append("Pattern with value field: true if pattern found similar value.\n");
        return null;
    }
}
