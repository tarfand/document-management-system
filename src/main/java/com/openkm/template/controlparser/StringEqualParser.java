//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.template.controlparser;

import com.openkm.dao.bean.OCRTemplateControlField;
import com.openkm.ocr.template.OCRParserEmptyValueException;
import com.openkm.ocr.template.OCRTemplateControlParser;
import com.openkm.ocr.template.OCRTemplateException;
import net.xeoh.plugins.base.annotations.PluginImplementation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@PluginImplementation
public class StringEqualParser implements OCRTemplateControlParser {
    public StringEqualParser() {
    }

    public boolean parse(OCRTemplateControlField otf, String text) throws OCRTemplateException, OCRParserEmptyValueException {
        if(text != null && !text.equals("")) {
            if(otf.getPattern() != null && !otf.getPattern().equals("")) {
                Pattern pattern = Pattern.compile("(" + otf.getPattern() + ")", 64);
                Matcher matcher = pattern.matcher(text);
                return matcher.find() && matcher.groupCount() == 1?(otf.getValue().equals("")?true:otf.getValue().equals(matcher.group())):false;
            } else {
                return text.equals(otf.getValue());
            }
        } else {
            return false;
        }
    }

    public String getName() {
        return "String equals";
    }

    public boolean isPatternRequired() {
        return false;
    }

    public String info() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empty pattern case: true if extracted text equals field value.\n");
        sb.append("Pattern with value field empty: true if pattern found.\n");
        sb.append("Pattern with value field: true if pattern found equals value.\n");
        return null;
    }
}
