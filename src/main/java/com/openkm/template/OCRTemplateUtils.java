//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.template;

import com.openkm.api.OKMDocument;
import com.openkm.api.OKMPropertyGroup;
import com.openkm.api.OKMRepository;
import com.openkm.bean.form.FormElement;
import com.openkm.bean.form.Input;
import com.openkm.core.*;
import com.openkm.dao.OCRTemplateDAO;
import com.openkm.dao.bean.OCRTemplate;
import com.openkm.dao.bean.OCRTemplateControlField;
import com.openkm.dao.bean.OCRTemplateField;
import com.openkm.dao.bean.OCRTemplateProperty;
import com.openkm.extension.core.ExtensionException;
import com.openkm.ocr.template.bean.OCRConfidenceLevel;
import com.openkm.ocr.template.bean.OCRRecognise;
import com.openkm.ocr.template.parser.BarcodeParser;
import com.openkm.plugin.extractor.BarcodeTextExtractor;
import com.openkm.plugin.extractor.OCRTextExtractor;
import com.openkm.util.ISO8601;
import com.openkm.util.ImageUtils;
import com.openkm.util.PropertyGroupUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

public class OCRTemplateUtils {
    private static Logger log = LoggerFactory.getLogger(OCRTemplateUtils.class);

    public OCRTemplateUtils() {
    }

    public static String extractText(OCRTemplateField otf, byte[] img) throws OCRTemplateException {
        log.debug("extractText({}, {})", otf, Integer.valueOf(img.length));
        return extractText(otf.getX1(), otf.getY1(), otf.getX2(), otf.getY2(), otf.getRotation(), otf.getOcr(), otf.getClassName(), otf.getCustom(), img);
    }

    public static String extractText(OCRTemplateControlField otf, byte[] img) throws OCRTemplateException {
        log.debug("extractText({}, {})", otf, Integer.valueOf(img.length));
        return extractText(otf.getX1(), otf.getY1(), otf.getX2(), otf.getY2(), otf.getRotation(), otf.getOcr(), otf.getClassName(), otf.getCustom(), img);
    }

    private static String extractText(int x1, int y1, int x2, int y2, int rotation, String ocr, String className, String custom, byte[] img) throws OCRTemplateException {
        ByteArrayInputStream bais = null;

        String var12;
        try {
            img = ImageUtils.custom(img, ImageUtils.getFormatMimeType(img), custom);
            byte[] e = ImageUtils.crop(img, x1, y1, x2 - x1, y2 - y1);
            byte[] rotatedImg = ImageUtils.rotate(e, (double) rotation);
            bais = new ByteArrayInputStream(rotatedImg);
            if (!BarcodeParser.class.getCanonicalName().equals(className)) {
                if (ocr != null && !ocr.isEmpty()) {
                    var12 = (new OCRTextExtractor()).extractText(ocr, bais, ImageUtils.getFormatMimeType(img), (String) null);
                    return var12;
                }

                var12 = (new OCRTextExtractor()).extractText(bais, ImageUtils.getFormatMimeType(img), (String) null);
                return var12;
            }

            var12 = (new BarcodeTextExtractor()).extractText(bais, ImageUtils.getFormatMimeType(img), (String) null);
        } catch (RasterFormatException var19) {
            throw new OCRTemplateException("Raster format exception", var19);
        } catch (FileNotFoundException var20) {
            throw new OCRTemplateException("File not found exception", var20);
        } catch (IOException var21) {
            throw new OCRTemplateException("IOException: " + var21.getMessage(), var21);
        } catch (DatabaseException var22) {
            throw new OCRTemplateException("DatabaseException: " + var22.getMessage(), var22);
        } finally {
            IOUtils.closeQuietly(bais);
        }

        return var12;
    }

    public static Object parse(OCRTemplateField otf, String text) throws OCRTemplateException, OCRParserEmptyValueException, com.openkm.ocr.template.OCRTemplateException, com.openkm.ocr.template.OCRParserEmptyValueException {
        log.debug("parse({}, {})", otf, text);
        return OCRTemplateDAO.getInstance().findParserByClassName(otf.getClassName()).parse(otf, text);
    }

    public static boolean parse(OCRTemplateControlField otcf, String text) throws OCRTemplateException, OCRParserEmptyValueException, com.openkm.ocr.template.OCRTemplateException, com.openkm.ocr.template.OCRParserEmptyValueException {
        log.debug("parse({}, {})", otcf, text);
        return OCRTemplateDAO.getInstance().findControlParserByClassName(otcf.getClassName()).parse(otcf, text);
    }

    public static String toString(Object value, String pattern) {
        log.debug("toString({}, {})", value, pattern);
        if (value instanceof Calendar) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.format(((Calendar) value).getTime());
        } else {
            return String.valueOf(value);
        }
    }

    public static OCRRecognise recognize(byte[] img, String mimeType, int level) throws DatabaseException, IOException, OCRTemplateException, com.openkm.ocr.template.OCRTemplateException, com.openkm.ocr.template.OCRParserEmptyValueException {
        return recognize(img, mimeType, level, false);
    }

    public static OCRRecognise recognize(byte[] img, String mimeType, int level, boolean check) throws DatabaseException, IOException, OCRTemplateException, com.openkm.ocr.template.OCRTemplateException, com.openkm.ocr.template.OCRParserEmptyValueException {
        img = convertToSupportedFormat(img, mimeType);
        OCRRecognise ocrRecognise = new OCRRecognise();
        ArrayList oclList = new ArrayList();
        ByteArrayInputStream in = new ByteArrayInputStream(img);
        BufferedImage image = ImageIO.read(in);
        int height = image.getHeight();
        int width = image.getWidth();
        int templatesFound = 0;
        IOUtils.closeQuietly(in);

        OCRConfidenceLevel ocl1;
        for (Iterator found = OCRTemplateDAO.getInstance().findAllActive().iterator(); found.hasNext(); oclList.add(ocl1)) {
            OCRTemplate ot = (OCRTemplate) found.next();
            boolean ocl = false;
            int zoneAvailable = 0;
            int textExtracted = 0;
            int textParsed = 0;
            ocl1 = new OCRConfidenceLevel();
            ocl1.setTemplate(ot);
            ArrayList fieldsWidthControl = new ArrayList();
            Iterator percentage = ot.getFields().iterator();

            OCRTemplateField otf;
            while (percentage.hasNext()) {
                otf = (OCRTemplateField) percentage.next();
                if (otf.getControl().booleanValue()) {
                    fieldsWidthControl.add(otf);
                }
            }

            ocl1.setCropZones(ot.getControlFields().size() + fieldsWidthControl.size());
            percentage = ot.getControlFields().iterator();

            String text;
            label180:
            while (true) {
                boolean e;
                OCRTemplateControlField var35;
                do {
                    while (true) {
                        label148:
                        do {
                            do {
                                if (!percentage.hasNext()) {
                                    break label180;
                                }

                                var35 = (OCRTemplateControlField) percentage.next();
                                if (var35.getX2() <= width && var35.getY2() <= height) {
                                    ++zoneAvailable;
                                    continue label148;
                                }
                            } while (check);

                            ocl = true;
                            break label180;
                        } while (level < 1);

                        text = null;

                        try {
                            text = extractText(var35, img);
                        } catch (OCRTemplateException var23) {
                            log.error(var23.getMessage());
                            if (!check) {
                                ocl = true;
                                break label180;
                            }
                        }

                        if (var35.getEmptyAllowed().booleanValue() && (text == null || text.isEmpty())) {
                            ++textExtracted;
                            break;
                        }

                        if (text != null && !text.isEmpty()) {
                            ++textExtracted;
                            if (level >= 2) {
                                try {
                                    e = parse(var35, text);
                                    if (!e) {
                                        ocl = true;
                                        break label180;
                                    }

                                    ++textParsed;
                                } catch (Exception var24) {
                                    log.error(var24.getMessage());
                                    if (!check) {
                                        ocl = true;
                                        break label180;
                                    }
                                }
                            }
                        } else if (!check) {
                            ocl = true;
                            break label180;
                        }
                    }
                } while (level < 2);

                try {
                    e = parse(var35, "");
                    if (!e) {
                        ocl = true;
                        break;
                    }

                    ++textParsed;
                } catch (Exception var25) {
                    log.error(var25.getMessage());
                    if (!check) {
                        ocl = true;
                        break;
                    }
                }
            }

            if (!ocl) {
                percentage = fieldsWidthControl.iterator();

                label226:
                while (true) {
                    label214:
                    do {
                        do {
                            label198:
                            do {
                                do {
                                    if (!percentage.hasNext()) {
                                        break label226;
                                    }

                                    otf = (OCRTemplateField) percentage.next();
                                    if (otf.getX2() <= width && otf.getY2() <= height) {
                                        ++zoneAvailable;
                                        continue label198;
                                    }
                                } while (check);

                                ocl = true;
                                break label226;
                            } while (level < 1);

                            text = null;

                            try {
                                text = extractText(otf, img);
                            } catch (OCRTemplateException var26) {
                                log.error(var26.getMessage());
                                if (!check) {
                                    ocl = true;
                                    break label226;
                                }
                            }

                            if (text != null && !text.isEmpty()) {
                                ++textExtracted;
                                continue label214;
                            }
                        } while (check);

                        ocl = true;
                        break label226;
                    } while (level < 2);

                    try {
                        parse(otf, text);
                        ++textParsed;
                    } catch (IllegalArgumentException var27) {
                        log.error(var27.getMessage());
                        if (!check) {
                            ocl = true;
                            break;
                        }
                    } catch (SecurityException var28) {
                        log.error(var28.getMessage());
                        if (!check) {
                            ocl = true;
                            break;
                        }
                    } catch (OCRTemplateException var29) {
                        log.error(var29.getMessage());
                        if (!check) {
                            ocl = true;
                            break;
                        }
                    } catch (OCRParserEmptyValueException var30) {
                        log.error(var30.getMessage());
                        if (!check) {
                            ocl = true;
                            break;
                        }
                    }
                }
            }

            ocl1.setCropZonesAvailable(zoneAvailable);
            ocl1.setTextExtracted(textExtracted);
            ocl1.setTextParsed(textParsed);
            int var34 = evaluateConfidenceLevel(ocl1, level);
            ocl1.setPercentage(var34);
            if (var34 == 100) {
                ocl1.setRecognized(true);
                ++templatesFound;
            }
        }

        if (level >= 3 && templatesFound != 0 && templatesFound > 1) {
            ;
        }

        ocrRecognise.setConfidences(oclList);
        int var31 = 0;
        Iterator var32 = oclList.iterator();

        while (var32.hasNext()) {
            OCRConfidenceLevel var33 = (OCRConfidenceLevel) var32.next();
            if (var33.isRecognized()) {
                ++var31;
            }
        }

        switch (var31) {
            case 0:
                ocrRecognise.setStatus(0);
                break;
            case 1:
                ocrRecognise.setStatus(1);
                break;
            default:
                ocrRecognise.setStatus(2);
        }

        return ocrRecognise;
    }

    public static OCRRecognise recognize(String uuid, String mimeType, int level) throws DatabaseException, IOException, PathNotFoundException, AccessDeniedException, RepositoryException, OCRTemplateException, LockException, com.openkm.ocr.template.OCRTemplateException, com.openkm.ocr.template.OCRParserEmptyValueException {
        InputStream is = OKMDocument.getInstance().getContent((String) null, uuid, false);
        byte[] img = IOUtils.toByteArray(is);
        return recognize((byte[]) img, mimeType, 3);
    }

    private static int evaluateConfidenceLevel(OCRConfidenceLevel ocl, int level) {
        long controlPoints = 0L;
        Iterator positive = ocl.getTemplate().getFields().iterator();

        while (positive.hasNext()) {
            OCRTemplateField otf = (OCRTemplateField) positive.next();
            if (otf.getControl().booleanValue()) {
                ++controlPoints;
            }
        }

        controlPoints += (long) ocl.getTemplate().getControlFields().size();
        controlPoints *= (long) level;
        int var6 = 0;
        switch (level) {
            case 3:
                var6 = ocl.getTextParsed();
            case 2:
                var6 += ocl.getTextExtracted();
            case 1:
                var6 += ocl.getCropZonesAvailable();
                if (var6 == 0) {
                    return 0;
                }

                return (new Double((double) ((long) (var6 * 100) / controlPoints))).intValue();
            default:
                return 0;
        }
    }

    public static void captureData(String token, String uuid, byte[] img, OCRTemplate ot) throws OCRTemplateException {
        boolean error = false;
        HashMap resultsMap = new HashMap();

        try {
            String e = OKMRepository.getInstance().getNodePath(token, uuid);

            Iterator groups;
            OCRTemplateField tplProp;
            Object grpName;
            for (groups = ot.getFields().iterator(); groups.hasNext(); resultsMap.put(tplProp.getPropertyGroupName(), grpName)) {
                tplProp = (OCRTemplateField) groups.next();
                grpName = new String("");

                try {
                    String properties = extractText(tplProp, img);
                    grpName = parse(tplProp, properties);
                } catch (IllegalArgumentException var13) {
                    log.error(var13.getMessage());
                    error = true;
                } catch (SecurityException var14) {
                    log.error(var14.getMessage());
                    error = true;
                } catch (OCRTemplateException var15) {
                    log.error(var15.getMessage());
                    error = true;
                } catch (OCRParserEmptyValueException var16) {
                    log.error(var16.getMessage());
                    error = true;
                }
            }

            groups = ot.getProperties().iterator();

            while (groups.hasNext()) {
                OCRTemplateProperty tplProp1 = (OCRTemplateProperty) groups.next();
                resultsMap.put(tplProp1.getName(), tplProp1.getValue());
            }

            List groups1 = PropertyGroupUtils.getRelatedGroupsFromProperties(new ArrayList(resultsMap.keySet()));
            Iterator tplProp2 = groups1.iterator();

            String grpName1;
			List<FormElement> elements = new ArrayList<>();

			while (tplProp2.hasNext()) {
                grpName1 = (String) tplProp2.next();
                Map<String, String> properties = new HashMap();
                Iterator var12 = resultsMap.keySet().iterator();
                while (var12.hasNext()) {
                    String key = (String) var12.next();
					if (PropertyGroupUtils.propertyNameContainsGroup(grpName1, key)) {
//						properties.put(key, toPropertyGroupValue(resultsMap.get(key)));
						Input element = new Input();
						element.setHeight("100");
						element.setWidth("100");
						element.setName(key);
						String v = toPropertyGroupValue(resultsMap.get(key));
						element.setValue(v);
						elements.add(element);
					}
                }

                if (!OKMPropertyGroup.getInstance().hasGroup(token, uuid, grpName1)) {
                    OKMPropertyGroup.getInstance().addGroup(token, e, grpName1);
                } else {
                    OKMPropertyGroup.getInstance().setProperties(token, e, grpName1, elements);
                }

            }
            if (error) {
                throw new OCRTemplateException("Error capturing field");
            }
        } catch (IOException | OKMException | IllegalArgumentException var15) {
            log.error(var15.getMessage());
            throw new OCRTemplateException(var15);
        } catch (SecurityException var16) {
            log.error(var16.getMessage());
            throw new OCRTemplateException(var16);
        } catch (RepositoryException e) {
            e.printStackTrace();
        } catch (DatabaseException e) {
            log.error(e.getMessage());
            throw new OCRTemplateException(e);
        } catch (ParseException e) {
            log.error(e.getMessage());
            throw new OCRTemplateException(e);
        } catch (NoSuchGroupException e) {
            log.error(e.getMessage());
            throw new OCRTemplateException(e);
        } catch (LockException e) {
            log.error(e.getMessage());
            throw new OCRTemplateException(e);
        } catch (AccessDeniedException e) {
            e.printStackTrace();
        } catch (PathNotFoundException e) {
            log.error(e.getMessage());
            throw new OCRTemplateException(e);
        } catch (ExtensionException | NoSuchPropertyException e) {
            log.error(e.getMessage());
            throw new OCRTemplateException(e);
        }
    }

    public static void captureData(String token, String uuid, String mimeType, OCRTemplate ot) throws OCRTemplateException, LockException {
        InputStream is = null;

        try {
            is = OKMDocument.getInstance().getContent((String) null, uuid, false);
            byte[] e = IOUtils.toByteArray(is);
            e = convertToSupportedFormat(e, mimeType);
            captureData(token, uuid, e, ot);
        } catch (PathNotFoundException var13) {
            log.error(var13.getMessage());
            throw new OCRTemplateException(var13);
        } catch (AccessDeniedException var14) {
            log.error(var14.getMessage());
            throw new OCRTemplateException(var14);
        } catch (RepositoryException var15) {
            log.error(var15.getMessage());
            throw new OCRTemplateException(var15);
        } catch (IOException var16) {
            log.error(var16.getMessage());
            throw new OCRTemplateException(var16);
        } catch (DatabaseException var17) {
            log.error(var17.getMessage());
            throw new OCRTemplateException(var17);
        } finally {
            IOUtils.closeQuietly(is);
        }

    }

    public static String toPropertyGroupValue(Object value) {
        log.debug("toString({})", value);
        return value instanceof Calendar ? ISO8601.formatBasic((Calendar) value) : String.valueOf(value);
    }

    public static boolean isMimeTypeSupported(String mimeType) {
        return mimeType.equals("image/png") || mimeType.equals("image/gif") || mimeType.equals("image/jpeg") || mimeType.equals("image/bmp") || mimeType.equals("image/tiff") || mimeType.equals("application/pdf");
    }

    public static byte[] convertToSupportedFormat(byte[] img, String mimeType) throws OCRTemplateException {
        if (!mimeType.equals("image/png") && !mimeType.equals("image/gif") && !mimeType.equals("image/jpeg") && !mimeType.equals("image/bmp")) {
            if (mimeType.equals("image/tiff")) {
                return ImageUtils.convert(img, "image/png", "${fileIn}[0] ${fileOut}");
            } else if (mimeType.equals("application/pdf")) {
                return ImageUtils.convert(img, "image/jpeg", "-quality 100 -density 300 ${fileIn}[0] ${fileOut}");
            } else {
                throw new OCRTemplateException("not supported mime type");
            }
        } else {
            return img;
        }
    }
}
