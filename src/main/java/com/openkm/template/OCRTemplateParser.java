//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.template;

import com.openkm.dao.bean.OCRTemplateField;
import net.xeoh.plugins.base.Plugin;

public interface OCRTemplateParser extends Plugin {
    Object parse(OCRTemplateField var1, String var2) throws OCRTemplateException, OCRParserEmptyValueException;

    String getName();

    boolean isPatternRequired();
}
