//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

import com.openkm.core.Config;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@PluginImplementation
public class BarcodeTextExtractor extends AbstractTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(BarcodeTextExtractor.class);

    public BarcodeTextExtractor() {
        super(new String[]{"image/tiff", "image/gif", "image/jpeg", "image/png"});
    }

    public BarcodeTextExtractor(String[] contentTypes) {
        super(contentTypes);
    }

    public String extractText(InputStream stream, String type, String encoding) throws IOException {
        return Config.SYSTEM_ZBAR.toLowerCase().contains("zbarimg")?(new ZBarTextExtractor()).extractText(stream, type, encoding):(new ZXingTextExtractor()).extractText(stream, type, encoding);
    }

    public String extractText(File input) throws IOException {
        return Config.SYSTEM_ZBAR.toLowerCase().contains("zbarimg")?(new ZBarTextExtractor()).extractText(input):(new ZXingTextExtractor()).extractText(input);
    }
}
