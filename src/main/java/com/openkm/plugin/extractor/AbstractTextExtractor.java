//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

public abstract class AbstractTextExtractor implements TextExtractor {
    private final String[] contentTypes;

    public AbstractTextExtractor(String[] contentTypes) {
        this.contentTypes = new String[contentTypes.length];
        System.arraycopy(contentTypes, 0, this.contentTypes, 0, contentTypes.length);
    }

    public String[] getContentTypes() {
        return this.contentTypes;
    }
}
