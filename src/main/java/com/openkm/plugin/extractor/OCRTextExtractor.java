//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

import com.openkm.core.Config;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@PluginImplementation
public class OCRTextExtractor extends AbstractTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(OCRTextExtractor.class);

    public OCRTextExtractor() {
        super(new String[]{"image/tiff", "image/gif", "image/jpg", "image/jpeg", "image/png"});
    }

    public String extractText(InputStream stream, String type, String encoding) throws IOException {
        if(Config.SYSTEM_OCR.toLowerCase().contains("cuneiform")) {
            return (new CuneiformTextExtractor()).extractText(stream, type, encoding);
        } else if(Config.SYSTEM_OCR.toLowerCase().contains("tesseract")) {
            return (new Tesseract3TextExtractor()).extractText(stream, type, encoding);
        } else if(Config.SYSTEM_OCR.toLowerCase().contains("abby")) {
            return (new AbbyTextExtractor()).extractText(stream, type, encoding);
        } else {
            throw new IOException("Unsupported OCR engine: " + Config.SYSTEM_OCR);
        }
    }

    public String extractText(File input) throws IOException {
        if(Config.SYSTEM_OCR.isEmpty()) {
            throw new IOException("OCR engine not configured: system.ocr");
        } else if(Config.SYSTEM_OCR.toLowerCase().contains("cuneiform")) {
            return (new CuneiformTextExtractor()).extractText(input);
        } else if(Config.SYSTEM_OCR.toLowerCase().contains("tesseract")) {
            return (new Tesseract3TextExtractor()).extractText(input);
        } else if(Config.SYSTEM_OCR.toLowerCase().contains("abby")) {
            return (new AbbyTextExtractor()).extractText(input);
        } else {
            throw new IOException("Unsupported OCR engine: " + Config.SYSTEM_OCR);
        }
    }

    public String extractText(String ocr, InputStream stream, String type, String encoding) throws IOException {
        if(Config.SYSTEM_OCR.isEmpty()) {
            throw new IOException("OCR engine not configured: system.ocr");
        } else if(ocr.toLowerCase().contains("tesseract")) {
            return (new Tesseract3TextExtractor()).extractText(ocr, stream, type, encoding);
        } else {
            throw new IOException("Unsupported OCR engine: " + ocr);
        }
    }

    public String extractText(String ocr, File input) throws IOException {
        if(ocr.toLowerCase().contains("tesseract")) {
            return (new Tesseract3TextExtractor()).extractText(ocr, input);
        } else {
            throw new IOException("Unsupported OCR engine: " + ocr);
        }
    }
}
