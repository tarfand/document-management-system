//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

import com.openkm.core.Config;
import com.openkm.core.DatabaseException;
import com.openkm.util.*;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;

@PluginImplementation
public class Tesseract3TextExtractor extends AbstractTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(Tesseract3TextExtractor.class);

    public Tesseract3TextExtractor() {
        super(new String[]{"image/tiff", "image/gif", "image/jpg", "image/jpeg", "image/png"});
    }

    public String extractText(InputStream stream, String type, String encoding) throws IOException {
        return this.extractText(Config.SYSTEM_OCR, stream, type, encoding);
    }

    public String extractText(File input) throws IOException {
        return this.extractText(Config.SYSTEM_OCR, input);
    }

    public String extractText(String ocr, InputStream stream, String type, String encoding) throws IOException {
        File tmpFileIn = null;

        String var8;
        try {
            tmpFileIn = FileUtils.createTempFileFromMime(type);
            FileOutputStream e = new FileOutputStream(tmpFileIn);
            IOUtils.copy(stream, e);
            e.close();
            String text = this.extractText(ocr, tmpFileIn);
            var8 = text;
        } catch (DatabaseException var12) {
            log.warn("Failed to extract barcode text", var12);
            throw new IOException(var12.getMessage(), var12);
        } finally {
            IOUtils.closeQuietly(stream);
            FileUtils.deleteQuietly(tmpFileIn);
        }

        return var8;
    }

    public String extractText(String ocr, File input) throws IOException {
        if(ocr.isEmpty()) {
            log.warn("Undefined OCR application");
            throw new IOException("Undefined OCR application");
        } else {
            StringBuilder sb = new StringBuilder();
            String text = this.doOcr(ocr, input);
            sb.append(text);
            if(!Config.SYSTEM_OCR_ROTATE.isEmpty()) {
                String[] angles = Config.SYSTEM_OCR_ROTATE.split(Config.LIST_SEPARATOR);
                String[] var6 = angles;
                int var7 = angles.length;

                for(int var8 = 0; var8 < var7; ++var8) {
                    String angle = var6[var8];
                    log.debug("Rotate image {} degrees", angle);
                    double degree = Double.parseDouble(angle);
                    ImageUtils.rotate(input, input, degree);
                    text = this.doOcr(ocr, input);
                    sb.append("\n-----------------------------\n");
                    sb.append(text);
                }
            }

            return sb.toString();
        }
    }

    public String doOcr(File tmpFileIn) throws IOException {
        return this.doOcr(Config.SYSTEM_OCR, tmpFileIn);
    }

    public String doOcr(String ocr, File tmpFileIn) throws IOException {
        Object stdout = null;
        FileInputStream fis = null;
        File tmpFileOut = null;
        String cmd = null;
        if(ocr.isEmpty()) {
            log.warn("Undefined OCR application");
            throw new IOException("Undefined OCR application");
        } else {
            String var9;
            try {
                tmpFileOut = File.createTempFile("okm", "");
                HashMap e = new HashMap();
                e.put("fileIn", tmpFileIn.getPath());
                e.put("fileOut", tmpFileOut.getPath());
                e.put("SYSTEM_TESSERACT_OCR", Config.SYSTEM_TESSERACT_OCR);
                cmd = TemplateUtils.replace("SYSTEM_OCR", ocr, e);
                ExecutionUtils.runCmd(cmd);
                fis = new FileInputStream(tmpFileOut.getPath() + ".txt");
                String text = IOUtils.toString(fis, "UTF-8");
                if(Config.SYSTEM_OCR_SPELL_CHECK && Config.SYSTEM_OPENOFFICE_DICTIONARY.isEmpty()) {
                    log.debug("TEXT: {}", text);
                    var9 = text;
                    return var9;
                }

                text = DocumentUtils.spellChecker(text);
                log.debug("TEXT: {}", text);
                var9 = text;
            } catch (SecurityException var16) {
                log.warn("Security exception executing command: " + cmd, var16);
                throw new IOException(var16.getMessage(), var16);
            } catch (IOException var17) {
                log.warn("IO exception executing command: " + cmd, var17);
                throw new IOException(var17.getMessage(), var17);
            } catch (InterruptedException var18) {
                log.warn("Interrupted exception executing command: " + cmd, var18);
                throw new IOException(var18.getMessage(), var18);
            } catch (Exception var19) {
                log.warn("Failed to extract OCR text", var19);
                throw new IOException(var19.getMessage(), var19);
            } finally {
                IOUtils.closeQuietly(fis);
                IOUtils.closeQuietly((Reader)stdout);
                FileUtils.deleteQuietly(tmpFileOut);
                if(tmpFileOut != null) {
                    FileUtils.deleteQuietly(new File(tmpFileOut.getPath() + ".txt"));
                }

            }

            return var9;
        }
    }
}
