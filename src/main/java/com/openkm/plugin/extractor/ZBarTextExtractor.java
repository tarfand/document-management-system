//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

import com.openkm.bean.ExecutionResult;
import com.openkm.core.Config;
import com.openkm.core.DatabaseException;
import com.openkm.util.ExecutionUtils;
import com.openkm.util.FileUtils;
import com.openkm.util.SystemProfiling;
import com.openkm.util.TemplateUtils;
import freemarker.template.TemplateException;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

@PluginImplementation
public class ZBarTextExtractor extends AbstractTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(ZBarTextExtractor.class);

    public ZBarTextExtractor() {
        super(new String[]{"image/tiff", "image/gif", "image/jpeg", "image/png"});
    }

    public ZBarTextExtractor(String[] contentTypes) {
        super(contentTypes);
    }

    public String extractText(InputStream stream, String type, String encoding) throws IOException {
        FileOutputStream tmpInFos = null;
        File tmpFileIn = null;

        String var7;
        try {
            tmpFileIn = FileUtils.createTempFileFromMime(type);
            tmpInFos = new FileOutputStream(tmpFileIn);
            IOUtils.copy(stream, tmpInFos);
            tmpInFos.flush();
            tmpInFos.close();
            String e = this.extractText(tmpFileIn);
            var7 = e;
        } catch (DatabaseException var11) {
            log.warn("Failed to extract barcode text", var11);
            throw new IOException(var11.getMessage(), var11);
        } finally {
            FileUtils.deleteQuietly(tmpFileIn);
            IOUtils.closeQuietly(tmpInFos);
            IOUtils.closeQuietly(stream);
        }

        return var7;
    }

    public String extractText(File input) throws IOException {
        try {
            return this.multiple(input);
        } catch (Exception var3) {
            log.warn("Failed to extract barcode text", var3);
            throw new IOException(var3.getMessage(), var3);
        }
    }

    private String multiple(File input) throws RuntimeException {
        long begin = System.currentTimeMillis();
        String cmd = null;
        String ret = null;

        try {
            HashMap e = new HashMap();
            e.put("fileIn", input.getPath());
            String tpl = Config.SYSTEM_ZBAR + " -q --raw ${fileIn}";
            cmd = TemplateUtils.replace("IMAGE_CONVERT", tpl, e);
            ExecutionResult er = ExecutionUtils.runCmd(cmd);
            if(er.getExitValue() != 0) {
                if(er.getExitValue() == 4) {
                    log.warn("Barcode not found");
                    throw new RuntimeException("Barcode not found");
                }

                throw new RuntimeException(er.getStderr());
            }

            ret = er.getStdout();
        } catch (SecurityException var9) {
            throw new RuntimeException("Security exception executing command: " + cmd, var9);
        } catch (InterruptedException var10) {
            throw new RuntimeException("Interrupted exception executing command: " + cmd, var10);
        } catch (IOException var11) {
            throw new RuntimeException("IO exception executing command: " + cmd, var11);
        } catch (TemplateException var12) {
            throw new RuntimeException("Template exception", var12);
        }

        SystemProfiling.log((String)null, System.currentTimeMillis() - begin);
        log.trace("multiple.Time: {}", Long.valueOf(System.currentTimeMillis() - begin));
        return ret;
    }
}
