//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PluginImplementation
public class AbbyTextExtractor extends CuneiformTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(AbbyTextExtractor.class);

    public AbbyTextExtractor() {
        super(new String[]{"application/pdf", "image/bmp", "image/pcx", "image/jpeg", "image/tiff", "image/png"});
    }
}
