//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

import com.openkm.core.Config;
import com.openkm.core.DatabaseException;
import com.openkm.util.*;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;

@PluginImplementation
public class CuneiformTextExtractor extends AbstractTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(CuneiformTextExtractor.class);

    public CuneiformTextExtractor() {
        super(new String[]{"image/tiff", "image/gif", "image/jpg", "image/jpeg", "image/png"});
    }

    public CuneiformTextExtractor(String[] contentTypes) {
        super(contentTypes);
    }

    public String extractText(InputStream stream, String type, String encoding) throws IOException {
        File tmpFileIn = null;

        String var7;
        try {
            tmpFileIn = FileUtils.createTempFileFromMime(type);
            FileOutputStream e = new FileOutputStream(tmpFileIn);
            IOUtils.copy(stream, e);
            e.close();
            String text = this.extractText(tmpFileIn);
            var7 = text;
        } catch (DatabaseException var11) {
            log.warn("Failed to extract barcode text", var11);
            throw new IOException(var11.getMessage(), var11);
        } finally {
            IOUtils.closeQuietly(stream);
            FileUtils.deleteQuietly(tmpFileIn);
        }

        return var7;
    }

    public String extractText(File input) throws IOException {
        if(Config.SYSTEM_OCR.isEmpty()) {
            log.warn("Undefined OCR application");
            throw new IOException("Undefined OCR application");
        } else {
            StringBuilder sb = new StringBuilder();
            String text = this.doOcr(input);
            sb.append(text);
            if(!Config.SYSTEM_OCR_ROTATE.isEmpty()) {
                String[] angles = Config.SYSTEM_OCR_ROTATE.split(Config.LIST_SEPARATOR);
                String[] var5 = angles;
                int var6 = angles.length;

                for(int var7 = 0; var7 < var6; ++var7) {
                    String angle = var5[var7];
                    log.debug("Rotate image {} degrees", angle);
                    double degree = Double.parseDouble(angle);
                    ImageUtils.rotate(input, input, degree);
                    text = this.doOcr(input);
                    sb.append("\n-----------------------------\n");
                    sb.append(text);
                }
            }

            return sb.toString();
        }
    }

    public String doOcr(File tmpFileIn) throws IOException {
        Object stdout = null;
        FileInputStream fis = null;
        File tmpFileOut = null;
        String cmd = null;
        if(Config.SYSTEM_OCR.isEmpty()) {
            log.warn("Undefined OCR application");
            throw new IOException("Undefined OCR application");
        } else {
            String var8;
            try {
                tmpFileOut = File.createTempFile("okm", ".txt");
                HashMap e = new HashMap();
                e.put("fileIn", tmpFileIn.getPath());
                e.put("fileOut", tmpFileOut.getPath());
                cmd = TemplateUtils.replace("SYSTEM_OCR", Config.SYSTEM_OCR, e);
                ExecutionUtils.runCmd(cmd);
                fis = new FileInputStream(tmpFileOut);
                String text = IOUtils.toString(fis);
                if(!Config.SYSTEM_OCR_SPELL_CHECK || !Config.SYSTEM_OPENOFFICE_DICTIONARY.isEmpty()) {
                    text = DocumentUtils.spellChecker(text);
                    log.debug("TEXT: {}", text);
                    var8 = text;
                    return var8;
                }

                log.debug("TEXT: {}", text);
                var8 = text;
            } catch (SecurityException var15) {
                log.warn("Security exception executing command: " + cmd, var15);
                throw new IOException(var15.getMessage(), var15);
            } catch (IOException var16) {
                log.warn("IO exception executing command: " + cmd, var16);
                throw new IOException(var16.getMessage(), var16);
            } catch (InterruptedException var17) {
                log.warn("Interrupted exception executing command: " + cmd, var17);
                throw new IOException(var17.getMessage(), var17);
            } catch (Exception var18) {
                log.warn("Failed to extract OCR text", var18);
                throw new IOException(var18.getMessage(), var18);
            } finally {
                IOUtils.closeQuietly(fis);
                IOUtils.closeQuietly((Reader)stdout);
                FileUtils.deleteQuietly(tmpFileOut);
            }

            return var8;
        }
    }
}
