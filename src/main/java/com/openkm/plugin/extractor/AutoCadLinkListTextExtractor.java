//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

import com.openkm.core.Config;
import com.openkm.extractor.PdfTextExtractor;
import com.openkm.util.DocConverter;
import com.openkm.util.FileUtils;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

@PluginImplementation
public class AutoCadLinkListTextExtractor extends AbstractTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(AutoCadLinkListTextExtractor.class);

    public AutoCadLinkListTextExtractor() {
        super(new String[]{"image/vnd.dxf", "image/vnd.dwg"});
    }

    @SuppressWarnings("unchecked")
    public String extractText(InputStream stream, String type, String encoding) throws IOException {
        File tmpDxf = null;
        File tmpDwg = null;
        FileOutputStream fos = null;
        String text = null;

        String var8;
        try {
            if (type.equals("image/vnd.dxf")) {
                tmpDxf = File.createTempFile("okm", ".dxf");
                fos = new FileOutputStream(tmpDxf);
                IOUtils.copy(stream, fos);
                text = this.getText(tmpDxf,type);
            } else if (type.equals("image/vnd.dwg")) {
                tmpDwg = File.createTempFile("okm", ".dwg");
                fos = new FileOutputStream(tmpDwg);
                IOUtils.copy(stream, fos);
                text = this.getText(tmpDwg,type);
            }

            log.debug("TEXT: " + text);
            var8 = text;
        } finally {
            IOUtils.closeQuietly(fos);
            IOUtils.closeQuietly(stream);
            FileUtils.deleteQuietly(tmpDxf);
            FileUtils.deleteQuietly(tmpDwg);
        }

        return var8;
    }

    private String getText(File dxf,String type) throws IOException {
        // File txt = FileUtils.createTempFile("txt");
        StringBuilder sb = new StringBuilder();
        FileInputStream fis = null;
        String cmd = null;
        File tmpPdf = FileUtils.createTempFile( "pdf");
        try {
            //  HashMap e = new HashMap();
            //e.put("fileIn", dxf.getPath());
            //e.put("fileOut", txt.getPath());
//            String tpl = Config.CADVIEWER_LINKLIST + " -i=${fileIn} -o=${fileOut} -txt";
            // cmd = TemplateUtils.replace("SYSTEM_LINKLIST", tpl, e);
            //CmdExecutionResult result = ExecutionUtils.runCmd2(cmd);
            //log.info("Result: {}", result);
//            if (result.getExitValue()!=0) {
            log.info("change Autocad to pdf then text extract :)");
            DocConverter.getInstance().cad2pdf(dxf, tmpPdf);
            PdfTextExtractor pdfTextExtractor = new PdfTextExtractor();
            sb.append(pdfTextExtractor.extractText(new FileInputStream(tmpPdf), type, "UTF-8"));
            /*} else {
                fis = new FileInputStream(txt);
                LineIterator it = IOUtils.lineIterator(fis, "UTF-8");

                while (it.hasNext()) {
                    String line = it.nextLine();
                    if (line.startsWith("TEXT")) {
                        int idx = line.indexOf(":");
                        if (idx > 0) {
                            line = line.substring(line.indexOf(":") + 1).trim();
                        }

                        if (line.startsWith("\'") && line.endsWith("\'")) {
                            line = line.substring(1, line.length() - 1);
                        }

                        sb.append(line).append(" ");
                    }
                }
            }*/
        } catch (SecurityException var18) {
            log.warn("Security exception executing command: " + cmd, var18);
            throw new IOException(var18.getMessage(), var18);
        } catch (IOException var19) {
            log.warn("IO exception executing command: " + cmd, var19);
            throw new IOException(var19.getMessage(), var19);
        }/* catch (InterruptedException var20) {
            log.warn("Interrupted exception executing command: " + cmd, var20);
            throw new IOException(var20.getMessage(), var20);
        }*/ catch (Exception var21) {
            log.warn("Failed to extract text", var21);
            throw new IOException(var21.getMessage(), var21);
        } finally {
            //  FileUtils.deleteQuietly(txt);
            FileUtils.deleteQuietly(tmpPdf);
        }

        return sb.toString();
    }
}
