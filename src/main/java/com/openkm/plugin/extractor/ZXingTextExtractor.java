//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin.extractor;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.GenericMultipleBarcodeReader;
import com.openkm.core.DatabaseException;
import com.openkm.util.FileUtils;
import com.openkm.util.SystemProfiling;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

@PluginImplementation
public class ZXingTextExtractor extends AbstractTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(ZXingTextExtractor.class);

    public ZXingTextExtractor() {
        super(new String[]{"image/tiff", "image/gif", "image/jpeg", "image/png"});
    }

    public ZXingTextExtractor(String[] contentTypes) {
        super(contentTypes);
    }

    public String extractText(InputStream stream, String type, String encoding) throws IOException {
        FileOutputStream tmpInFos = null;
        File tmpFileIn = null;

        String var7;
        try {
            tmpFileIn = FileUtils.createTempFileFromMime(type);
            tmpInFos = new FileOutputStream(tmpFileIn);
            IOUtils.copy(stream, tmpInFos);
            tmpInFos.flush();
            tmpInFos.close();
            String e = this.extractText(tmpFileIn);
            var7 = e;
        } catch (DatabaseException var11) {
            log.warn("Failed to extract barcode text", var11);
            throw new IOException(var11.getMessage(), var11);
        } finally {
            FileUtils.deleteQuietly(tmpFileIn);
            IOUtils.closeQuietly(tmpInFos);
            IOUtils.closeQuietly(stream);
        }

        return var7;
    }

    public String extractText(File input) throws IOException {
        try {
            return this.multiple(ImageIO.read(input));
        } catch (NotFoundException var3) {
            log.warn("Barcode not found", var3);
            throw new IOException("Barcode not found", var3);
        } catch (Exception var4) {
            log.warn("Failed to extract barcode text", var4);
            throw new IOException(var4.getMessage(), var4);
        }
    }

    private String simple(BufferedImage img) throws NotFoundException, ChecksumException, FormatException {
        long begin = System.currentTimeMillis();
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(img);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        MultiFormatReader reader = new MultiFormatReader();
        Result result = reader.decode(bitmap);
        SystemProfiling.log((String)null, System.currentTimeMillis() - begin);
        log.trace("simple.Time: {}", Long.valueOf(System.currentTimeMillis() - begin));
        return result.getText();
    }

    private String multiple(BufferedImage img) throws NotFoundException, ChecksumException, FormatException {
        long begin = System.currentTimeMillis();
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(img);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        MultiFormatReader reader = new MultiFormatReader();
        GenericMultipleBarcodeReader bcReader = new GenericMultipleBarcodeReader(reader);
        Hashtable hints = new Hashtable();
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        StringBuilder sb = new StringBuilder();
        Result[] var10 = bcReader.decodeMultiple(bitmap, hints);
        int var11 = var10.length;

        for(int var12 = 0; var12 < var11; ++var12) {
            Result result = var10[var12];
            sb.append(result.getText()).append(" ");
        }

        SystemProfiling.log((String)null, System.currentTimeMillis() - begin);
        log.trace("multiple.Time: {}", Long.valueOf(System.currentTimeMillis() - begin));
        return sb.toString();
    }
}
