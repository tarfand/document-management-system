//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.plugin;

import com.openkm.core.OKMException;

public class PluginNotFoundException extends OKMException {
    private static final long serialVersionUID = 1L;

    public PluginNotFoundException() {
        this.setErrorCode("057");
    }

    public PluginNotFoundException(String message) {
        super(message);
        this.setErrorCode("057");
    }

    public PluginNotFoundException(String message, Throwable cause) {
        super(message, cause);
        this.setErrorCode("057");
    }

    public PluginNotFoundException(Throwable arg0) {
        super(arg0);
        this.setErrorCode("057");
    }
}
