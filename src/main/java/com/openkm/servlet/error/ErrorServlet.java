package com.openkm.servlet.error;

import com.openkm.servlet.admin.BaseServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet({"/404"})
public class ErrorServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static Logger log = LoggerFactory.getLogger(ErrorServlet.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("doGet({}, {})", request, response);
        ServletContext sc = getServletConfig().getServletContext();
        sc.getRequestDispatcher("/404.jsp").forward(request, response);

    }

}
