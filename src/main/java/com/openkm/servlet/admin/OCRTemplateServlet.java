//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.openkm.servlet.admin;

import com.openkm.core.*;
import com.openkm.dao.HibernateUtil;
import com.openkm.dao.OCRTemplateDAO;
import com.openkm.dao.bean.OCRTemplate;
import com.openkm.dao.bean.OCRTemplateControlField;
import com.openkm.dao.bean.OCRTemplateField;
import com.openkm.dao.bean.OCRTemplateProperty;
import com.openkm.ocr.template.OCRParserEmptyValueException;
import com.openkm.ocr.template.OCRTemplateException;
import com.openkm.ocr.template.OCRTemplateUtils;
import com.openkm.ocr.template.parser.StringParser;
import com.openkm.plugin.PluginNotFoundException;
import com.openkm.util.ImageUtils;
import com.openkm.util.PropertyGroupUtils;
import com.openkm.util.UserActivity;
import com.openkm.util.WebUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class OCRTemplateServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(OCRTemplateServlet.class);
	private static final int[] degrees = new int[]{-270, -180, -90, 0, 90, 180, 270};

	public OCRTemplateServlet() {
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		log.debug("doGet({}, {})", request, response);
		String action = WebUtils.getString(request, "action");
		this.updateSessionManager(request);

		try {

			if (action.equals("create")) {
				this.create(request, response);
			} else if (action.equals("edit")) {
				this.edit(request, response);
			} else if (action.equals("delete")) {
				this.delete(request, response);
			} else if (action.equals("image")) {
				this.downloadImage(request, response);
			} else if (action.equals("fieldsList")) {
				this.fieldsList(request, response);
			} else if (action.equals("createField")) {
				this.createField(request, response);
			} else if (!action.equals("editField") && !action.equals("editContinueField")) {
				if (action.equals("deleteField")) {
					this.deleteField(request, response);
				} else if (action.equals("createControlField")) {
					this.createControlField(request, response);
				} else if (!action.equals("editControlField") && !action.equals("editContinueControlField")) {
					if (action.equals("deleteControlField")) {
						this.deleteControlField(request, response);
					} else if (action.equals("check")) {
						this.check(request, response);
					} else if (action.equals("downloadCroppedImage")) {
						this.downloadCroppedImage(request, response);
					} else if (action.equals("recognise")) {
						this.recognise(request, response, (byte[]) null, "", 0);
					} else {
						this.list(request, response);
					}
				} else {
					this.editControlField(request, response);
				}
			} else {
				this.editField(request, response);
			}
		} catch (Exception var5) {
			log.error(var5.getMessage(), var5);
			this.sendErrorRedirect(request, response, var5);
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		log.debug("doPost({}, {})", request, response);
		response.setCharacterEncoding("UTF-8");
		String action = WebUtils.getString(request, "action");
		String userId = request.getRemoteUser();
		Object dbSession = null;
		this.updateSessionManager(request);

		try {

			if (ServletFileUpload.isMultipartContent(request)) {
				DiskFileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List items = upload.parseRequest(request);
				OCRTemplate ot = new OCRTemplate();
				ArrayList pgroup = new ArrayList();
				ArrayList pgroupValue = new ArrayList();
				ArrayList tplProps = new ArrayList();
				byte[] data = null;
				int level = 0;
				Iterator count = items.iterator();

				while (count.hasNext()) {
					FileItem tmp = (FileItem) count.next();
					if (tmp.isFormField()) {
						if (tmp.getFieldName().equals("action")) {
							action = tmp.getString("UTF-8");
						} else if (tmp.getFieldName().equals("ot_id")) {
							ot.setId((long) Integer.parseInt(tmp.getString("UTF-8")));
						} else if (tmp.getFieldName().equals("ot_name")) {
							ot.setName(tmp.getString("UTF-8").toLowerCase());
						} else if (tmp.getFieldName().equals("ot_active")) {
							ot.setActive(Boolean.valueOf(true));
						} else if (tmp.getFieldName().equals("level")) {
							level = Integer.parseInt(tmp.getString("UTF-8"));
						} else if (tmp.getFieldName().equals("ot_property_name")) {
							pgroup.add(tmp.getString("UTF-8"));
						} else if (tmp.getFieldName().equals("ot_property_value")) {
							pgroupValue.add(tmp.getString("UTF-8"));
						}
					} else {
						InputStream is = tmp.getInputStream();
						data = IOUtils.toByteArray(is);
						ot.setImageMime(MimeTypeConfig.mimeTypes.getContentType(tmp.getName()));
						is.close();
						if ("image/tiff".equals(ot.getImageMime())) {
							data = OCRTemplateUtils.convertToSupportedFormat(data, ot.getImageMime());
							ot.setImageMime(ImageUtils.getFormatMimeType(data));
						} else if ("application/pdf".equals(ot.getImageMime())) {
							data = OCRTemplateUtils.convertToSupportedFormat(data, ot.getImageMime());
							ot.setImageMime(ImageUtils.getFormatMimeType(data));
						}
					}
				}

				int var26 = 0;

				for (Iterator var27 = pgroup.iterator(); var27.hasNext(); ++var26) {
					String name = (String) var27.next();
					OCRTemplateProperty tplProp = new OCRTemplateProperty();
					tplProp.setName(name);
					if (pgroupValue.size() > var26) {
						tplProp.setValue((String) pgroupValue.get(var26));
					}

					tplProps.add(tplProp);
				}

				if (action.equals("create")) {
					ot.setImageContent(data);
					ot.setProperties(tplProps);
					long var28 = OCRTemplateDAO.getInstance().create(ot);
					UserActivity.log(userId, "ADMIN_OCR_TEMPLATE_CREATE", Long.toString(var28), (String) null, ot.toString());
				} else if (action.equals("edit")) {
					OCRTemplate var29 = OCRTemplateDAO.getInstance().findByPk(ot.getId());
					ot.setImageContent(data);
					ot.setFields(var29.getFields());
					ot.setControlFields(var29.getControlFields());
					ot.setProperties(tplProps);
					OCRTemplateDAO.getInstance().update(ot);
					UserActivity.log(userId, "ADMIN_OCR_TEMPLATE_UPDATE", Long.toString(ot.getId()), (String) null, ot.toString());
				} else if (action.equals("delete")) {
					OCRTemplateDAO.getInstance().delete(ot.getId());
					UserActivity.log(userId, "ADMIN_OCR_TEMPLATE_DELETE", Long.toString(ot.getId()), (String) null, (String) null);
				} else if (action.equals("recognise")) {
					this.recognise(request, response, data, ot.getImageMime(), level);
				}

				if (!action.equals("recognise")) {
					response.sendRedirect(request.getContextPath() + request.getServletPath());
				}
			} else if (action.equals("createField")) {
				this.createField(request, response);
			} else if (!action.equals("editField") && !action.equals("editContinueField")) {
				if (action.equals("deleteField")) {
					this.deleteField(request, response);
				} else if (action.equals("createControlField")) {
					this.createControlField(request, response);
				} else if (!action.equals("editControlField") && !action.equals("editContinueControlField")) {
					if (action.equals("deleteControlField")) {
						this.deleteControlField(request, response);
					}
				} else {
					this.editControlField(request, response);
				}
			} else {
				this.editField(request, response);
			}
		} catch (Exception var24) {
			log.error(var24.getMessage(), var24);
			this.sendErrorRedirect(request, response, var24);
		} finally {
			HibernateUtil.close((Session) dbSession);
		}

	}

	private void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException {
		log.debug("list({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		sc.setAttribute("ocrTemplates", OCRTemplateDAO.getInstance().findAll());
		sc.getRequestDispatcher("/admin/ocr_template_list.jsp").forward(request, response);
		log.debug("list: void");
	}

	private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException {
		log.debug("delete({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		int otId = WebUtils.getInt(request, "ot_id");
		OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk((long) otId);
		sc.setAttribute("action", WebUtils.getString(request, "action"));
		sc.setAttribute("ot", ot);
		sc.getRequestDispatcher("/admin/ocr_template_edit.jsp").forward(request, response);
		log.debug("delete: void");
	}

	private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, ParseException, AccessDeniedException, RepositoryException, PluginNotFoundException {
		log.debug("create({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		OCRTemplate ot = new OCRTemplate();
		sc.setAttribute("action", WebUtils.getString(request, "action"));
		sc.setAttribute("ot", ot);
		sc.setAttribute("pgprops", PropertyGroupUtils.getAllGroupsProperties());
		sc.getRequestDispatcher("/admin/ocr_template_edit.jsp").forward(request, response);
		log.debug("create: void");
	}

	private void edit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, ParseException, AccessDeniedException, RepositoryException, PluginNotFoundException {
		log.debug("edit({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		int otId = WebUtils.getInt(request, "ot_id");
		OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk((long) otId);
		sc.setAttribute("action", WebUtils.getString(request, "action"));
		List pgProps = PropertyGroupUtils.getAllGroupsProperties();
		Iterator var7 = ot.getProperties().iterator();

		while (var7.hasNext()) {
			OCRTemplateProperty tplProp = (OCRTemplateProperty) var7.next();
			if (pgProps.contains(tplProp.getName())) {
				pgProps.remove(tplProp.getName());
			}
		}

		sc.setAttribute("pgprops", pgProps);
		sc.setAttribute("ot", ot);
		sc.getRequestDispatcher("/admin/ocr_template_edit.jsp").forward(request, response);
		log.debug("edit: void");
	}

	private void downloadImage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException {
		log.debug("downloadImage({}, {})", new Object[]{request, response});
		int otId = WebUtils.getInt(request, "ot_id");
		OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk((long) otId);
		if (ot != null) {
			ServletOutputStream os = response.getOutputStream();

			try {
				byte[] img = ot.getImageContent();
				WebUtils.prepareSendFile(request, response, ot.getName(), ot.getImageMime(), true);
				response.setContentLength(img.length);
				os.write(img);
				os.flush();
			} finally {
				IOUtils.closeQuietly(os);
			}
		}

		log.debug("downloadImage: void");
	}

	private void fieldsList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, PathNotFoundException, RepositoryException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, URISyntaxException {
		log.debug("fieldsList({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		long otId = WebUtils.getLong(request, "ot_id");
		OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk(otId);
		sc.setAttribute("ot", ot);
		sc.setAttribute("parsers", OCRTemplateDAO.getInstance().findParsers(false));
		sc.setAttribute("controlParsers", OCRTemplateDAO.getInstance().findControlParsers(false));
		sc.getRequestDispatcher("/admin/ocr_template_field_list.jsp").forward(request, response);
		log.debug("fieldsList: void");
	}

	private void createField(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, AccessDeniedException, PathNotFoundException, RepositoryException, ParseException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, URISyntaxException, PluginNotFoundException {
		log.debug("createField({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		long otId = WebUtils.getLong(request, "ot_id");
		OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk(otId);
		if (this.isPost(request)) {
			OCRTemplateField otf = new OCRTemplateField();
			otf.setName(WebUtils.getString(request, "otf_name"));
			String otf_className = WebUtils.getString(request, "otf_className");
			otf.setClassName(otf_className);
			otf.setPropertyGroupName(WebUtils.getString(request, "otf_pgname"));
			otf.setPattern(WebUtils.getString(request, "otf_pattern"));
			if (otf_className.equals(StringParser.class.getName())) {
				otf.setFilterPattern(WebUtils.getString(request, "otf_filter_pattern"));
				otf.setWordListUUID(WebUtils.getString(request, "otf_word_list_uuid"));
			}
			otf.setRotation(WebUtils.getInt(request, "otf_rotation", 0));
			otf.setCustom(WebUtils.getString(request, "otf_custom"));
			otf.setOcr(WebUtils.getString(request, "otf_ocr"));
			otf.setControl(Boolean.valueOf(WebUtils.getBoolean(request, "otf_control")));
			otf.setX1(WebUtils.getInt(request, "otf_x1"));
			otf.setY1(WebUtils.getInt(request, "otf_y1"));
			otf.setX2(WebUtils.getInt(request, "otf_x2"));
			otf.setY2(WebUtils.getInt(request, "otf_y2"));
			this.arrangeLeftToRightSquarePoints(otf);
			ot.getFields().add(otf);
			OCRTemplateDAO.getInstance().update(ot);
			sc.setAttribute("ot", ot);
			sc.getRequestDispatcher("/admin/ocr_template_field_list.jsp").forward(request, response);
			UserActivity.log(request.getRemoteUser(), "ADMIN_OCR_TEMPLATE_FIELD_CREATE", Long.toString(ot.getId()), (String) null, (String) null);
		} else {
			sc.setAttribute("ot", ot);
			sc.setAttribute("otf", new OCRTemplateField());
			sc.setAttribute("action", WebUtils.getString(request, "action"));
			sc.setAttribute("parsers", OCRTemplateDAO.getInstance().findParsers(false));
			sc.setAttribute("pgprops", PropertyGroupUtils.getAllGroupsProperties());
			sc.setAttribute("degrees", degrees);
			sc.getRequestDispatcher("/admin/ocr_template_field_edit.jsp").forward(request, response);
		}

		log.debug("createField: void");
	}

	private void editField(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, AccessDeniedException, PathNotFoundException, RepositoryException, ParseException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, URISyntaxException, PluginNotFoundException {
		log.debug("editField({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		long otId = WebUtils.getLong(request, "ot_id");
		long otfId = WebUtils.getLong(request, "otf_id");
		OCRTemplateField otf = OCRTemplateDAO.getInstance().findFieldByPk(otfId);
		if (this.isPost(request)) {
			otf.setName(WebUtils.getString(request, "otf_name"));
			String otf_className = WebUtils.getString(request, "otf_className");
			otf.setClassName(otf_className);
			otf.setPropertyGroupName(WebUtils.getString(request, "otf_pgname"));
			otf.setPattern(WebUtils.getString(request, "otf_pattern"));
			if (otf_className.equals(StringParser.class.getName())) {
				otf.setFilterPattern(WebUtils.getString(request, "otf_filter_pattern"));
				otf.setWordListUUID(WebUtils.getString(request, "otf_word_list_uuid"));
			} else {
				otf.setFilterPattern(null);
				otf.setWordListUUID(null);
			}
			otf.setRotation(WebUtils.getInt(request, "otf_rotation", 0));
			otf.setCustom(WebUtils.getString(request, "otf_custom"));
			otf.setOcr(WebUtils.getString(request, "otf_ocr"));
			otf.setControl(Boolean.valueOf(WebUtils.getBoolean(request, "otf_control")));
			otf.setX1(WebUtils.getInt(request, "otf_x1"));
			otf.setY1(WebUtils.getInt(request, "otf_y1"));
			otf.setX2(WebUtils.getInt(request, "otf_x2"));
			otf.setY2(WebUtils.getInt(request, "otf_y2"));
			this.arrangeLeftToRightSquarePoints(otf);
			OCRTemplateDAO.getInstance().updateField(otf);
			sc.setAttribute("ot", OCRTemplateDAO.getInstance().findByPk(otId));
			if ("editField".equals(WebUtils.getString(request, "action"))) {
				sc.getRequestDispatcher("/admin/ocr_template_field_list.jsp").forward(request, response);
			} else {
				OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk(otId);
				boolean foundFlag = false;
				otf = null;
				Iterator var11 = ot.getFields().iterator();

				while (var11.hasNext()) {
					OCRTemplateField field = (OCRTemplateField) var11.next();
					if (foundFlag) {
						otf = field;
						foundFlag = false;
					}

					if (field.getId() == otfId) {
						foundFlag = true;
					}
				}

				if (otf == null && ot.getFields().size() > 0) {
					otf = (OCRTemplateField) ot.getFields().get(0);
				}

				sc.setAttribute("ot", ot);
				sc.setAttribute("otf", otf);
				sc.setAttribute("action", "editField");
				sc.setAttribute("parsers", OCRTemplateDAO.getInstance().findParsers(false));
				sc.setAttribute("pgprops", PropertyGroupUtils.getAllGroupsProperties());
				sc.setAttribute("degrees", degrees);
				sc.getRequestDispatcher("/admin/ocr_template_field_edit.jsp").forward(request, response);
			}

			UserActivity.log(request.getRemoteUser(), "ADMIN_OCR_TEMPLATE_FIELD_EDIT", Long.toString(otId), (String) null, (String) null);
		} else {
			sc.setAttribute("ot", OCRTemplateDAO.getInstance().findByPk(otId));
			sc.setAttribute("otf", otf);
			sc.setAttribute("action", WebUtils.getString(request, "action"));
			sc.setAttribute("parsers", OCRTemplateDAO.getInstance().findParsers(false));
			sc.setAttribute("pgprops", PropertyGroupUtils.getAllGroupsProperties());
			sc.setAttribute("degrees", degrees);
			sc.getRequestDispatcher("/admin/ocr_template_field_edit.jsp").forward(request, response);
		}

		log.debug("editField: void");
	}

	private void createControlField(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, PathNotFoundException, RepositoryException, ParseException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, URISyntaxException {
		log.debug("createControlField({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		long otId = WebUtils.getLong(request, "ot_id");
		OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk(otId);
		if (this.isPost(request)) {
			OCRTemplateControlField otcf = new OCRTemplateControlField();
			otcf.setName(WebUtils.getString(request, "otcf_name"));
			otcf.setOrder(WebUtils.getInt(request, "otcf_order", 0));
			otcf.setClassName(WebUtils.getString(request, "otcf_className"));
			otcf.setPattern(WebUtils.getString(request, "otcf_pattern"));
			otcf.setPattern(WebUtils.getString(request, "otcf_filter_pattern"));
			otcf.setValue(WebUtils.getString(request, "otcf_value"));
			otcf.setRotation(WebUtils.getInt(request, "otcf_rotation", 0));
			otcf.setEmptyAllowed(Boolean.valueOf(WebUtils.getBoolean(request, "otcf_emptyAllowed")));
			otcf.setOcr(WebUtils.getString(request, "otcf_ocr"));
			otcf.setEmptyAllowed(Boolean.valueOf(WebUtils.getBoolean(request, "otcf_emptyAllowed")));
			otcf.setX1(WebUtils.getInt(request, "otcf_x1"));
			otcf.setY1(WebUtils.getInt(request, "otcf_y1"));
			otcf.setX2(WebUtils.getInt(request, "otcf_x2"));
			otcf.setY2(WebUtils.getInt(request, "otcf_y2"));
			this.arrangeLeftToRightSquarePoints(otcf);
			ot.getControlFields().add(otcf);
			OCRTemplateDAO.getInstance().update(ot);
			sc.setAttribute("ot", ot);
			sc.getRequestDispatcher("/admin/ocr_template_field_list.jsp").forward(request, response);
			UserActivity.log(request.getRemoteUser(), "ADMIN_OCR_TEMPLATE_FIELD_CREATE", Long.toString(ot.getId()), (String) null, (String) null);
		} else {
			sc.setAttribute("ot", ot);
			sc.setAttribute("otcf", new OCRTemplateControlField());
			sc.setAttribute("action", WebUtils.getString(request, "action"));
			sc.setAttribute("controlParsers", OCRTemplateDAO.getInstance().findControlParsers(false));
			sc.setAttribute("degrees", degrees);
			sc.getRequestDispatcher("/admin/ocr_template_control_field_edit.jsp").forward(request, response);
		}

		log.debug("createControlField: void");
	}

	private void editControlField(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, PathNotFoundException, RepositoryException, ParseException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, URISyntaxException {
		log.debug("editControlField({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		long otId = WebUtils.getLong(request, "ot_id");
		long otcfId = WebUtils.getLong(request, "otcf_id");
		OCRTemplateControlField otcf = OCRTemplateDAO.getInstance().findControlFieldByPk(otcfId);
		if (this.isPost(request)) {
			otcf.setName(WebUtils.getString(request, "otcf_name"));
			otcf.setOrder(WebUtils.getInt(request, "otcf_order", 0));
			otcf.setClassName(WebUtils.getString(request, "otcf_className"));
			otcf.setPattern(WebUtils.getString(request, "otcf_pattern"));
			otcf.setFilterPattern(WebUtils.getString(request, "otcf_filter_pattern"));
			otcf.setValue(WebUtils.getString(request, "otcf_value"));
			otcf.setRotation(WebUtils.getInt(request, "otcf_rotation", 0));
			otcf.setCustom(WebUtils.getString(request, "otcf_custom"));
			otcf.setOcr(WebUtils.getString(request, "otcf_ocr"));
			otcf.setEmptyAllowed(Boolean.valueOf(WebUtils.getBoolean(request, "otcf_emptyAllowed")));
			otcf.setX1(WebUtils.getInt(request, "otcf_x1"));
			otcf.setY1(WebUtils.getInt(request, "otcf_y1"));
			otcf.setX2(WebUtils.getInt(request, "otcf_x2"));
			otcf.setY2(WebUtils.getInt(request, "otcf_y2"));
			this.arrangeLeftToRightSquarePoints(otcf);
			OCRTemplateDAO.getInstance().updateControlField(otcf);
			sc.setAttribute("ot", OCRTemplateDAO.getInstance().findByPk(otId));
			if ("editControlField".equals(WebUtils.getString(request, "action"))) {
				sc.getRequestDispatcher("/admin/ocr_template_field_list.jsp").forward(request, response);
			} else {
				OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk(otId);
				boolean foundFlag = false;
				otcf = null;
				Iterator var11 = ot.getControlFields().iterator();

				while (var11.hasNext()) {
					OCRTemplateControlField controlField = (OCRTemplateControlField) var11.next();
					if (foundFlag) {
						otcf = controlField;
						foundFlag = false;
					}

					if (controlField.getId() == otcfId) {
						foundFlag = true;
					}
				}

				if (otcf == null && ot.getFields().size() > 0) {
					otcf = (OCRTemplateControlField) ot.getControlFields().get(0);
				}

				sc.setAttribute("ot", ot);
				sc.setAttribute("otcf", otcf);
				sc.setAttribute("action", "editControlField");
				sc.setAttribute("controlParsers", OCRTemplateDAO.getInstance().findControlParsers(false));
				sc.setAttribute("degrees", degrees);
				sc.getRequestDispatcher("/admin/ocr_template_control_field_edit.jsp").forward(request, response);
			}

			UserActivity.log(request.getRemoteUser(), "ADMIN_OCR_TEMPLATE_CONTROL_FIELD_EDIT", Long.toString(otId), (String) null, (String) null);
		} else {
			sc.setAttribute("ot", OCRTemplateDAO.getInstance().findByPk(otId));
			sc.setAttribute("otcf", otcf);
			sc.setAttribute("action", WebUtils.getString(request, "action"));
			sc.setAttribute("controlParsers", OCRTemplateDAO.getInstance().findControlParsers(false));
			sc.setAttribute("degrees", degrees);
			sc.getRequestDispatcher("/admin/ocr_template_control_field_edit.jsp").forward(request, response);
		}

		log.debug("editControlField: void");
	}

	private void check(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, PathNotFoundException, RepositoryException, ParseException, OCRTemplateException, IllegalArgumentException, SecurityException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, URISyntaxException {
		log.debug("check({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		long otId = WebUtils.getLong(request, "ot_id");
		OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk(otId);
		sc.setAttribute("ot", ot);
		sc.setAttribute("action", WebUtils.getString(request, "action"));
		sc.setAttribute("parsers", OCRTemplateDAO.getInstance().findParsers(false));
		HashMap controlResultsMap = new HashMap();
		HashMap resultsMap = new HashMap();
		byte[] img = ot.getImageContent();
		Iterator var10 = ot.getControlFields().iterator();

		String text;
		while (var10.hasNext()) {
			OCRTemplateControlField otf = (OCRTemplateControlField) var10.next();
			text = OCRTemplateUtils.extractText(otf, img);

			try {
				boolean e = OCRTemplateUtils.parse(otf, text);
				controlResultsMap.put(String.valueOf(otf.getId()), String.valueOf(e));
			} catch (OCRParserEmptyValueException var16) {
				controlResultsMap.put(String.valueOf(otf.getId()), "");
			}
		}

		var10 = ot.getFields().iterator();

		while (var10.hasNext()) {
			OCRTemplateField otf1 = (OCRTemplateField) var10.next();
			text = OCRTemplateUtils.extractText(otf1, img);

			try {
				Object e1 = OCRTemplateUtils.parse(otf1, text);
				resultsMap.put(String.valueOf(otf1.getId()), OCRTemplateUtils.toString(e1, otf1.getPattern()));
			} catch (Exception var15) {
				resultsMap.put(String.valueOf(otf1.getId()), OCRTemplateUtils.toString("", otf1.getPattern()));
			}
		}

		sc.setAttribute("controlResultsMap", controlResultsMap);
		sc.setAttribute("resultsMap", resultsMap);
		sc.getRequestDispatcher("/admin/ocr_template_check.jsp").forward(request, response);
		log.debug("check: void");
	}

	private void downloadCroppedImage(HttpServletRequest request, HttpServletResponse response) throws DatabaseException, IOException, OCRTemplateException {
		log.debug("downloadCroppedImage({}, {})", new Object[]{request, response});
		int otId = WebUtils.getInt(request, "ot_id");
		int otfId = WebUtils.getInt(request, "otf_id", 0);
		int otcfId = WebUtils.getInt(request, "otcf_id", 0);
		OCRTemplateControlField otcf = null;
		OCRTemplateField otf = null;
		if (otfId != 0) {
			otf = OCRTemplateDAO.getInstance().findFieldByPk((long) otfId);
		}

		if (otcfId != 0) {
			otcf = OCRTemplateDAO.getInstance().findControlFieldByPk((long) otcfId);
		}

		OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk((long) otId);
		if (ot != null) {
			ServletOutputStream os;
			byte[] img;
			byte[] cropedImg;
			byte[] rotatedImg;
			if (otf != null) {
				os = response.getOutputStream();

				try {
					img = ot.getImageContent();
					WebUtils.prepareSendFile(request, response, ot.getName(), ot.getImageMime(), true);
					img = ImageUtils.custom(img, ImageUtils.getFormatMimeType(img), otf.getCustom());
					cropedImg = ImageUtils.crop(img, otf.getX1(), otf.getY1(), otf.getX2() - otf.getX1(), otf.getY2() - otf.getY1());
					rotatedImg = ImageUtils.rotate(cropedImg, (double) otf.getRotation());
					response.setContentLength(rotatedImg.length);
					os.write(rotatedImg);
					os.flush();
				} finally {
					IOUtils.closeQuietly(os);
				}
			} else if (otcf != null) {
				os = response.getOutputStream();

				try {
					img = ot.getImageContent();
					WebUtils.prepareSendFile(request, response, ot.getName(), ot.getImageMime(), true);
					img = ImageUtils.custom(img, ImageUtils.getFormatMimeType(img), otcf.getCustom());
					cropedImg = ImageUtils.crop(img, otcf.getX1(), otcf.getY1(), otcf.getX2() - otcf.getX1(), otcf.getY2() - otcf.getY1());
					rotatedImg = ImageUtils.rotate(cropedImg, (double) otcf.getRotation());
					response.setContentLength(rotatedImg.length);
					os.write(rotatedImg);
					os.flush();
				} finally {
					IOUtils.closeQuietly(os);
				}
			}
		}

		log.debug("downloadCroppedImage: void");
	}

	private void recognise(HttpServletRequest request, HttpServletResponse response, byte[] img, String mimeType, int level) throws DatabaseException, IOException, OCRTemplateException, ServletException {
		log.debug("recognise({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		sc.setAttribute("action", WebUtils.getString(request, "action"));
		if (img == null) {
			sc.getRequestDispatcher("/admin/ocr_template_recognise_file.jsp").forward(request, response);
		} else {
			sc.setAttribute("level", Integer.valueOf(level));
			sc.setAttribute("recognize", OCRTemplateUtils.recognize(img, mimeType, level, true));
			sc.getRequestDispatcher("/admin/ocr_template_recognise.jsp").forward(request, response);
		}

		log.debug("recognise: void");
	}

	private void deleteField(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, AccessDeniedException, PathNotFoundException, RepositoryException, ParseException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, URISyntaxException, PluginNotFoundException {
		log.debug("deleteField({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		long otId = WebUtils.getLong(request, "ot_id");
		long otfId = WebUtils.getLong(request, "otf_id");
		if (this.isPost(request)) {
			OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk(otId);
			Iterator var9 = ot.getFields().iterator();

			while (var9.hasNext()) {
				OCRTemplateField otf = (OCRTemplateField) var9.next();
				if (otf.getId() == otfId) {
					ot.getFields().remove(otf);
					break;
				}
			}

			OCRTemplateDAO.getInstance().update(ot);
			OCRTemplateDAO.getInstance().deleteField(otfId);
			sc.setAttribute("ot", ot);
			sc.getRequestDispatcher("/admin/ocr_template_field_list.jsp").forward(request, response);
			UserActivity.log(request.getRemoteUser(), "ADMIN_OCR_TEMPLATE_FIELD_DELETE", Long.toString(otId), (String) null, (String) null);
		} else {
			sc.setAttribute("ot", OCRTemplateDAO.getInstance().findByPk(otId));
			sc.setAttribute("otf", OCRTemplateDAO.getInstance().findFieldByPk(otfId));
			sc.setAttribute("action", WebUtils.getString(request, "action"));
			sc.setAttribute("parsers", OCRTemplateDAO.getInstance().findParsers(false));
			sc.setAttribute("pgprops", PropertyGroupUtils.getAllGroupsProperties());
			sc.setAttribute("degrees", degrees);
			sc.getRequestDispatcher("/admin/ocr_template_field_edit.jsp").forward(request, response);
		}

		log.debug("deleteField: void");
	}

	private void deleteControlField(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DatabaseException, PathNotFoundException, RepositoryException, ParseException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, URISyntaxException {
		log.debug("deleteControlField({}, {})", new Object[]{request, response});
		ServletContext sc = this.getServletContext();
		long otId = WebUtils.getLong(request, "ot_id");
		long otcfId = WebUtils.getLong(request, "otcf_id");
		if (this.isPost(request)) {
			OCRTemplate ot = OCRTemplateDAO.getInstance().findByPk(otId);
			Iterator var9 = ot.getControlFields().iterator();

			while (var9.hasNext()) {
				OCRTemplateControlField otcf = (OCRTemplateControlField) var9.next();
				if (otcf.getId() == otcfId) {
					ot.getControlFields().remove(otcf);
					break;
				}
			}

			OCRTemplateDAO.getInstance().update(ot);
			OCRTemplateDAO.getInstance().deleteControlField(otcfId);
			sc.setAttribute("ot", ot);
			sc.getRequestDispatcher("/admin/ocr_template_field_list.jsp").forward(request, response);
			UserActivity.log(request.getRemoteUser(), "ADMIN_OCR_TEMPLATE_CONTROL_FIELD_DELETE", Long.toString(otId), (String) null, (String) null);
		} else {
			sc.setAttribute("ot", OCRTemplateDAO.getInstance().findByPk(otId));
			sc.setAttribute("otcf", OCRTemplateDAO.getInstance().findControlFieldByPk(otcfId));
			sc.setAttribute("action", WebUtils.getString(request, "action"));
			sc.setAttribute("controlParsers", OCRTemplateDAO.getInstance().findControlParsers(false));
			sc.setAttribute("degrees", degrees);
			sc.getRequestDispatcher("/admin/ocr_template_control_field_edit.jsp").forward(request, response);
		}

		log.debug("deleteControlField: void");
	}

	private void arrangeLeftToRightSquarePoints(OCRTemplateField otf) {
		otf.setX1(Math.min(otf.getX1(), otf.getX2()));
		otf.setX2(Math.max(otf.getX1(), otf.getX2()));
		otf.setY1(Math.min(otf.getY1(), otf.getY2()));
		otf.setY2(Math.max(otf.getY1(), otf.getY2()));
	}

	private void arrangeLeftToRightSquarePoints(OCRTemplateControlField otf) {
		otf.setX1(Math.min(otf.getX1(), otf.getX2()));
		otf.setX2(Math.max(otf.getX1(), otf.getX2()));
		otf.setY1(Math.min(otf.getY1(), otf.getY2()));
		otf.setY2(Math.max(otf.getY1(), otf.getY2()));
	}
}
