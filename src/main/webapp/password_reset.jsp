<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ page import="com.openkm.core.Config" %>
<!DOCTYPE html>
<head>
  <% int today=20201003;%>
  <meta charset="utf-8">
  <meta name="author" content="DMS">
  <meta name="description" content="DMS is an EDRMS EDRMS, Document Management System and Record Management, easily to manage digital content, simplify your workload and yield high efficiency.">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
  <link rel="Shortcut icon" href="<%=request.getContextPath() %>/logo/favicon"/>
  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap/bootstrap.min.css" type="text/css"/>
  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome/font-awesome.min.css" type="text/css"/>
  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/login.css?v=<%=today%>" type="text/css"/>
  <link rel="stylesheet" href="<%=request.getContextPath() %>/css/desktop.css?v=<%=today%>" type="text/css"/>

  <title>DMS password reset</title>
</head>
<body onload="document.forms[0].elements[0].focus()">

<u:constantsMap className="com.openkm.core.Config" var="Config"/>
<div id="box_login">
  <div id="logo"><%=Config.TEXT_TITLE %></div>


  <div style="margin-top: 28px">
    <form name="resetForm" method="post" action="PasswordReset"
          class="form-horizontal form-bordered form-control-borderless" id="form-reset">
      <div class="text-center">
        <div class="col-xs-12" style="margin:0px auto 0px auto;width:316px;text-align:center;font-weight:bolder;font-size:20px;color:black;">
          <p style="font-size: 14px;">لطفا نام کاربری خود را وارد نمایید، یک ایمیل جهت بازیابی کلمه عبور برای شما ارسال می شود</p>
        </div>
      </div>
        <% ServletContext sc = getServletContext(); %>
      <c:if test="${not empty resetFailed}">
      <div class="form-error" style="text-align: center;">
        <div id="col-xs-12" style="text-align="center">
        <p class="text-danger text-center">${resetFailed}</p>
      </div>
  </div>
  <% sc.removeAttribute("resetFailed"); %>
  </c:if>
  <c:if test="${not empty resetOk}">
    <div class="form-error" style="text-align:center;">
      <div id="col-xs-12">
        <p class="text-success text-center"> فرستاده شد ${resetOk}کلمه عبور به درستی بازیابی شد و به ایمیل  </p>
      </div>
    </div>
    <% sc.removeAttribute("resetOk"); %>
  </c:if>
  <div>
    <div class="col-xs-12">
      <div style="text-align: center;">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <input name="userId" id="userId" type="text"/>
      </div>
    </div>
  </div>
  <div >
    <div class="col-xs-4 pull-right" style="text-align: center;">
      <button name="submit" type="submit" class="btn btn-sm btn-primary btn-block" id="btn_reset">
        <i class="fa fa-key"></i> بازیابی کلمه عبور
      </button>
    </div>
    <div class="col-xs-6" style="text-align: center;margin-top: 10px;">
      <a href="login" class="btn btn-sm btn-default" style="text-align: center;font-size: 17px;margin-top: 7px;">
        <i class="fa fa-arrow-left"></i>         بازگشت به صفحه ورود          </a>
    </div>

  </div>

  </form>
</div>
</div>
</body>
</html>
