<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true" %>
<% response.setStatus(404); %>


<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl" lang="fa-FA">
<head>
    <title>404 Error</title>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Language" content="fa"/>
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath() %>/expiration/style.css"/>

</head>

<body>


<div class="wrapper">

    <div class="mainWrapper">

        <div align="center" class="message" style="margin: 0 auto;
    color: white;
    font-size: 4em;}">

صفحه مورد نظر یافت نشد
        </div>


</div>

</div>

</body>
</html>