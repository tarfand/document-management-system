<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="Shortcut icon" href="favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="css/style.css?v=1506588453493"/>
    <link rel="stylesheet" type="text/css" href="css/imgareaselect/imgareaselect-default.css"/>
    <link rel="stylesheet" type="text/css" href="../css/chosen.css"/>
    <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
    <script src="../js/vanadium-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.imgareaselect.pack.js"></script>
    <script type="text/javascript" src="../js/chosen.jquery.js"></script>
    <script type="text/javascript">
        // Keep in sync with ocr_template_check.jsp
        var maxWidth = 800;
        var maxHeight = 933;
        var imageScaleApplied = 1;
        var showSquare = false;
        var scale = 2;
        var imageWidth = 0;
        var imageHeight = 0;
        oft_x1 = '${otf.x1}';
        oft_y1 = '${otf.y1}';
        oft_x2 = '${otf.x2}';
        oft_y2 = '${otf.y2}';

        // Parsers map
        var parserMap = {};
        <c:forEach var="parser" items="${parsers}" varStatus="row">
        parserMap['${parser.getClass().getName()}'] = ${parser.patternRequired};
        </c:forEach>

        function preview(img, selection) {
            cropImage(selection.width, selection.height, selection.x1, selection.y1);
        }

        function cropImage(width, height, x1, y1) {
            $('#imageCroped').css({
                width: Math.floor(width * scale) + 'px',
                height: Math.floor(height * scale) + 'px'
            });

            $('#imageToCrop').css({
                margin: '-' + Math.floor(y1 * scale) + 'px 0 0 -' + Math.floor(x1 * scale) + 'px',
                width: Math.floor(imageWidth * scale) + 'px',
                height: Math.floor(imageHeight * scale) + 'px'
            });
        }

        function changeToContinueAction() {
            $('input[name="action"]').val('editContinueField');
        }

        $(document).ready(function () {
            $('#ocr_template').hide();
            $('#scroll').height($(window).height() - 21);
            $('select#otf_className').chosen({disable_search_threshold: 10});
            $('select#otf_pgname').chosen({disable_search_threshold: 10});
            $('select#otf_rotation').chosen({disable_search_threshold: 10});

            $('#otf_className').change(function () {
                // parserMap contains if pattern is required or not
                if (parserMap[$(this).val()]) {
                    $('#otf_pattern').addClass(':required :only_on_blur');
                } else {
                    $('#otf_pattern').removeClass(':required :only_on_blur');
                }
            });

            $('#action').submit(function () {
                // parserMap contains if pattern is required or not
                if (parserMap[$('#otf_className').val()] && $('#otf_pattern').val() == '') {
                    alert('Pattern needed');
                    return false;
                } else {
                    return true;
                }
            });
        });

        jQuery(window).load(function () {
            // Init real width and height
            imageWidth = $("#ocr_template").width();
            imageHeight = $("#ocr_template").height();
            realImageWidth = imageWidth; // Used to calculate image scale apply

            // Resize image
            if (imageWidth > maxWidth) {
                imageHeight = Math.floor(imageHeight * (maxWidth / imageWidth));
                imageWidth = maxWidth;
            }
            if (imageHeight > maxHeight) {
                imageWidth = Math.floor(imageWidth * (maxHeight / imageHeight));
                imageHeight = maxHeight;
            }

            imageScaleApplied = imageWidth / realImageWidth; // always will be 1 or decimal value
            oft_x1 = Math.floor(oft_x1 * imageScaleApplied);
            oft_y1 = Math.floor(oft_y1 * imageScaleApplied);
            oft_x2 = Math.floor(oft_x2 * imageScaleApplied);
            oft_y2 = Math.floor(oft_y2 * imageScaleApplied);

            $('#ocr_template').width(imageWidth);
            $('#ocr_template').height(imageHeight);
            $('#ocr_template').show();
            $('#loading').hide();

            $('#ocr_template').imgAreaSelect({
                onSelectEnd: function (img, selection) {
                    // calculate real value
                    $('input[name="otf_x1"]').val(Math.floor(selection.x1 / imageScaleApplied));
                    $('input[name="otf_y1"]').val(Math.floor(selection.y1 / imageScaleApplied));
                    $('input[name="otf_x2"]').val(Math.floor(selection.x2 / imageScaleApplied));
                    $('input[name="otf_y2"]').val(Math.floor(selection.y2 / imageScaleApplied));
                }
            });

            if ($('#otf_className').val() == 'com.openkm.ocr.template.parser.StringParser') {
                $('input[name="otf_filter_pattern"]').removeAttr("disabled")
                $('input[name="otf_word_list_uuid"]').removeAttr("disabled")
            } else {
                $('input[name="otf_filter_pattern"]').attr("disabled", "disabled");
                $('input[name="otf_word_list_uuid"]').attr("disabled", "disabled");
            }

            ias = $('#ocr_template').imgAreaSelect({handles: true, onSelectChange: preview});

            if (showSquare) {
                $('#ocr_template').imgAreaSelect({handles: true, x1: oft_x1, y1: oft_y1, x2: oft_x2, y2: oft_y2});
                cropImage((oft_x2 - oft_x1), (oft_y2 - oft_y1), oft_x1, oft_y1);
            }
        });

        function showFilterPatternElement(element) {
            console.log(element.value);
            if (element.value === 'com.openkm.ocr.template.parser.StringParser') {
                document.getElementById("otf_filter_pattern").removeAttribute("disabled");
                document.getElementById("otf_word_list_uuid").removeAttribute("disabled");
            } else {
                document.getElementById("otf_filter_pattern").setAttribute("disabled", "disabled");
                document.getElementById("otf_word_list_uuid").setAttribute("disabled", "disabled");
                document.getElementById("otf_filter_pattern").value = "";
                document.getElementById("otf_word_list_uuid").value = "";
            }
        }

        function setWordListPath(element) {
            var wordListFile = document.getElementById('otf_word_list_uuid');
            wordListFile.value = element.value;
        }

        function clearWordListPath() {
            var wordListFile = document.getElementById('otf_word_list_uuid');
            wordListFile.value = '';
        }
    </script>
    <title>OCR template field edit</title>
</head>
<body>
<c:choose>
    <c:when test="${u:isAdmin()}">
        <ul id="breadcrumb">
            <li class="path">
                <a href="OCRTemplate"><%out.print(LanguageDAO.i18n("ocr.template.title"));%></a>
            </li>
            <li class="path">
                <a href="OCRTemplate?action=fieldsList&ot_id=${ot.id}"><%out.print(LanguageDAO.i18n("ocr.template.definition"));%></a>
            </li>
            <li class="path">
                <c:choose>
                    <c:when test="${action == 'createField'}"><%out.print(LanguageDAO.i18n("ocr.template.createDCF"));%></c:when>
                    <c:when test="${action == 'editField'}">
                      <%out.print(LanguageDAO.i18n("ocr.template.editDCF"));%>
                        <script type="text/javascript">
                            showSquare = true;
                        </script>
                    </c:when>
                    <c:when test="${action == 'deleteField'}">
                      <%out.print(LanguageDAO.i18n("ocr.template.deleteDCF"));%>
                        <script type="text/javascript">
                            showSquare = true;
                        </script>
                    </c:when>
                </c:choose>
            </li>
        </ul>
        <div id="scroll" style="width: 100%; height: 100%; overflow: auto;">
            <br/>
            <c:url value="OCRTemplate" var="urlImage">
                <c:param name="action" value="image"/>
                <c:param name="ot_id" value="${ot.id}"/>
            </c:url>
            <form id="action" action="OCRTemplate" method="post">
                <input type="hidden" name="action" value="${action}"/>
                <input type="hidden" name="ot_id" value="${ot.id}"/>
                <input type="hidden" name="otf_id" value="${otf.id}"/>
                <table width="100%" align="center">
                    <tr>
                        <td width="5px"></td>
                        <td valign="top" align="right">
                            <div id="loading" style="width: 650px; text-align: center;"><%out.print(LanguageDAO.i18n("ocr.template.pleaseWait"));%>
                            </div>
                            <img id="ocr_template" src="${urlImage}" alt="<%out.print(LanguageDAO.i18n("ocr.template.imageTemplate"));%>"/>
                        </td>
                        <td width="5px"></td>
                        <td valign="top" width="100%">
                            <table class="form" align="left">
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template"));%></td>
                                    <td><b>${ot.name}</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr width="100%"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.name"));%></td>
                                    <td><input class=":required :only_on_blur" name="otf_name" size="40"
                                               value="${otf.name}"/></td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.type"));%></td>
                                    <td>
                                        <select class=":required :only_on_blur" id="otf_className"
                                                onchange="showFilterPatternElement(this)" name="otf_className"
                                                style="width: 125px" data-placeholder="Select type">
                                            <option value="">&nbsp;</option>
                                            <c:forEach var="parser" items="${parsers}">
                                                <c:choose>
                                                    <c:when test="${otf.className == parser.getClass().getName()}">
                                                        <option value="${parser.getClass().getName()}"
                                                                selected="selected">${parser.name}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${parser.getClass().getName()}">${parser.name}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.metadataField"));%></td>
                                    <td>
                                        <select class=":required :only_on_blur" name="otf_pgname" id="otf_pgname"
                                                style="width: 250px" data-placeholder="Select property">
                                            <option value="">&nbsp;</option>
                                            <c:forEach var="pgprop" items="${pgprops}">
                                                <c:choose>
                                                    <c:when test="${otf.propertyGroupName == pgprop}">
                                                        <option value="${pgprop}" selected="selected">${pgprop}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${pgprop}">${pgprop}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.pattern"));%></td>
                                    <td><input id="otf_pattern" name="otf_pattern" size="25" value="${otf.pattern}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.filterPattern"));%></td>
                                    <td>
                                        <input id="otf_filter_pattern" name="otf_filter_pattern" size="25"
                                               value="${otf.filterPattern}"/></td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.wordListUUID"));%></td>
                                    <td>
                                        <input id="otf_word_list_uuid" name="otf_word_list_uuid" size="25"
                                               value="${otf.wordListUUID}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.rotation"));%></td>
                                    <td>
                                        <select id="otf_rotation" name="otf_rotation" style="width: 75px">
                                            <c:forEach var="degree" items="${degrees}">
                                                <c:choose>
                                                    <c:when test="${otf.rotation == degree}">
                                                        <option value="${degree}" selected="selected">${degree}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${degree}">${degree}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.converterPath"));%></td>
                                    <td><textarea id="otf_custom" name="otf_custom" rows="5"
                                                  style="width:90%;">${otf.custom}</textarea></td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.ocrPath"));%></td>
                                    <td><input id="otf_ocr" name="otf_ocr" size="25" value="${otf.ocr}"
                                               style="width:90%;"/></td>
                                </tr>
                                <tr>
                                    <td><%out.print(LanguageDAO.i18n("ocr.template.useToRecognise"));%></td>
                                    <td><input type="checkbox" id="otf_control" name="otf_control"
                                               <c:if test="${otf.control}">checked="checked"</c:if>/></td>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.zone"));%></td>
                                    <td>
                                        <div id="imageCroped"
                                             style="float:left; position: relative; overflow:hidden; width: 0px; height: 0px;">
                                            <img id="imageToCrop" style="position: relative; margin: 0 0 0 0;"
                                                 src="${urlImage}"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        x1 <input readonly="readonly" class=":required :only_on_blur" name="otf_x1"
                                                  size="4" value="${otf.x1}"/>&nbsp;
                                        y1 <input readonly="readonly" class=":required :only_on_blur" name="otf_y1"
                                                  size="4" value="${otf.y1}"/><br/>
                                        x2 <input readonly="readonly" class=":required :only_on_blur" name="otf_x2"
                                                  size="4" value="${otf.x2}"/>&nbsp;
                                        y2 <input readonly="readonly" class=":required :only_on_blur" name="otf_y2"
                                                  size="4" value="${otf.y2}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <input type="button"
                                               onclick="javascript:window.location='OCRTemplate?action=fieldsList&ot_id=${ot.id}'"
                                               value="Cancel" class="noButton"/>
                                        <c:choose>
                                            <c:when test="${action == 'createField'}">
                                                <input type="submit" value="Create" class="yesButton"/>
                                            </c:when>
                                            <c:when test="${action == 'editField'}">
                                                <input type="submit" value="Edit" class="yesButton"/>
                                                <c:if test="${ot.fields.size() > 1}">
                                                    <input type="submit" onclick="javascript:changeToContinueAction();"
                                                           value="Edit & Next" class="yesButton"/>
                                                </c:if>
                                            </c:when>
                                            <c:when test="${action == 'deleteField'}">
                                                <input type="submit" value="Delete" class="yesButton"/>
                                            </c:when>
                                        </c:choose>
                                    </td>
                                </tr>
                                <c:choose>
                                    <c:when test="${action == 'editField'}">
                                        <tr>
                                            <td colspan="2">
                                                <hr width="100%"/>
                                                Other available fields:<br/><br/>
                                                <table class="results-old" width="80%">
                                                    <tr>
                                                        <th>Field</th>
                                                        <th width="50px">Action</th>
                                                    </tr>
                                                    <c:forEach var="field" items="${ot.fields}" varStatus="row">
                                                        <c:choose>
                                                            <c:when test="${otf.id != field.id}">
                                                                <c:url value="OCRTemplate" var="urlEdit">
                                                                    <c:param name="action" value="editField"/>
                                                                    <c:param name="ot_id" value="${ot.id}"/>
                                                                    <c:param name="otf_id" value="${field.id}"/>
                                                                </c:url>
                                                                <c:url value="OCRTemplate" var="urlDelete">
                                                                    <c:param name="action" value="deleteField"/>
                                                                    <c:param name="ot_id" value="${ot.id}"/>
                                                                    <c:param name="otf_id" value="${field.id}"/>
                                                                </c:url>
                                                                <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                                                                    <td height="19px">${field.name}</td>
                                                                    <td align="center">
                                                                        <a href="${urlEdit}"><img
                                                                                src="img/action/edit.png" alt="Edit"
                                                                                title="Edit"/></a>
                                                                        &nbsp;
                                                                        <a href="${urlDelete}"><img
                                                                                src="img/action/delete.png" alt="Delete"
                                                                                title="Delete"/></a>
                                                                    </td>
                                                                </tr>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                                                                    <td height="19px">${field.name}</td>
                                                                    <td align="center">&nbsp;</td>
                                                                </tr>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </table>
                                            </td>
                                        </tr>
                                    </c:when>
                                </c:choose>
                            </table>
                        </td>
                    </tr>
                </table>
            </form>
            <br/>
        </div>
    </c:when>
    <c:otherwise>
        <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
</c:choose>
</body>
</html>
