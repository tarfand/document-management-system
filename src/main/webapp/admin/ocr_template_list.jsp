<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="../css/dataTables-1.10.10/jquery.dataTables-1.10.10.min.css" />
  <link rel="stylesheet" type="text/css" href="css/style.css?v=1506588453493" />
  <script type="text/javascript" src="../js/utils.js"></script>
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables-1.10.10.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#results').dataTable({
        "bStateSave": true,
        "iDisplayLength": 10,
        "lengthMenu": [[10, 15, 20], [10, 15, 20]],
        "fnDrawCallback": function (oSettings) {
          dataTableAddRows(this, oSettings);
        }
      });
    });
  </script>
  <title><%out.print(LanguageDAO.i18n("ocr.template.title"));%></title>
</head>
<body dir="rtl">
  <c:choose>
    <c:when test="${u:isAdmin()}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="OCRTemplate"><%out.print(LanguageDAO.i18n("ocr.template.title"));%></a>
        </li>
      </ul>
      <br/>
      <div style="width:50%; margin-left:auto; margin-right:auto;">
       <table id="results" class="results">
        <thead>
          <tr>
            <th><%out.print(LanguageDAO.i18n("ocr.template.name"));%></th>
            <th width="120px"><%out.print(LanguageDAO.i18n("ocr.template.active"));%></th>
            <th width="120px">
              <c:url value="OCRTemplate" var="urlCreate">
                <c:param name="action" value="create"/>
              </c:url>
              <c:url value="OCRTemplate" var="urlRecognise">
                <c:param name="action" value="recognise"/>
              </c:url>
              <a href="${urlCreate}"><img src="img/action/new.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.new.template"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.new.template"));%>"/></a>
              &nbsp;
              <a href="${urlRecognise}"><img title="<%out.print(LanguageDAO.i18n("ocr.template.recognise"));%>" alt="<%out.print(LanguageDAO.i18n("ocr.template.recognise"));%>" src="img/action/recognize.png"/></a>
            </th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="ot" items="${ocrTemplates}" varStatus="row">
            <c:url value="OCRTemplate" var="urlEdit">
              <c:param name="action" value="edit"/>
              <c:param name="ot_id" value="${ot.id}"/>
            </c:url>
            <c:url value="OCRTemplate" var="urlDelete">
              <c:param name="action" value="delete"/>
              <c:param name="ot_id" value="${ot.id}"/>
            </c:url>
            <c:url value="OCRTemplate" var="urlFields">
              <c:param name="action" value="fieldsList"/>
              <c:param name="ot_id" value="${ot.id}"/>
            </c:url>
            <c:url value="OCRTemplate" var="urlCheck">
              <c:param name="action" value="check"/>
              <c:param name="ot_id" value="${ot.id}"/>
            </c:url>
            <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
              <td>${ot.name}</td>
              <td align="center">
                <c:choose>
                  <c:when test="${ot.active}">
                    <img src="img/true.png" alt="Active" title="Active"/>
                  </c:when>
                  <c:otherwise>
                    <img src="img/false.png" alt="Inactive" title="Inactive"/>
                  </c:otherwise>
                </c:choose>
              </td>
              <td align="center">
                <a href="${urlEdit}"><img src="img/action/edit.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.edit"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.edit"));%>"/></a>
                &nbsp;
                <a href="${urlDelete}"><img src="img/action/delete.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>"/></a>
                &nbsp;
                <a href="${urlFields}"><img src="img/action/params.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.fields"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.fields"));%>"/></a>
                &nbsp;
                <a href="${urlCheck}"><img title="<%out.print(LanguageDAO.i18n("ocr.template.check"));%>" alt="<%out.print(LanguageDAO.i18n("ocr.template.check"));%>" src="img/action/check.png"/></a>
              </td>
            </tr>
          </c:forEach>
        </tbody>
    </table>
   </div>
  </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
