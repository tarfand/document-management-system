<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/vanadium-min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('form').bind('submit', function(event) {
        var error = $('input[name="rol_id"] + span.vanadium-invalid');

        if (error == null || error.text() == '') {
          return true;
        } else {
          return false;
        }
      });
    });
  </script>
  <title><%out.print(LanguageDAO.i18n("management.roles.edit"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
        <a href="Auth?action=roleList"><%out.print(LanguageDAO.i18n("management.roles.list"));%></a>
        </li>
        <li class="path">
          <c:choose>
            <c:when test="${action == 'roleCreate'}"><%out.print(LanguageDAO.i18n("management.role.edit.create"));%></c:when>
            <c:when test="${action == 'roleEdit'}"><%out.print(LanguageDAO.i18n("management.role.edit.edit"));%></c:when>
            <c:when test="${action == 'roleDelete'}"><%out.print(LanguageDAO.i18n("management.role.edit.delete"));%></c:when>
          </c:choose>
        </li>
      </ul>
      <br/>
      <form action="Auth" method="post">
        <input type="hidden" name="action" value="${action}"/>
        <input type="hidden" name="persist" value="${persist}"/>
        <input type="hidden" name="csrft" value="${csrft}"/>
        <table class="form rtl-table" width="300px">
          <tr>
          <td><%out.print(LanguageDAO.i18n("management.role.edit.id"));%></td>
            <td width="100%">
              <c:choose>
                <c:when test="${action != 'roleCreate'}">
                  <input class=":required :only_on_blur" name="rol_id" size="45" value="${rol.id}" readonly="readonly"/>
                </c:when>
                <c:otherwise>
                  <input class=":required :only_on_blur :ajax;Auth?action=validateRole" name="rol_id" size="45" value=""/>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
          <td><%out.print(LanguageDAO.i18n("management.role.edit.active"));%></td>
            <td>
              <c:choose>
                <c:when test="${rol.active}">
                  <input name="rol_active" type="checkbox" checked="checked"/>
                </c:when>
                <c:otherwise>
                  <input name="rol_active" type="checkbox"/>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <c:choose>
              <c:when test="${action == 'roleCreate'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("management.role.edit.create"));%>" class="yesButton"/></c:when>
              <c:when test="${action == 'roleEdit'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("management.role.edit.edit"));%>" class="yesButton"/></c:when>
              <c:when test="${action == 'roleDelete'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("management.role.edit.delete"));%>" class="yesButton"/></c:when>
              </c:choose>
            <input type="button" onclick="javascript:window.history.back()" value="<%out.print(LanguageDAO.i18n("management.role.edit.cancel"));%>" class="noButton"/>

            </td>
          </tr>
        </table>
      </form>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
