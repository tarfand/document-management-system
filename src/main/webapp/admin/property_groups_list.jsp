<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.openkm.servlet.admin.BaseServlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="Shortcut icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/admin-style.css" />
<script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#scroll').height($(window).height() - 21);
  });
</script>
  <title><%out.print(LanguageDAO.i18n("profile.property.groups"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path"><a href="PropertyGroups"><%out.print(LanguageDAO.i18n("profile.property.groups"));%></a></li>
        <li class="action">
          <a href="PropertyGroups?action=register">
            <img src="img/action/generic.png" alt="Generic" title="Generic" style="vertical-align: middle;" />  ثبت فراداده
          </a>
        </li>
        <li class="action">
          <a href="PropertyGroups?action=edit">
            <img src="img/action/generic.png" alt="Generic" title="Generic" style="vertical-align: middle;" />  <%out.print(LanguageDAO.i18n("profile.property.groups.edit"));%>
          </a>
        </li>
      </ul>
      <div id="scroll" style="width: 100%; height: 100%; overflow: auto;">
        <br />
        <c:if test="${empty pGroups}">
      <table class="results-old" style="direction: rtl" width="80%">
            <tr>
          <th colspan="2"><%out.print(LanguageDAO.i18n("profile.property.groups.group.label"));%></th>
          <th colspan="3"><%out.print(LanguageDAO.i18n("profile.property.groups.group.name"));%></th>
          <th colspan="1"><%out.print(LanguageDAO.i18n("profile.property.groups.group.information"));%></th>
            </tr>
            <tr>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.label"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.name"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.width"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.height"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.field"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.other"));%></th>
            </tr>
          </table>
        </c:if>
        <c:forEach var="pGroup" items="${pGroups}">
      <table class="results-old" style="border-bottom: 0;direction: rtl" width="80%">
            <thead>
              <tr>
          <th colspan="2"><%out.print(LanguageDAO.i18n("profile.property.groups.group.label"));%></th>
          <th colspan="3"><%out.print(LanguageDAO.i18n("profile.property.groups.group.name"));%></th>
          <th colspan="1"><%out.print(LanguageDAO.i18n("profile.property.groups.group.information"));%></th>
              </tr>
            </thead>
            <tbody>
              <tr class="fuzzy">
                <td colspan="2" align="center"><b>${pGroup.key.label}</b></td>
                <td colspan="3" align="center"><b>${pGroup.key.name}</b></td>
                <td colspan="1" align="center"><i>Visible</i>: ${pGroup.key.visible}<br /> <i>ReadOnly</i>:
                  ${pGroup.key.readonly}</td>
              </tr>
            </tbody>
          </table>
      <table class="results-old id-results" style="border-top: 0;direction: rtl;text-align: center" width="80%">
            <thead>
              <tr>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.label"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.name"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.width"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.height"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.field"));%></th>
          <th><%out.print(LanguageDAO.i18n("profile.property.groups.other"));%></th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="pgForm" items="${pGroup.value}" varStatus="row">
                <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                  <td>${pgForm.label}</td>
                  <td>${pgForm.name}</td>
                  <td>${pgForm.width}</td>
                  <td>${pgForm.height}</td>
                  <td>${pgForm.field}</td>
                  <td width="45%">${pgForm.others}</td>
                </tr>
              </c:forEach>
            </tbody>
          </table>
          <br />
        </c:forEach>
      </div>
    </c:when>
    <c:otherwise>
      <div class="error">
  <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
