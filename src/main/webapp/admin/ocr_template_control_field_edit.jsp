<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/style.css?v=1506588453493" />
  <link rel="stylesheet" type="text/css" href="css/imgareaselect/imgareaselect-default.css" />
  <link rel="stylesheet" type="text/css" href="../css/chosen.css" />
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script src="../js/vanadium-min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/jquery.imgareaselect.pack.js"></script>
  <script type="text/javascript" src="../js/chosen.jquery.js"></script>
  <script type="text/javascript">
    // Keep in sync with ocr_template_check.jsp
    var maxWidth = 800;
    var maxHeight = 933;
    var imageScaleApplied = 1;
    var showSquare = false;
    var scale = 2;
    var imageWidth = 0;
    var imageHeight = 0;
    ofct_x1 = '${otcf.x1}';
    ofct_y1 = '${otcf.y1}';
    ofct_x2 = '${otcf.x2}';
    ofct_y2 = '${otcf.y2}';

    // Parsers map
    var parserMap = {};
    <c:forEach var="parser" items="${parsers}" varStatus="row">
      parserMap['${parser.getClass().getName()}'] = ${parser.patternRequired};
    </c:forEach>

    function preview(img, selection) {
      cropImage(selection.width, selection.height, selection.x1, selection.y1);
  }

  function cropImage(width, height, x1, y1) {
    $('#imageCroped').css({
      width: Math.floor(width * scale) + 'px',
      height: Math.floor(height * scale) + 'px'
    });

    $('#imageToCrop').css({
      margin: '-' + Math.floor(y1 * scale) + 'px 0 0 -' + Math.floor(x1 * scale) + 'px',
      width: Math.floor(imageWidth * scale) + 'px',
      height: Math.floor(imageHeight * scale) + 'px'
    });
  }

  function changeToContinueAction() {
    $('input[name="action"]').val('editContinueControlField');
  }

  $(document).ready(function() {
    $('#ocr_template').hide();
    $('#scroll').height($(window).height() - 21);
    $('select#otcf_className').chosen({disable_search_threshold: 10});
    $('select#otcf_rotation').chosen({disable_search_threshold: 10});

    $('#otcf_className').change(function() {
        // parserMap contains if pattern is required or not
      if (parserMap[$(this).val()]) {
        $('#otcf_pattern').addClass(':required :only_on_blur');
      } else {
        $('#otcf_pattern').removeClass(':required :only_on_blur');
      }
    });
    $('#action').submit(function() {
      // parserMap contains if pattern is required or not
      if (parserMap[$('#otcf_className').val()] && $('#otcf_pattern').val() == '') {
        alert('Pattern needed');
        return false;
      } else {
        return true;
      }
    });
  });

  jQuery(window).load(function () {
    // Init real width and height
    imageWidth = $("#ocr_template").width();
    imageHeight = $("#ocr_template").height();
    realImageWidth = imageWidth; // Used to calculate image scale apply

    // Resize image
    if (imageWidth > maxWidth) {
      imageHeight = Math.floor(imageHeight * (maxWidth / imageWidth));
      imageWidth = maxWidth;
    }
    if (imageHeight > maxHeight) {
      imageWidth = Math.floor(imageWidth * (maxHeight / imageHeight));
      imageHeight = maxHeight;
    }

    imageScaleApplied = imageWidth / realImageWidth; // always will be 1 or decimal value
    ofct_x1 = Math.floor(ofct_x1 * imageScaleApplied);
    ofct_y1 = Math.floor(ofct_y1 * imageScaleApplied);
    ofct_x2 = Math.floor(ofct_x2 * imageScaleApplied);
    ofct_y2 = Math.floor(ofct_y2 * imageScaleApplied);

    $('#ocr_template').width(imageWidth);
    $('#ocr_template').height(imageHeight);
    $('#ocr_template').show();
    $('#loading').hide();

    $('#ocr_template').imgAreaSelect({
      onSelectEnd: function (img, selection) {
        // calculate real value
        $('input[name="otcf_x1"]').val(Math.floor(selection.x1 / imageScaleApplied));
        $('input[name="otcf_y1"]').val(Math.floor(selection.y1 / imageScaleApplied));
        $('input[name="otcf_x2"]').val(Math.floor(selection.x2 / imageScaleApplied));
        $('input[name="otcf_y2"]').val(Math.floor(selection.y2 / imageScaleApplied));
      }
    });

    ias = $('#ocr_template').imgAreaSelect({ handles: true, onSelectChange: preview });

    if (showSquare) {
      $('#ocr_template').imgAreaSelect({ handles: true, x1: ofct_x1, y1: ofct_y1, x2: ofct_x2, y2: ofct_y2 });
      cropImage((ofct_x2-ofct_x1), (ofct_y2-ofct_y1), ofct_x1, ofct_y1);
    }
  });
  </script>
  <title>OCR template control field edit</title>
</head>
<body>
  <c:choose>
    <c:when test="${u:isAdmin()}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="OCRTemplate">OCR Template</a>
        </li>
        <li class="path">
          <a href="OCRTemplate?action=fieldsList&ot_id=${ot.id}">OCR Template definition</a>
        </li>
        <li class="path">
          <c:choose>
            <c:when test="${action == 'createControlField'}">Create Validation Control field</c:when>
            <c:when test="${action == 'editControlField'}">
              Edit Validation Control field
              <script type="text/javascript">
                showSquare = true;
              </script>
            </c:when>
            <c:when test="${action == 'deleteControlField'}">
              Delete Validation Control field
              <script type="text/javascript">
                showSquare = true;
              </script>
            </c:when>
          </c:choose>
        </li>
      </ul>
      <div id="scroll" style="width: 100%; height: 100%; overflow: auto;">
        <br/>
        <c:url value="OCRTemplate" var="urlImage">
          <c:param name="action" value="image"/>
          <c:param name="ot_id" value="${ot.id}"/>
        </c:url>
        <form id="action" action="OCRTemplate" method="post">
          <input type="hidden" name="action" value="${action}"/>
          <input type="hidden" name="ot_id" value="${ot.id}"/>
          <input type="hidden" name="otcf_id" value="${otcf.id}"/>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
          <table width="100%" align="center">
            <tr>
              <td width="5px"></td>
              <td valign="top" align="right">
                <div id="loading" style="width: 650px; text-align: center;">Please wait while image is loading</div>
                <img id="ocr_template" src="${urlImage}" alt="Image template"/>
              </td>
              <td width="5px"></td>
              <td valign="top" width="100%">
                <table class="form" align="left">
                  <tr>
                    <td>Template</td>
                    <td><b>${ot.name}</b></td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <hr width="100%" />
                    </td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td><input class=":required :only_on_blur" name="otcf_name" size="40" value="${otcf.name}"/></td>
                  </tr>
                  <tr>
                    <td>Order</td>
                    <td><input id="otcf_order" name="otcf_order" size="5" value="${otcf.order}"/></td>
                  </tr>
                  <tr>
                    <td>Type</td>
                    <td>
                      <select class=":required :only_on_blur" id="otcf_className" name="otcf_className" style="width: 150px" data-placeholder="Select type">
                        <option value="">&nbsp;</option>
                        <c:forEach var="controlParser" items="${controlParsers}">
                          <c:choose>
                            <c:when test="${otcf.className == controlParser.getClass().getName()}">
                              <option value="${controlParser.getClass().getName()}" selected="selected">${controlParser.name}</option>
                            </c:when>
                            <c:otherwise>
                              <option value="${controlParser.getClass().getName()}">${controlParser.name}</option>
                            </c:otherwise>
                          </c:choose>
                        </c:forEach>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td>Value</td>
                    <td><input id="otcf_value" name="otcf_value" size="25" value="${otcf.value}"/></td>
                  </tr>
                  <tr>
                    <td>Pattern</td>
                    <td><input id="otcf_pattern" name="otcf_pattern" size="25" value="${otcf.pattern}"/></td>
                  </tr>
                  <tr>
                    <td>Filter Pattern</td>
                    <td><input id="otcf_filter_pattern" name="otcf_filter_pattern" size="25" value="${otcf.filterPattern}"/></td>
                  </tr>
                  <tr>
                    <td>Rotation</td>
                    <td>
                      <select id="otcf_rotation" name="otcf_rotation" style="width: 75px">
                        <c:forEach var="degree" items="${degrees}">
                          <c:choose>
                            <c:when test="${otcf.rotation == degree}">
                              <option value="${degree}" selected="selected">${degree}</option>
                            </c:when>
                            <c:otherwise>
                              <option value="${degree}">${degree}</option>
                            </c:otherwise>
                          </c:choose>
                        </c:forEach>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td valign="top">Custom Image Converter Path</td>
                    <td><textarea id="otcf_custom" name="otcf_custom" rows="5" style="width:90%;">${otcf.custom}</textarea></td>
                  </tr>
                  <tr>
                    <td>Custom OCR Path</td>
                    <td><input id="otcf_ocr" name="otcf_ocr" size="25" value="${otcf.ocr}" style="width:90%;"/></td>
                  </tr>
                  <tr>
                    <td>Empty value allowed</td>
                    <td><input type="checkbox" id="otcf_emptyAllowed" name="otcf_emptyAllowed" <c:if test="${otcf.emptyAllowed}">checked="checked"</c:if>/></td>
                  </td>
                  <tr>
                    <td valign="top">Zone</td>
                    <td>
                      <div id="imageCroped" style="float:left; position: relative; overflow:hidden; width: 0px; height: 0px;">
                        <img id="imageToCrop" style="position: relative; margin: 0 0 0 0;" src="${urlImage}"  />
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="center">
                      x1 <input readonly="readonly" class=":required :only_on_blur" name="otcf_x1" size="4" value="${otcf.x1}"/>&nbsp;
                      y1 <input readonly="readonly" class=":required :only_on_blur" name="otcf_y1" size="4" value="${otcf.y1}"/><br/>
                      x2 <input readonly="readonly" class=":required :only_on_blur" name="otcf_x2" size="4" value="${otcf.x2}"/>&nbsp;
                      y2 <input readonly="readonly" class=":required :only_on_blur" name="otcf_y2" size="4" value="${otcf.y2}"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="center">
                      <input type="button" onclick="javascript:window.location='OCRTemplate?action=fieldsList&ot_id=${ot.id}'" value="Cancel" class="noButton"/>
                      <c:choose>
                        <c:when test="${action == 'createControlField'}">
                          <input type="submit" value="Create" class="yesButton"/>
                        </c:when>
                        <c:when test="${action == 'editControlField'}">
                          <input type="submit" value="Edit" class="yesButton"/>
                          <c:if test="${ot.fields.size() > 1}">
                            <input type="submit" onclick="javascript:changeToContinueAction();" value="Edit & Next" class="yesButton"/>
                          </c:if>
                        </c:when>
                        <c:when test="${action == 'deleteControlField'}">
                          <input type="submit" value="Delete" class="yesButton"/>
                        </c:when>
                      </c:choose>
                    </td>
                  </tr>
                  <c:choose>
                    <c:when test="${action == 'editcontrolField'}">
                      <tr>
                        <td colspan="2">
                          <hr width="100%" />
                          Other available fields:<br /><br />
                          <table class="results-old" width="80%">
                            <tr><th>Field</th><th width="50px">Action</th></tr>
                            <c:forEach var="field" items="${ot.fields}" varStatus="row">
                              <c:choose>
                                <c:when test="${otcf.id != field.id}">
                                  <c:url value="OCRTemplate" var="urlEdit">
                                    <c:param name="action" value="editControlField"/>
                                    <c:param name="ot_id" value="${ot.id}"/>
                                    <c:param name="otcf_id" value="${field.id}"/>
                                  </c:url>
                                  <c:url value="OCRTemplate" var="urlDelete">
                                    <c:param name="action" value="deleteControlField"/>
                                    <c:param name="ot_id" value="${ot.id}"/>
                                    <c:param name="otcf_id" value="${field.id}"/>
                                  </c:url>
                                  <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                                    <td height="19px">${field.name}</td>
                                    <td align="center">
                                      <a href="${urlEdit}"><img src="img/action/edit.png" alt="Edit" title="Edit"/></a>
                                      &nbsp;
                                      <a href="${urlDelete}"><img src="img/action/delete.png" alt="Delete" title="Delete"/></a>
                                    </td>
                                  </tr>
                                </c:when>
                                <c:otherwise>
                                  <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                                    <td height="19px">${field.name}</td>
                                    <td align="center">&nbsp;</td>
                                  </tr>
                                </c:otherwise>
                              </c:choose>
                            </c:forEach>
                          </table>
                        </td>
                      </tr>
                    </c:when>
                  </c:choose>
                </table>
              </td>
            </tr>
          </table>
        </form>
        <br/>
      </div>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
