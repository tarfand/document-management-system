<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.menu.bookmark.menu"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.bookmark.edit"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfBookmark.manageBookmarksVisible}">
            <input name="prf_menu_bookmark_manage_bookmarks_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_bookmark_manage_bookmarks_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.bookmark.add"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfBookmark.addBookmarkVisible}">
            <input name="prf_menu_bookmark_add_bookmark_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_bookmark_add_bookmark_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.bookmark.set.home"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfBookmark.setHomeVisible}">
            <input name="prf_menu_bookmark_set_home_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_bookmark_set_home_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.bookmark.home"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfBookmark.goHomeVisible}">
            <input name="prf_menu_bookmark_go_home_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_bookmark_go_home_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
