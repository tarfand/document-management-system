<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.general"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.general.name"));%></td>
      <td>
        <c:choose>
          <c:when test="${action == 'delete' || prf.name eq 'Default'}">
            <input class=":required :only_on_blur" name="prf_name" value="${prf.name}" readonly="readonly"/>
          </c:when>
          <c:otherwise>
            <input class=":required :only_on_blur" name="prf_name" value="${prf.name}"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.general.active"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.active}">
            <input name="prf_active" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_active" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
