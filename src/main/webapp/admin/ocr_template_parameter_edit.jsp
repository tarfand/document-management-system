<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="stylesheet" type="text/css" href="css/imgareaselect/imgareaselect-default.css" />
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script type="text/javascript" src="js/jquery.imgareaselect.pack.js"></script>
  <script type="text/javascript">
    function preview(img, selection) {
      $('#second').css({
        width: selection.width + 'px',
        height: selection.height + 'px',
      });

      $('#image').css({
        margin: '-'+selection.y1+'px 0 0 -'+selection.x1+'px'
      });
    }

    $(document).ready(function () {
      $('#ocr_template').imgAreaSelect({
        onSelectEnd: function (img, selection) {
          $('input[name="x1"]').val(selection.x1);
          $('input[name="y1"]').val(selection.y1);
          $('input[name="x2"]').val(selection.x2);
          $('input[name="y2"]').val(selection.y2);
        }
      });

      ias = $('#ocr_template').imgAreaSelect({ onSelectChange: preview });

      $('button#draw').click(function () {
        $('#ocr_template').imgAreaSelect({ x1: 120, y1: 90, x2: 280, y2: 210 });
      });
	});
  </script>
  <title><%out.print(LanguageDAO.i18n("ocr.template.title"));%></title>
</head>
<body>
  <table>
    <tr>
      <td valign="top">
        <img id="ocr_template" src="../ocr_template.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.imageTemplate"));%>" />
      </td>
      <td valign="top">
        <div id="second" style="overflow:hidden; width:200px; height: 100px;">
          <img id="image" style="margin: 0px;" src="../ocr_template.png"  />
        </div>
        <br/>
        <form>
          x1 <input name="x1" value=""/><br/>
          y1 <input name="y1" value=""/><br/>
          x2 <input name="x2" value=""/><br/>
          y2 <input name="y2" value=""/><br/>
        </form>
        <br/>
        <button id="draw" type="button"><%out.print(LanguageDAO.i18n("ocr.template.drawSquare"));%></button>
      </td>
    </tr>
  </table>
</body>
</html>
