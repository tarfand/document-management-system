<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="../css/chosen.css" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/vanadium-min.js"></script>
  <script type="text/javascript" src="../js/chosen.jquery.js"></script>
   <script type="text/javascript">
   	$(document).ready(function() {
   	 $('select#uc_profile').chosen({disable_search_threshold : 10 });
   	});
   </script>
  <title><%out.print(LanguageDAO.i18n("management.user.config"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="Auth"><%out.print(LanguageDAO.i18n("management.user.config.users.list"));%></a>
        </li>
        <li class="path"><%out.print(LanguageDAO.i18n("management.user.config.edit"));%></li>
      </ul>
      <br/>
      <form action="UserConfig">
        <input type="hidden" name="persist" value="${persist}"/>
        <input type="hidden" name="uc_user" value="${uc.user}"/>
        <table class="form rtl-table" width="150px">
          <tr>
          <td><%out.print(LanguageDAO.i18n("management.user.config.profile.user"));%></td>
            <td><input name="uc_user" value="${uc.user}" readonly/></td>
          </tr>
          <tr>
          <td nowrap="nowrap"><%out.print(LanguageDAO.i18n("management.user.config.profile.user.profile"));%></td>
            <td>
              <select name="uc_profile" id="uc_profile" data-placeholder="Select profile" style="width: 100%">
                <c:forEach var="up" items="${profiles}">
                  <c:choose>
                    <c:when test="${up.id == uc.profile.id}">
                      <option value="${up.id}" selected="selected">${up.name}</option>
                    </c:when>
                    <c:otherwise>
                      <option value="${up.id}">${up.name}</option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </select>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
            <input type="submit" value="<%out.print(LanguageDAO.i18n("management.user.config.send"));%>" class="yesButton"/>
            <input type="button" onclick="javascript:window.history.back()" value="<%out.print(LanguageDAO.i18n("management.user.config.cancel"));%>" class="noButton"/>
            </td>
          </tr>
        </table>
      </form>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
