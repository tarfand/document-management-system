<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script src="../js/vanadium-min.js" type="text/javascript"></script>

  <title><h3><%out.print(LanguageDAO.i18n("mime.types"));%></h3></title></head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="MimeType"><%out.print(LanguageDAO.i18n("mime.types"));%></a>
        </li>
        <li class="path">
          <c:choose>
            <c:when test="${action == 'create'}"><%out.print(LanguageDAO.i18n("mime.create"));%></c:when>
            <c:when test="${action == 'edit'}"><%out.print(LanguageDAO.i18n("mime.edit"));%></c:when>
            <c:when test="${action == 'delete'}"><%out.print(LanguageDAO.i18n("mime.delete"));%></c:when>
          </c:choose>
        </li>
      </ul>
      <br/>
      <form action="MimeType" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="${action}"/>
        <input type="hidden" name="mt_id" value="${mt.id}"/>
        <table class="form rtl-table" width="425px">
          <tr>
            <td><%out.print(LanguageDAO.i18n("mime.name"));%></td>
            <td><input class=":required :only_on_blur" size="36" name="mt_name" value="${mt.name}"/></td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("mime.description"));%></td>
            <td><input class=":required :only_on_blur" size="36" name="mt_description" value="${mt.description}"/></td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("mime.extensions"));%></td>
            <td><input class=":required :only_on_blur" name="mt_extensions" value="${extensions}"/></td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("mime.image"));%></td>
            <td>
              <c:choose>
                <c:when test="${action == 'create'}">
                  <input class=":required :only_on_blur" type="file" name="image"/>
                </c:when>
                <c:otherwise>
                  <c:url value="/mime/${mt.name}" var="urlIcon">
                  </c:url>
                  <table cellpadding="0" cellspacing="0"><tr><td><img src="${urlIcon}"/>&nbsp;</td><td><input type="file" name="image"/></td></tr></table>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("mime.search"));%></td>
            <td>
              <c:choose>
                <c:when test="${mt.search}">
                  <input name="mt_search" type="checkbox" checked="checked"/>
                </c:when>
                <c:otherwise>
                  <input name="mt_search" type="checkbox"/>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <c:url value="MimeType" var="urlCancel">
              </c:url>
              <c:choose>
                <c:when test="${action == 'create'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("mime.create"));%>" class="yesButton"/></c:when>
                <c:when test="${action == 'edit'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("mime.edit"));%>" class="yesButton"/></c:when>
                <c:when test="${action == 'delete'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("mime.delete"));%>" class="yesButton"/></c:when>
              </c:choose>
              <input type="button" onclick="javascript:window.location.href='${urlCancel}'" value="<%out.print(LanguageDAO.i18n("mime.cancel"));%>" class="noButton"/>

            </td>
          </tr>
        </table>
      </form>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
