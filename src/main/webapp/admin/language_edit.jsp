<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/vanadium-min.js"></script>
    <title><%out.print(LanguageDAO.i18n("language.edit.label"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isMultipleInstancesAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
	 <ul id="breadcrumb">
	   <li class="path">
           <a href="Language"><%out.print(LanguageDAO.i18n("language.list"));%></a>
	   </li>
	   <li class="path">
	     <c:choose>
	   	   <c:when test="${action == 'create'}"><%out.print(LanguageDAO.i18n("language.create"));%></c:when>
	   	   <c:when test="${action == 'edit'}"><%out.print(LanguageDAO.i18n("language.edit"));%></c:when>
	   	   <c:when test="${action == 'delete'}"><%out.print(LanguageDAO.i18n("language.delete"));%></c:when>
	      </c:choose>
	    </li>
	  </ul>
      <br/>
      <form action="Language" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="${action}"/>
        <input type="hidden" name="persist" value="${persist}"/>
        <table class="form rtl-table" width="372px">
          <tr>
            <td>شناسه</td>
            <td width="100%">
              <c:choose>
                <c:when test="${action != 'create'}">
                  <input size="5" class=":required :only_on_blur" name="lg_id" value="${lg.id}" readonly="readonly"/>
                </c:when>
                <c:otherwise>
                  <input class=":required :only_on_blur" name="lg_id" value=""/>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td>نام</td>
            <td><input class=":required :only_on_blur" name="lg_name" value="${lg.name}"/></td>
          </tr>
          <tr>
            <td>پرچم</td>
            <td>
              <c:choose>
                <c:when test="${action == 'create'}">
                  <input class=":required :only_on_blur" type="file" name="image"/>
                </c:when>
                <c:otherwise>
                  <c:url value="Language" var="urlFlag">
                    <c:param name="action" value="flag"/>
                    <c:param name="lg_id" value="${lg.id}"/>
                  </c:url>
                  <table cellpadding="0" cellspacing="0"><tr><td><img src="${urlFlag}"/>&nbsp;</td><td><input type="file" name="image"/></td></tr></table>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <c:choose>
                <c:when test="${action == 'create'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("language.create"));%>" class="yesButton"/></c:when>
                <c:when test="${action == 'edit'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("language.edit"));%>" class="yesButton"/></c:when>
                <c:when test="${action == 'delete'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("language.delete"));%>" class="yesButton"/></c:when>
              </c:choose>
                <input type="button" onclick="javascript:window.history.back()" value="<%out.print(LanguageDAO.i18n("language.cancel"));%>" class="noButton"/>

            </td>
          </tr>
        </table>
      </form>
  	</c:when>
	<c:otherwise>
	  <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
	</c:otherwise>
  </c:choose>
</body>
</html>
