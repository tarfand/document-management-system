<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="../css/dataTables-1.10.10/jquery.dataTables-1.10.10.min.css" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/utils.js"></script>
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables-1.10.10.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#results').dataTable({
        "bStateSave": true,
        "iDisplayLength": 15,
        "lengthMenu": [[10, 15, 20], [10, 15, 20]],
        "fnDrawCallback": function (oSettings) {
          dataTableAddRows(this, oSettings);
        }
      });
    });
  </script>

    <title><%out.print(LanguageDAO.i18n("mime.types"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
                <a href="MimeType"><%out.print(LanguageDAO.i18n("mime.types"));%></a>
        </li>
      </ul>
      <br/>
        <div style="width: 90%; margin-left: auto; margin-right: auto;">

            <table class="results" id="results">
	       <thead>
	         <tr>
                    <th><%out.print(LanguageDAO.i18n("mime.name"));%></th>
                    <th><%out.print(LanguageDAO.i18n("mime.description"));%></th>
                    <th><%out.print(LanguageDAO.i18n("mime.image"));%></th>
                    <th><%out.print(LanguageDAO.i18n("mime.extensions"));%></th>
                    <th><%out.print(LanguageDAO.i18n("mime.search"));%></th>
	           <th width="50px">
	             <c:url value="MimeType" var="urlCreate">
	               <c:param name="action" value="create"/>
	             </c:url>
	             <c:url value="MimeType" var="urlExport">
	               <c:param name="action" value="export"/>
	             </c:url>
                        <a href="${urlCreate}"><img src="img/action/new.png" alt="New mime type"
                                                    title="<%out.print(LanguageDAO.i18n("mime.new"));%>"/></a>
                        <a href="${urlExport}"><img src="img/action/export_sql.png" alt="SQL export"
                                                    title="<%out.print(LanguageDAO.i18n("mime.export"));%>"/></a>
	           </th>
	         </tr>
	       </thead>
	       <tbody>
                <c:forEach var="mt" items="${mimeTypes}" varStatus="xx">
	           <c:url value="/mime/${mt.name}" var="urlIcon">
	           </c:url>
	           <c:url value="MimeType" var="urlEdit">
	             <c:param name="action" value="edit"/>
	             <c:param name="mt_id" value="${mt.id}"/>
	           </c:url>
	           <c:url value="MimeType" var="urlDelete">
	             <c:param name="action" value="delete"/>
	             <c:param name="mt_id" value="${mt.id}"/>
	           </c:url>
                    <tr class="${xx.index % 2 == 0 ? 'even' : 'odd'}">
	             <td>${mt.name}</td>
	             <td>${mt.description}</td>
	             <td align="center"><img src="${urlIcon}"/></td>
	             <td>${mt.extensions}</td>
	             <td align="center">
	               <c:choose>
	                 <c:when test="${mt.search}">
                                    <img src="img/true.png" alt="Search"
                                         title="<%out.print(LanguageDAO.i18n("mime.show.in.search"));%>"/>
	                 </c:when>
	                 <c:otherwise>
                                    <img src="img/false.png" alt="No Search"
                                         title="<%out.print(LanguageDAO.i18n("mime.do.not.show.in.search"));%>"/>
	                 </c:otherwise>
	               </c:choose>
	             </td>
                        <td align="center">
                            <a href="${urlEdit}"><img src="img/action/edit.png" alt="Edit"
                                                      title="<%out.print(LanguageDAO.i18n("mime.edit"));%>"/></a>
	               &nbsp;
                            <a href="${urlDelete}"><img src="img/action/delete.png" alt="Delete"
                                                        title="<%out.print(LanguageDAO.i18n("mime.delete"));%>"/></a>
	             </td>
	           </tr>
	         </c:forEach>
	       </tbody>
	       <tfoot>
		       <tr>
		         <td align="right" colspan="6">
		           <form action="MimeType" method="post" enctype="multipart/form-data">
		             <input type="hidden" name="action" value="import"/>
	                 <input class=":required :only_on_blur" type="file" name="sql-file"/>
	                 <input type="submit" value="Import mime types" class="addButton"/>
		           </form>
		         </td>
		       </tr>
	       </tfoot>
	      </table>
      </div>
    </c:when>
    <c:otherwise>
        <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
