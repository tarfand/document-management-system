<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="../css/dataTables-1.10.10/jquery.dataTables-1.10.10.min.css" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/utils.js"></script>
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables-1.10.10.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function () {
      $('#results').dataTable({
        "bStateSave": true,
        "iDisplayLength": 10,
        "lengthMenu": [[10, 15, 20], [10, 15, 20]],
        "fnDrawCallback": function (oSettings) {
          dataTableAddRows(this, oSettings);
        }
      });
    });
  </script>
  <title><%out.print(LanguageDAO.i18n("management.roles.list"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="Auth?action=roleList"><%out.print(LanguageDAO.i18n("management.roles.list"));%></a>
        </li>
        <li class="action">
          <a href="Auth">
            <img src="img/action/generic.png" alt="Generic" title="Generic" style="vertical-align: middle;"/>
          <%out.print(LanguageDAO.i18n("management.roles.list.users.list"));%>
          </a>
        </li>
      </ul>
      <br/>
      <div style="width: 40%; margin-left: auto; margin-right: auto;">
        <table id="results" class="results rtl-table">
          <thead>
            <tr>
              <th>#</th>
              <th><%out.print(LanguageDAO.i18n("management.role.edit.id"));%></th>
              <th width="25px"><%out.print(LanguageDAO.i18n("management.role.edit.active"));%></th>
              <th width="75px"><c:url value="Auth" var="urlCreate">
                  <c:param name="action" value="roleCreate" />
                </c:url> <c:url value="Auth" var="urlExport">
                  <c:param name="action" value="roleListExport" />
                </c:url>
                <c:if test="${db}">
              <a href="${urlCreate}"><img src="img/action/new.png" alt="New role" title="<%out.print(LanguageDAO.i18n("management.role.new"));%>" /></a>
	              &nbsp;
	            </c:if>
            <a href="${urlExport}"><img src="img/action/export_csv.png" alt="CSV export" title="<%out.print(LanguageDAO.i18n("management.role.list.export"));%>CSV" /></a>
              </th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="role" items="${roles}" varStatus="row">
              <c:url value="Auth" var="urlEdit">
                <c:param name="action" value="roleEdit" />
                <c:param name="rol_id" value="${role.id}" />
              </c:url>
              <c:url value="Auth" var="urlDelete">
                <c:param name="action" value="roleDelete" />
                <c:param name="rol_id" value="${role.id}" />
              </c:url>
              <c:url value="Auth" var="urlActive">
                <c:param name="action" value="roleActive" />
                <c:param name="rol_id" value="${role.id}" />
                <c:param name="rol_active" value="${!role.active}" />
              </c:url>
              <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                <td width="20px">${row.index + 1}</td>
                <td>${role.id}</td>
                <td align="center">
                  <c:choose>
                    <c:when test="${db}">
                      <c:choose>
                        <c:when test="${role.active}">
                      <a href="${urlActive}"><img src="img/true.png" alt="Active" title="<%out.print(LanguageDAO.i18n("management.role.edit.active"));%>" /></a>
                        </c:when>
                        <c:otherwise>
                      <a href="${urlActive}"><img src="img/false.png" alt="Inactive" title="<%out.print(LanguageDAO.i18n("management.role.list.inactive"));%>" /></a>
                        </c:otherwise>
                      </c:choose>
                    </c:when>
                    <c:otherwise>
                  <img src="img/true.png" alt="Active" title="<%out.print(LanguageDAO.i18n("management.role.edit.active"));%>" />
                    </c:otherwise>
                  </c:choose>
                </td>
                <td align="center">
                  <c:if test="${db}">
                  <a href="${urlEdit}"><img src="img/action/edit.png" alt="Edit" title="<%out.print(LanguageDAO.i18n("management.role.edit.edit"));%>"/></a>
	                &nbsp;
	                <a href="${urlDelete}"><img src="img/action/delete.png" alt="Delete" title="<%out.print(LanguageDAO.i18n("management.role.edit.delete"));%>" /></a>
                  </c:if>
                </td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
