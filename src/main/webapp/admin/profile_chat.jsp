<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.chat"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.active"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfChat.chatEnabled}">
            <input name="prf_chat_enabled" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_chat_enabled" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.auto.login"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfChat.autoLoginEnabled}">
            <input name="prf_chat_auto_login" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_chat_auto_login" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
