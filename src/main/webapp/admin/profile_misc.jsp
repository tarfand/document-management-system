<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ page pageEncoding="utf-8" %>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.misc"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.misc.user.quota"));%></td>
      <td><input class=":integer :only_on_blur" name="prf_misc_user_quota" value="${prf.prfMisc.userQuota}" size="10"/>
      </td>
    </tr>
    <tr>
      <td>Advanced filters</td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMisc.advancedFilters}">
            <input name="prf_misc_advanced_filter" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_misc_advanced_filter" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.misc.web.skin"));%></td>
      <td>
        <select name="prf_misc_web_skin" id="prf_misc_web_skin" data-placeholder="Select skin" style="width: 200px">
          <c:choose>
            <c:when test="${prf.prfMisc.webSkin == 'default'}">
              <option value="default"
                      selected="selected"><%out.print(LanguageDAO.i18n("profile.misc.default"));%></option>
            </c:when>
            <c:otherwise>
              <option value="default"><%out.print(LanguageDAO.i18n("profile.misc.default"));%></option>
            </c:otherwise>
          </c:choose>
          <c:choose>
            <c:when test="${prf.prfMisc.webSkin == 'mediumfont'}">
              <option value="mediumfont"
                      selected="selected"><%out.print(LanguageDAO.i18n("profile.misc.medium.font"));%>فونت متوسط
              </option>
            </c:when>
            <c:otherwise>
              <option value="mediumfont"><%out.print(LanguageDAO.i18n("profile.misc.medium.font"));%></option>
            </c:otherwise>
          </c:choose>
          <c:choose>
            <c:when test="${prf.prfMisc.webSkin == 'bigfont'}">
              <option value="bigfont"
                      selected="selected"><%out.print(LanguageDAO.i18n("profile.misc.big.font"));%></option>
            </c:when>
            <c:otherwise>
              <option value="bigfont"><%out.print(LanguageDAO.i18n("profile.misc.big.font"));%></option>
            </c:otherwise>
          </c:choose>
        </select>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.misc.print.preview"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMisc.printPreview}">
            <input name="prf_misc_print_preview" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_misc_print_preview" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td colspan="2"><%out.print(LanguageDAO.i18n("profile.misc.keyword.in.property.panel"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMisc.keywordsEnabled}">
            <input name="prf_misc_keywords_enabled" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_misc_keywords_enabled" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td colspan="2"><%out.print(LanguageDAO.i18n("profile.misc.upload.notify.users"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMisc.uploadNotifyUsers}">
            <input name="prf_misc_upload_notify_users" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_misc_upload_notify_users" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td colspan="2"><%out.print(LanguageDAO.i18n("profile.misc.notify.external.users"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMisc.notifyExternalUsers}">
            <input name="prf_misc_notify_external_users" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_misc_notify_external_users" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td>Acrobat plugin preview</td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMisc.acrobatPluginPreview}">
            <input name="prf_misc_acrobat_plugin_preview" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_misc_acrobat_plugin_preview" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td colspan="2"><%out.print(LanguageDAO.i18n("profile.misc.increase.version"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMisc.increaseVersion}">
            <input name="prf_misc_increase_version" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_misc_increase_version" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.misc.modules"));%></td>
      <td>
        <select name="prf_misc_extensions" id="prf_misc_extensions" data-placeholder="انتخاب ماژول"
                multiple="multiple" style="width: 230px">
          <c:forEach var="ext" items="${exts}">
            <c:choose>
              <c:when test="${u:contains(prf.prfMisc.extensions, ext.uuid)}">
                <option value="${ext.uuid}" selected="selected">${ext.name}</option>
              </c:when>
              <c:otherwise>
                <option value="${ext.uuid}">${ext.name}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.misc.reports"));%></td>
      <td>
        <select name="prf_misc_reports" id="prf_misc_reports" data-placeholder="انتخاب گزارش"
                multiple="multiple" style="width: 230px">
          <c:forEach var="rep" items="${reps}">
            <c:choose>
              <c:when test="${u:contains(prf.prfMisc.reports, rep.id)}">
                <option value="${rep.id}" selected="selected">${rep.name}</option>
              </c:when>
              <c:otherwise>
                <option value="${rep.id}">${rep.name}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.misc.workflows"));%></td>
      <td>
        <select name="prf_misc_workflows" id="prf_misc_workflows" data-placeholder="انتخاب گردش کار"
                multiple="multiple" style="width: 230px">
          <c:forEach var="wf" items="${wflows}">
            <c:choose>
              <c:when test="${u:contains(prf.prfMisc.workflows, wf.name)}">
                <option value="${wf.name}" selected="selected">${wf.name}</option>
              </c:when>
              <c:otherwise>
                <option value="${wf.name}">${wf.name}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
  </table>
</fieldset>
