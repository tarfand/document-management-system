<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/style.css?v=1506588453493" />
  <link rel="stylesheet" type="text/css" href="../css/chosen.css" />
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script src="../js/vanadium-min.js" type="text/javascript"></script>
  <script type="text/javascript" src="../js/chosen.jquery.js"></script>
  <title><%out.print(LanguageDAO.i18n("ocr.template.title"));%></title>
  <script type="text/javascript">
    (function($) {
      $.fn.option=function(value) {
        return this.children().filter(function(index, option) {
          return option.value===value;
        });
      };
    })(jQuery);

    jQuery(window).load(function() {
      scale = 1;
      height = $('#image').height();
      width = $('#image').width();

      if (height > 300) {
        scale = 300 / height;
        height = 300;
      }

      width = Math.floor(width * scale); // round number

      if (width > 200) {
        scale = 200 / width;
        width = 200;
        height = Math.floor(height*scale); // round number
      }

      $('#image').width(width);
      $('#image').height(height);
    });

    function hideAll() {
      $('#addGroup').hide();
    }

    function showAll() {
      $('#addGroup').show();
    }

    function removeRow(property, tr) {
        $('#ot_properties').append("<option value='"+property+"'>"+property+"</option>");
        tr.remove();
    }

    $(document).ready(function() {
      $('select#ot_properties').chosen({disable_search_threshold: 10});

      $("#addProperty").click(function(event) {
        var value = $("#ot_properties").val();
        if (value != '') {
          hideAll();
          data = '<tr>';
          data = data +'<td>'+value+'</td>';
          data = data +'<td><input name="ot_property_name" value='+value+' type="hidden"></td>';
          data = data +'<td><input name="ot_property_value" type="text" style="width:200px;"></td>';
          data = data +'<td><img onclick="javascript:removeRow(\''+value+'\',$(this).closest(\'tr\'));" title="Delete" alt="Delete" src="img/action/delete.png" style="cursor:pointer; cursor:hand;"/></td>';
          data = data + '</tr>';
          $('#propertiesTable > tbody:last').append(data);
          $('#ot_properties').option(value).remove();
        } else {
          alert('Should select a property to be added');
        }
      });
    });
  </script>
</head>
<body>
  <c:choose>
    <c:when test="${u:isAdmin()}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="OCRTemplate"><%out.print(LanguageDAO.i18n("ocr.template.title"));%></a>
        </li>
        <li class="path">
          <c:choose>
            <c:when test="${action == 'create'}"><%out.print(LanguageDAO.i18n("ocr.template.createTemplate"));%></c:when>
            <c:when test="${action == 'edit'}"><%out.print(LanguageDAO.i18n("ocr.template.editTemplate"));%></c:when>
            <c:when test="${action == 'delete'}"><%out.print(LanguageDAO.i18n("ocr.template.deleteTemplate"));%></c:when>
          </c:choose>
        </li>
      </ul>
      <br/>
      <form dir="rtl" action="OCRTemplate" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="${action}"/>
        <input type="hidden" name="ot_id" value="${ot.id}"/>
        <table class="form" width="425px" style="white-space: nowrap;">
          <tr>
            <td><%out.print(LanguageDAO.i18n("ocr.template.name"));%></td>
            <td><input class=":required :only_on_blur" size="36" name="ot_name" value="${ot.name}"/></td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("ocr.template.image"));%></td>
            <td>
              <c:choose>
                <c:when test="${action == 'create'}">
                  <input class=":required :only_on_blur" type="file" name="image"/>
                </c:when>
                <c:otherwise>
                  <c:url value="OCRTemplate" var="urlImage">
                    <c:param name="action" value="image"/>
                    <c:param name="ot_id" value="${ot.id}"/>
                  </c:url>
                  <table cellpadding="0" cellspacing="0"><tr><td><img id="image" src="${urlImage}"/></td><td width="10">&nbsp;</td><td><input type="file" name="image"/></td></tr></table>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("ocr.template.metadataField"));%></td>
            <td>
              <table cellspacing="0" cellpadding="0">
              <tr align="left">
                <td align="left">
                  <select id="ot_properties" style="width: 250px" data-placeholder="<%out.print(LanguageDAO.i18n("ocr.template.selectMetadataField"));%>">
                    <option value=""></option>
                    <c:forEach var="pgprop" items="${pgprops}" varStatus="row">
                      <option value="${pgprop}">${pgprop}</option>
                    </c:forEach>
                  </select>
                </td>
                <td width="10px">&nbsp;</td>
                <td>
                  <img id="addProperty" title="<%out.print(LanguageDAO.i18n("ocr.template.newMetadataField"));%>" alt="<%out.print(LanguageDAO.i18n("ocr.template.newMetadataField"));%>" style="cursor:pointer; cursor:hand;" src="img/action/new.png"/>
                </td>
              </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <table id="propertiesTable">
                <tbody>
                <c:forEach var="prop" items="${ot.properties}" varStatus="row">
                  <tr>
                    <td>${prop.name}</td>
                    <td><input name="ot_property_name" value="${prop.name}" type="hidden" /></td>
                    <td><input name="ot_property_value" type="text" value="${prop.value}" style="width:200px;"/></td>
                    <td><img onclick="javascript:removeRow('${prop.name}',$(this).closest('tr'));" title="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>" alt="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>" src="img/action/delete.png" style="cursor:pointer; cursor:hand;" /></td>
                  </tr>
                </c:forEach>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("ocr.template.active"));%></td>
            <td>
              <c:choose>
                <c:when test="${ot.active}">
                  <input name="ot_active" id="ot_active" type="checkbox" checked="checked"/>
                </c:when>
                <c:otherwise>
                  <input name="ot_active" id="ot_active" type="checkbox"/>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <input type="button" onclick="javascript:window.history.back()" value="<%out.print(LanguageDAO.i18n("ocr.template.cancel"));%>" class="noButton"/>
              <c:choose>
                <c:when test="${action == 'create'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("ocr.template.create"));%>" class="yesButton"/></c:when>
                <c:when test="${action == 'edit'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("ocr.template.edit"));%>" class="yesButton"/></c:when>
                <c:when test="${action == 'delete'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>" class="yesButton"/></c:when>
              </c:choose>
            </td>
          </tr>
        </table>
      </form>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
