<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.stacks"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.stacks.taxonomy"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfStack.taxonomyVisible}">
            <input name="prf_stack_taxonomy_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_stack_taxonomy_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.stacks.categories"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfStack.categoriesVisible}">
            <input name="prf_stack_categories_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_stack_categories_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.stacks.thesaurus"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfStack.thesaurusVisible}">
            <input name="prf_stack_thesaurus_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_stack_thesaurus_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.stacks.templates"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfStack.templatesVisible}">
            <input name="prf_stack_templates_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_stack_templates_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.stacks.personal"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfStack.personalVisible}">
            <input name="prf_stack_personal_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_stack_personal_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.stacks.mail"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfStack.mailVisible}">
            <input name="prf_stack_mail_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_stack_mail_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.stacks.metadata"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfStack.metadataVisible}">
            <input name="prf_stack_metadata_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_stack_metadata_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.stacks.trash"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfStack.trashVisible}">
            <input name="prf_stack_trash_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_stack_trash_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
