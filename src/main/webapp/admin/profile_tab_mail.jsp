<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.tabs.mail"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.mail.properties"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfMail.propertiesVisible}">
            <input name="prf_tab_mail_properties_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_mail_properties_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.mail.preview"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfMail.previewVisible}">
            <input name="prf_tab_mail_preview_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_mail_preview_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.mail.security"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfMail.securityVisible}">
            <input name="prf_tab_mail_security_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_mail_security_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.mail.notes"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfMail.notesVisible}">
            <input name="prf_tab_mail_notes_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_mail_notes_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
