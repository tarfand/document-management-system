<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/style.css?v=1506588453493" />
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script src="../js/vanadium-min.js" type="text/javascript"></script>
  </head>
<body dir="rtl">
  <c:choose>
    <c:when test="${u:isAdmin()}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="OCRTemplate"><%out.print(LanguageDAO.i18n("ocr.template.title"));%> </a>
        </li>
        <li class="path">
          <%out.print(LanguageDAO.i18n("ocr.template.recognise"));%>
        </li>
      </ul>
      <br/>
      <table class="results-old" width="50%">
        <thead>
          <tr>
            <th><%out.print(LanguageDAO.i18n("ocr.template.name"));%></th>
            <th width="150px"><%out.print(LanguageDAO.i18n("ocr.template.confidenceLevel"));%></th>
            <th width="100px"><%out.print(LanguageDAO.i18n("ocr.template.control"));%></th>
            <th width="80px"><%out.print(LanguageDAO.i18n("ocr.template.recognised"));%></th>
          </tr>
        </thead>
      	<tbody>
          <c:forEach var="rec" items="${recognize.confidences}" varStatus="row">
            <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
              <td>${rec.template.name}</td>
              <td align="right">${rec.percentage} %&nbsp;</td>
              <td align="right">
                (
                <c:choose>
                  <c:when test="${level == 3}">
                    ${rec.textParsed + rec.textExtracted + rec.cropZonesAvailable} of ${rec.cropZones * 3}
                  </c:when>
                  <c:when test="${level == 2}">
                    ${rec.textExtracted + rec.cropZonesAvailable} of ${rec.cropZones * 2}
                  </c:when>
                  <c:otherwise>
                    ${rec.cropZonesAvailable} of ${rec.cropZones}
                  </c:otherwise>
                </c:choose>
                )&nbsp;
              </td>
              <td align="center">
              	<c:choose>
                  <c:when test="${rec.recognized}">
                    <img src="img/true.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.recognised"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.recognised"));%>"/>
                  </c:when>
                  <c:otherwise>
                    <img src="img/false.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.recognised"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.recognised"));%>"/>
                  </c:otherwise>
                </c:choose>
              </td>
            </tr>
          </c:forEach>
        </tbody>
	  </table>
      </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
