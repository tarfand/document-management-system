<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.menu.menu.help"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.help.documentation"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfHelp.documentationVisible}">
            <input name="prf_menu_help_documentation_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_help_documentation_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.help.bug.tracking"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfHelp.bugTrackingVisible}">
            <input name="prf_menu_help_bug_tracking_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_help_bug_tracking_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.help.support"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfHelp.supportVisible}">
            <input name="prf_menu_help_support_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_help_support_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.help.forum"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfHelp.forumVisible}">
            <input name="prf_menu_help_forum_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_help_forum_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.help.change.log"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfHelp.changelogVisible}">
            <input name="prf_menu_help_changelog_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_help_changelog_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.help.web.site"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfHelp.webSiteVisible}">
            <input name="prf_menu_help_web_site_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_help_web_site_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.help.about"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfHelp.aboutVisible}">
            <input name="prf_menu_help_about_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_help_about_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
