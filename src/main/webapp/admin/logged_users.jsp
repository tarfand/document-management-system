<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="../css/dataTables-1.10.10/jquery.dataTables-1.10.10.min.css" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css"/>
  <script type="text/javascript" src="../js/utils.js"></script>
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables-1.10.10.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#results').dataTable({
        "bStateSave": true,
        "iDisplayLength": 10,
        "lengthMenu": [[10, 15, 20], [10, 15, 20]],
        "fnDrawCallback": function (oSettings) {
          dataTableAddRows(this, oSettings);
        }
      });
    });
  </script>
  <title><%out.print(LanguageDAO.i18n("loged.users"));%></title></head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="Auth"><%out.print(LanguageDAO.i18n("loged.users.list"));%></a>
        </li>
        <li class="path"><%out.print(LanguageDAO.i18n("loged.users"));%></li>
        <li class="action">
          <a href="LoggedUsers">
          	<img src="img/action/refresh.png" alt="Refresh" title="<%out.print(LanguageDAO.i18n("loged.users.refresh"));%>" style="vertical-align: middle;"/>
            <%out.print(LanguageDAO.i18n("loged.users.refresh"));%>
          </a>
        </li>
      </ul>
      <br/>
      <div style="width: 80%; margin-left: auto; margin-right: auto;">
        <table id="results" class="results">
          <thead>
            <tr>
              <th width="20px">#</th>
            <th width="100px"><%out.print(LanguageDAO.i18n("loged.users.user"));%></th>
            <th width="225px"><%out.print(LanguageDAO.i18n("loged.users.session.id"));%></th>
            <th><%out.print(LanguageDAO.i18n("loged.users.ip.address"));%></th>
            <th><%out.print(LanguageDAO.i18n("loged.users.server.address"));%></th>
            <th width="125px"><%out.print(LanguageDAO.i18n("loged.users.create"));%></th>
            <th width="125px"><%out.print(LanguageDAO.i18n("loged.users.last.access"));%></th>
              <th width="25px"></th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="se" items="${sessions}" varStatus="row">
              <c:url value="ActivityLog" var="urlLog">
                <c:param name="user" value="${se.user}" />
              <c:param name="dbegin" value="${PersianDate}" />
              <c:param name="dend" value="${PersianDate}" />
              </c:url>
              <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                <td width="20px">${row.index + 1}</td>
                <td width="100px">${se.user}</td>
                <td width="225px">${se.id}</td>
                <td>${se.ip}</td>
                <td>${se.host}</td>
              <td width="125px"><u:formatDate calendar="${se.creation}" persian="true" /></td>
              <td width="125px"><u:formatDate calendar="${se.lastAccess}" persian="true" /></td>
                <td width="25px" align="center">
                  <a href="${urlLog}"><img src="img/action/calendar.png" alt="Activity log" title="Activity log" /></a>
                </td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
