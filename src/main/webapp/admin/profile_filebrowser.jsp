<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.file.browser"));%></legend>
  <table>
    <tr>
      <td>اندازه</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_status_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.statusWidth}"/>
      </td>
    <td><%out.print(LanguageDAO.i18n("profile.file.browser.state"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfFileBrowser.statusVisible}">
            <input name="prf_filebrowser_status_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_filebrowser_status_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_massive_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.massiveWidth}"/>
      </td>
    <td><%out.print(LanguageDAO.i18n("profile.file.browser.massive"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfFileBrowser.massiveVisible}">
            <input name="prf_filebrowser_massive_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_filebrowser_massive_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_icon_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.iconWidth}"/>
      </td>
    <td><%out.print(LanguageDAO.i18n("profile.file.browser.icon"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfFileBrowser.iconVisible}">
            <input name="prf_filebrowser_icon_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_filebrowser_icon_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_name_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.nameWidth}"/>
      </td>
    <td><%out.print(LanguageDAO.i18n("profile.file.browser.name"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfFileBrowser.nameVisible}">
            <input name="prf_filebrowser_name_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_filebrowser_name_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td>
        <div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_size_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.sizeWidth}"/>
      </td>
    <td><%out.print(LanguageDAO.i18n("profile.file.browser.size"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfFileBrowser.sizeVisible}">
            <input name="prf_filebrowser_size_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_filebrowser_size_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_lastmod_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.lastModifiedWidth}"/>
      </td>
    <td><%out.print(LanguageDAO.i18n("profile.file.browser.last.modified"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfFileBrowser.lastModifiedVisible}">
            <input name="prf_filebrowser_lastmod_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_filebrowser_lastmod_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_author_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.authorWidth}"/>
      </td>
    <td><%out.print(LanguageDAO.i18n("profile.file.browser.author"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfFileBrowser.authorVisible}">
            <input name="prf_filebrowser_author_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_filebrowser_author_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_version_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.versionWidth}"/>
      </td>
    <td><%out.print(LanguageDAO.i18n("profile.file.browser.version"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfFileBrowser.versionVisible}">
            <input name="prf_filebrowser_version_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_filebrowser_version_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>

    <!-- Additional columns -->
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 1</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column0_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column0Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column0" id="prf_filebrowser_column0" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column0 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 2</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column1_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column1Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column1" id="prf_filebrowser_column1" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column1 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 3</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column2_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column2Width}"/>
      </td>
      <td colspan="2">
      <select name="prf_filebrowser_column2" id="prf_filebrowser_column2" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column2 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 4</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column3_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column3Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column3" id="prf_filebrowser_column3" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column3 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 5</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column4_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column4Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column4" id="prf_filebrowser_column4" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column4 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 6</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column5_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column5Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column5" id="prf_filebrowser_column5" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column5 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 7</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column6_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column6Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column6" id="prf_filebrowser_column6" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column6 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 8</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column7_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column7Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column7" id="prf_filebrowser_column7" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column7 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 9</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column8_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column8Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column8" id="prf_filebrowser_column8" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column8 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
    <td colspan="3"><%out.print(LanguageDAO.i18n("profile.file.browser.column"));%> 10</td>
    </tr>
    <tr>
      <td>
      	<div id="error_integer" style="display:none; color : red;">مقدار فیلدهای 'اندازه' باید عددی باشد.</div>
        <input class=":required;; :integer;;error_integer :only_on_blur" name="prf_filebrowser_column9_width" size="3" maxlength="3" type="text" value="${prf.prfFileBrowser.column9Width}"/>
      </td>
      <td colspan="2">
        <select name="prf_filebrowser_column9" id="prf_filebrowser_column9" data-placeholder="انتخاب فیلد فراداده" style="width: 200px">
          <option/>
          <c:forEach var="prop" items="${pgprops}">
            <c:choose>
              <c:when test="${prf.prfFileBrowser.column9 == prop}">
                <option value="${prop}" selected="selected">${prop}</option>
              </c:when>
              <c:otherwise>
                <option value="${prop}">${prop}</option>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </select>
      </td>
    </tr>
  </table>
</fieldset>
