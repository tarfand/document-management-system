<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <link rel="Shortcut icon" href="favicon.ico"/>
  <link rel="stylesheet" type="text/css" href="../css/dataTables-1.10.10/jquery.dataTables-1.10.10.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/admin-style.css"/>
  <script type="text/javascript" src="../js/utils.js"></script>
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables-1.10.10.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#results').dataTable({
        "bStateSave": true,
        "iDisplayLength": 15,
        "lengthMenu": [[10, 15, 20], [10, 15, 20]],
        "fnDrawCallback": function (oSettings) {
          dataTableAddRows(this, oSettings);
        }
      });
    });
  </script>
  <title>Crontab</title>
</head>
<body>
<c:set var="isAdmin"><%=BaseServlet.isMultipleInstancesAdmin(request)%>
</c:set>
<c:choose>
  <c:when test="${isAdmin}">
    <ul id="breadcrumb">
      <li class="path"><a href="CronTab"><%out.print(LanguageDAO.i18n("crontab"));%></a></li>
      <li class="action">
        <a href="CronTab">
          <img src="img/action/refresh.png" alt="Refresh" title=" <%out.print(LanguageDAO.i18n("crontab.refresh"));%>" style="vertical-align: middle;" />  <%out.print(LanguageDAO.i18n("crontab.refresh"));%>
        </a>
      </li>
    </ul>
    <br/>
    <div style="width: 90%; margin-left: auto; margin-right: auto;">
      <table id="results" class="results">
        <thead>
        <tr>
          <th><%out.print(LanguageDAO.i18n("crontab.name"));%></th>
          <th><%out.print(LanguageDAO.i18n("crontab.circle"));%></th>
          <th><%out.print(LanguageDAO.i18n("crontab.mime"));%></th>
          <th><%out.print(LanguageDAO.i18n("crontab.filename"));%></th>
          <th><%out.print(LanguageDAO.i18n("crontab.email"));%></th>
          <th><%out.print(LanguageDAO.i18n("crontab.last.start"));%></th>
          <th><%out.print(LanguageDAO.i18n("crontab.last.end"));%></th>
          <th width="50px"><%out.print(LanguageDAO.i18n("crontab.active"));%></th>
          <th width="100px">
            <c:url value="CronTab" var="urlCreate">
              <c:param name="action" value="create"/>
            </c:url>
            <a href="${urlCreate}"><img src="img/action/new.png" alt="New crontab" title="<%out.print(LanguageDAO.i18n("crontab.new"));%>" /></a>
          </th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="ct" items="${crontabs}" varStatus="row">
          <c:url value="CronTab" var="urlEdit">
            <c:param name="action" value="edit"/>
            <c:param name="ct_id" value="${ct.id}"/>
          </c:url>
          <c:url value="CronTab" var="urlDelete">
            <c:param name="action" value="delete"/>
            <c:param name="ct_id" value="${ct.id}"/>
          </c:url>
          <c:url value="CronTab" var="urlExecute">
            <c:param name="action" value="execute"/>
            <c:param name="ct_id" value="${ct.id}"/>
          </c:url>
          <c:url value="CronTab" var="urlDownload">
            <c:param name="action" value="download"/>
            <c:param name="ct_id" value="${ct.id}"/>
          </c:url>
          <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
            <td>${ct.name}</td>
            <td>${ct.expression}</td>
            <td>${ct.fileMime}</td>
            <td>${ct.fileName}</td>
            <td>${ct.mail}</td>
            <td><u:formatDate calendar="${ct.lastBegin}" persian="true" /></td>
            <td><u:formatDate calendar="${ct.lastEnd}" persian="true" /></td>
            <td width="50px" align="center">
              <c:choose>
                <c:when test="${ct.active}">
                  <img src="img/true.png" alt="Active" title="<%out.print(LanguageDAO.i18n("crontab.active"));%>"/>
                </c:when>
                <c:otherwise>
                  <img src="img/false.png" alt="Inactive" title="<%out.print(LanguageDAO.i18n("crontab.inactive"));%>"/>
                </c:otherwise>
              </c:choose>
            </td>
            <td width="100px" align="center">
              <a href="${urlEdit}"><img src="img/action/edit.png" alt="Edit" title="<%out.print(LanguageDAO.i18n("crontab.edit"));%>" /></a> &nbsp;
              <a href="${urlDelete}"><img src="img/action/delete.png" alt="Delete" title="<%out.print(LanguageDAO.i18n("crontab.delete"));%>" /></a> &nbsp;
              <c:choose>
                <c:when test="${(empty ct.lastBegin && empty ct.lastEnd) || not empty ct.lastEnd}">
                  <a href="${urlExecute}"><img src="img/action/signal.png" alt="Execute" title="<%out.print(LanguageDAO.i18n("crontab.execute"));%>" /></a>
                </c:when>
                <c:otherwise>
                  <img src="img/action/signal_disabled.png" alt="Running" title="<%out.print(LanguageDAO.i18n("crontab.running"));%>" />
                </c:otherwise>
              </c:choose>
              &nbsp; <a href="${urlDownload}"><img src="img/action/download.png" alt="Download" title="<%out.print(LanguageDAO.i18n("crontab.download"));%>" /></a>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </c:when>
  <c:otherwise>
    <div class="error">
      <h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3>
    </div>
  </c:otherwise>
</c:choose>
</body>
</html>
