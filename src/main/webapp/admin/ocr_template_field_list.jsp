<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>

<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <link rel="Shortcut icon" href="favicon.ico"/>
  <link rel="stylesheet" type="text/css" href="css/style.css?v=1506588453493"/>
  <link rel="stylesheet" type="text/css" href="../css/fixedTableHeader.css?v=1506588453493"/>
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script type="text/javascript" src="../js/fixedTableHeader.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#scroll').height($(window).height() - 21);
      $('#image').hide();
    });

    jQuery(window).load(function () {
      scale = 1;
      height = $('#image').height();
      width = $('#image').width();

      if (height > 200) {
        scale = 200 / height;
        height = 200;
      }
      width = Math.floor(width * scale);
      if (width > 120) {
        scale = 120 / width;
        width = 120;
        height = Math.floor(height * scale);
      }

      $('#image').hide();
      $('#image').width(width);
      $('#image').height(height);

      $('#show_hide').click(function () {
        if ($('#image').is(":visible")) {
          $('#image').hide();
        } else {
          $('#image').show();
        }
      });

      TABLE.fixHeader('table');
    });
  </script>
  <title><%out.print(LanguageDAO.i18n("ocr.template.title"));%></title>
</head>
<body dir="rtl">
<c:choose>
  <c:when test="${u:isAdmin()}">
    <ul id="breadcrumb">
      <li class="path">
        <a href="OCRTemplate"><%out.print(LanguageDAO.i18n("ocr.template.title"));%></a>
      </li>
      <li class="path">
        <%out.print(LanguageDAO.i18n("ocr.template.definition"));%>
      </li>
    </ul>
    <div id="scroll" style="width: 100%; height: 100%; overflow: auto;">
      <br/>
      <c:url value="OCRTemplate" var="urlImage">
        <c:param name="action" value="image"/>
        <c:param name="ot_id" value="${ot.id}"/>
      </c:url>
        <table dir="rtl" cellpadding="5px" class="results-old" width="70%">
        <thead>
        <tr class="fuzzy">
          <td colspan="5" align="right">
            <table id="info" style="white-space: nowrap;">
              <tr>
                <td>
                  <a id="show_hide" href="#"><img title="Show / Hide" alt="Show / Hide"
                                                  src="img/action/examine.png"/></a>
                </td>
                <td valign="top">
                  <b><%out.print(LanguageDAO.i18n("ocr.template"));%></b> ${ot.name}
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center">
                  <img id="image" src="${urlImage}"/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <th><%out.print(LanguageDAO.i18n("ocr.template.name"));%></th>
          <th><%out.print(LanguageDAO.i18n("ocr.template.class"));%></th>
          <th><%out.print(LanguageDAO.i18n("ocr.template.type"));%></th>
          <th><%out.print(LanguageDAO.i18n("ocr.template.data"));%></th>
          <th width="80px">
            <c:url value="OCRTemplate" var="urlCreateField">
              <c:param name="action" value="createField"/>
              <c:param name="ot_id" value="${ot.id}"/>
            </c:url>
            <c:url value="OCRTemplate" var="urlCreateControlField">
              <c:param name="action" value="createControlField"/>
              <c:param name="ot_id" value="${ot.id}"/>
            </c:url>
            <c:url value="OCRTemplate" var="urlCheck">
              <c:param name="action" value="check"/>
              <c:param name="ot_id" value="${ot.id}"/>
            </c:url>
                <a href="${urlCreateField}"><img src="img/action/new.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.new.field"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.new.field"));%>"/></a>
            &nbsp;
                <a href="${urlCreateControlField}"><img src="img/action/new_template_field.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.new.control.field"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.new.control.field"));%>"/></a>
            &nbsp;
                <a href="${urlCheck}"><img title="<%out.print(LanguageDAO.i18n("ocr.template.check"));%>" alt="<%out.print(LanguageDAO.i18n("ocr.template.check"));%>" src="img/action/check.png"/></a>
          </th>
        </tr>
        </thead>
        <tbody>
        <c:set var="row" value="1" scope="page"/>
        <c:forEach var="otf" items="${ot.fields}">
          <c:url value="OCRTemplate" var="urlEdit">
            <c:param name="action" value="editField"/>
            <c:param name="ot_id" value="${ot.id}"/>
            <c:param name="otf_id" value="${otf.id}"/>
          </c:url>
          <c:url value="OCRTemplate" var="urlDelete">
            <c:param name="action" value="deleteField"/>
            <c:param name="ot_id" value="${ot.id}"/>
            <c:param name="otf_id" value="${otf.id}"/>
          </c:url>
          <c:set var="row" value="${row + 1}" scope="page"/>
          <tr class="${row % 2 == 0 ? 'even' : 'odd'}">
            <td valign="top">${otf.name}</td>
            <td valign="top">
              <c:forEach var="parser" items="${parsers}">
                <c:choose>
                  <c:when test="${otf.className == parser.getClass().getName()}">
                    ${parser.name}
                  </c:when>
                </c:choose>
              </c:forEach>
            </td>
            <td valign="top">Data Capture <c:if test="${otf.control}">& Validation Control </c:if>field</td>
            <td>
              <table border="0" cellpadding="2" cellspacing="0">
                <!--
                    <tr>
                      <td valign="top">Coordinates</td>
                      <td valign="top">:</td>
                      <td>x1:${otf.x1} y1:${otf.y1} x2:${otf.x2} y2:${otf.y2}</td>
                    </tr>
                    -->
                <tr>
                  <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.propertyGroup"));%></td>
                  <td valign="top">:</td>
                  <td>${otf.propertyGroupName}</td>
                </tr>
                <c:if test="${not empty otf.pattern}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.pattern"));%></td>
                    <td valign="top">:</td>
                    <td>${otf.pattern}</td>
                  </tr>
                </c:if>
                <c:if test="${not empty otf.wordListUUID}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.wordListUII"));%></td>
                    <td valign="top">:</td>
                    <td>${otf.wordListUUID}</td>
                  </tr>
                </c:if>
                <c:if test="${not empty otf.filterPattern}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.filterPattern"));%></td>
                    <td valign="top">:</td>
                    <td>${otf.filterPattern}</td>
                  </tr>
                </c:if>
                <c:if test="${otf.rotation != 0}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.rotation"));%></td>
                    <td valign="top">:</td>
                    <td>${otf.rotation}</td>
                  </tr>
                </c:if>
                <c:if test="${not empty otf.custom}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.converterPath"));%></td>
                    <td valign="top">:</td>
                    <td>
                      <c:set var="newline" value="<%= \"\n\" %>"/>
                        ${fn:replace(otf.custom, newline, "<br/>")}
                    </td>
                  </tr>
                </c:if>
                <c:if test="${not empty otf.ocr}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.ocrPath"));%></td>
                    <td valign="top">:</td>
                    <td>${otf.ocr}</td>
                  </tr>
                </c:if>
              </table>
            </td>
            <td align="center" valign="top">
              <a href="${urlEdit}"><img src="img/action/edit.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.edit"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.edit"));%>"/></a>
              &nbsp;
              <a href="${urlDelete}"><img src="img/action/delete.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>"/></a>
            </td>
          </tr>
        </c:forEach>
        <c:forEach var="otcf" items="${ot.controlFields}">
          <c:url value="OCRTemplate" var="urlControlEdit">
            <c:param name="action" value="editControlField"/>
            <c:param name="ot_id" value="${ot.id}"/>
            <c:param name="otcf_id" value="${otcf.id}"/>
          </c:url>
          <c:url value="OCRTemplate" var="urlControlDelete">
            <c:param name="action" value="deleteControlField"/>
            <c:param name="ot_id" value="${ot.id}"/>
            <c:param name="otcf_id" value="${otcf.id}"/>
          </c:url>
          <c:set var="row" value="${row + 1}" scope="page"/>
          <tr class="${row % 2 == 0 ? 'even' : 'odd'}">
            <td valign="top">${otcf.name}</td>
            <td valign="top">
              <c:forEach var="controlParser" items="${controlParsers}">
                <c:choose>
                  <c:when test="${otcf.className == controlParser.getClass().getName()}">
                    ${controlParser.name}
                  </c:when>
                </c:choose>
              </c:forEach>
            </td>
            <td valign="top">Validation Control field</td>
            <td>
              <table border="0" cellpadding="2" cellspacing="0">
                <!--
                    <tr>
                      <td valign="top">Coordinates</td>
                      <td valign="top">:</td>
                      <td>x1:${otcf.x1} y1:${otcf.y1} x2:${otcf.x2} y2:${otcf.y2}</td>
                    </tr>
                    -->
                <tr>
                  <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.order"));%></td>
                  <td valign="top">:</td>
                  <td>${otcf.order}</td>
                </tr>
                <c:if test="${not empty otcf.value}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.value"));%></td>
                    <td valign="top">:</td>
                    <td>${otcf.value}</td>
                  </tr>
                </c:if>
                <c:if test="${not empty otcf.pattern}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.pattern"));%></td>
                    <td valign="top">:</td>
                    <td>${otcf.pattern}</td>
                  </tr>
                </c:if>
                <c:if test="${otcf.rotation != 0}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.rotation"));%></td>
                    <td valign="top">:</td>
                    <td>${otcf.rotation}</td>
                  </tr>
                </c:if>
                <c:if test="${not empty otcf.custom}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.converterPath"));%></td>
                    <td valign="top">:</td>
                    <td>
                      <c:set var="newline" value="<%= \"\n\" %>"/>
                        ${fn:replace(otcf.custom, newline, "<br/>")}
                    </td>
                  </tr>
                </c:if>
                <c:if test="${not empty otcf.ocr}">
                  <tr>
                    <td valign="top"><%out.print(LanguageDAO.i18n("ocr.template.ocrPath"));%></td>
                    <td valign="top">:</td>
                    <td>${otcf.ocr}</td>
                  </tr>
                </c:if>
              </table>
            </td>
            <td align="center" valign="top">
              <a href="${urlControlEdit}"><img src="img/action/edit.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.edit"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.edit"));%>"/></a>
              &nbsp;
              <a href="${urlControlDelete}"><img src="img/action/delete.png" alt="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>" title="<%out.print(LanguageDAO.i18n("ocr.template.delete"));%>"/></a>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </c:when>
  <c:otherwise>
    <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
  </c:otherwise>
</c:choose>
</body>
</html>
