<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" href="css/admin-style.css" type="text/css" />
  <title><%out.print(LanguageDAO.i18n("check.email"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isMultipleInstancesAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
       <li class="path">
          <a href="utilities.jsp"><%out.print(LanguageDAO.i18n("utilities"));%></a>
        </li>
        <li class="path"><%out.print(LanguageDAO.i18n("check.email"));%></li>
      </ul>
      <br/>
      <form action="CheckEmail">
        <input type="hidden" name="action" value="send"/>
        <table class="form" width="250px" style="direction: rtl;">
          <tr><td><%out.print(LanguageDAO.i18n("check.email.from"));%></td><td><input type="text" name="from" size="35" value="${from}"/></td></tr>
          <tr><td><%out.print(LanguageDAO.i18n("check.email.to"));%></td><td><input type="text" name="to" size="35" value="${to}"/></td></tr>
          <tr><td><%out.print(LanguageDAO.i18n("check.email.subject"));%></td><td><input type="text" name="subject" size="50" value="${subject}"/></td></tr>
          <tr><td colspan="2"><%out.print(LanguageDAO.i18n("automation.context"));%></td></tr>
          <tr><td colspan="2"><textarea name="content" cols="60" rows="7">${content}</textarea></td></tr>
          <tr>
            <td colspan="2" align="right">
              <input type="button" onclick="javascript:window.history.back()" value="انصراف" class="noButton"/>
              <input type="submit" value="<%out.print(LanguageDAO.i18n("check.email.send"));%>" class="yesButton"/>
            </td>
          </tr>
        </table>
      </form>
      <br/>
      <c:choose>
        <c:when test="${not empty error}">
          <div class="warn" style="text-align: center;">${error}</div>
        </c:when>
        <c:when test="${not empty success}">
          <div class="ok" style="text-align: center;">${success}</div>
        </c:when>
      </c:choose>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
