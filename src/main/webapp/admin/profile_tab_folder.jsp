<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.tabs.folders"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.folders.properties"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfFolder.propertiesVisible}">
            <input name="prf_tab_folder_properties_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_folder_properties_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.folders.security"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfFolder.securityVisible}">
            <input name="prf_tab_folder_security_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_folder_security_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.folders.notes"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfFolder.notesVisible}">
            <input name="prf_tab_folder_notes_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_folder_notes_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
