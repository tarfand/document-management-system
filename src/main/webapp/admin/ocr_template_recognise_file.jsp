<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/style.css?v=1506588453493" />
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script src="../js/vanadium-min.js" type="text/javascript"></script>
  </head>
<body dir="rtl">
  <u:constantsMap className="com.openkm.ocr.template.bean.OCRConfidenceLevel" var="OCRConfidenceLevel"/>
  <c:choose>
    <c:when test="${u:isAdmin()}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="OCRTemplate"><%out.print(LanguageDAO.i18n("ocr.template.title"));%></a>
        </li>
        <li class="path">
          <%out.print(LanguageDAO.i18n("ocr.template.recognise"));%>
        </li>
      </ul>
      <br/>
      <form action="OCRTemplate" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="${action}"/>
        <table class="form" width="425px">
          <tr>
            <td colspan="2"><%out.print(LanguageDAO.i18n("ocr.template.recognise.uploadHint"));%></td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("ocr.template.recognise.image"));%></td>
            <td><input class=":required :only_on_blur" type="file" name="image"/></td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("ocr.template.recognise.level"));%></td>
            <td>
            	<select name="level">
            	  <option value="${OCRConfidenceLevel.LEVEL_FAST}"><%out.print(LanguageDAO.i18n("ocr.template.recognise.fast"));%></option>
            	  <option value="${OCRConfidenceLevel.LEVEL_MEDIUM}"><%out.print(LanguageDAO.i18n("ocr.template.recognise.medium"));%></option>
            	  <option value="${OCRConfidenceLevel.LEVEL_SLOW}" selected="selected"><%out.print(LanguageDAO.i18n("ocr.template.recognise.slow"));%></option>
            	</select>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <input type="button" onclick="javascript:window.history.back()" value="<%out.print(LanguageDAO.i18n("ocr.template.cancel"));%>" class="noButton"/>
              <input type="submit" value="<%out.print(LanguageDAO.i18n("ocr.template.recognise"));%>" class="yesButton"/>
            </td>
          </tr>
        </table>
      </form>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
