<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <title>Purge Permissions</title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isMultipleInstancesAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
        <a href="utilities.jsp"><%out.print(LanguageDAO.i18n("utilities"));%></a>
        </li>
      <li class="path">مجوزهای حذف</li>
      </ul>
      <br/>
      <form action="PurgePermissions">
        <table class="form" align="center" width="150px">
          <tr><td>کاربران با سطح دسترسی حذف</td><td><input name="action" value="listUsers" type="radio"/></td></tr>
          <tr><td>گروه های با سطح دسترسی حذف</td><td><input name="action" value="listRoles" type="radio"/></td></tr>
          <tr>
            <td colspan="2" align="right">
              <input type="button" onclick="javascript:window.history.back()" value="انصراف" class="noButton"/>
              <input type="submit" value="مشاهده" class="yesButton"/>
            </td>
          </tr>
        </table>
      </form>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
