<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.openkm.core.Config" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/vanadium-min.js" ></script>
  <title><%out.print(LanguageDAO.i18n("omr.template"));%></title>
</head>
<body>
<c:set var="isAdmin"><%=request.isUserInRole(Config.DEFAULT_ADMIN_ROLE)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
            <a href="Omr"><%out.print(LanguageDAO.i18n("omr.template"));%></a>
        </li>
        <li class="path">
          <c:choose>
            <c:when test="${action == 'editFields'}"><%out.print(LanguageDAO.i18n("omr.edit.field.file"));%></c:when>
          </c:choose>
        </li>
      </ul>
      <br/>
  	<form action="Omr" method="post" enctype="multipart/form-data">
  	<input type="hidden" name="action" value="${action}"/>
        <input type="hidden" name="om_id" value="${om.id}"/>
        <table class="form rtl-table" width="425px">

          <tr>
		  	<td valign="top"><%out.print(LanguageDAO.i18n("omr.fields"));%></td>
		  	<td valign="top">
		  		<c:if test="${om.fieldsFileName!=null && om.fieldsFileName ne ''}">
			  		<c:url value="Omr" var="urlDownload">
		          		<c:param name="action" value="downloadFile"/>
		          		<c:param name="om_id" value="${om.id}"/>
		        	</c:url>
		        	<a href="${urlDownload}&type=2">${om.fieldsFileName}</a><br/>
          		</c:if>
		  		<input class=":required :only_on_blur" type="file" name="file"/>
		  	</td>
		  </tr>
          <tr>
            <td colspan="2" align="right">
              <div id="buttons">
              	<input type="submit" value="<%out.print(LanguageDAO.i18n("omr.send"));%>" class="yesButton"/>
                  <input type="button" onclick="javascript:window.history.back()" value="<%out.print(LanguageDAO.i18n("omr.cancel"));%>" class="noButton"/>
              </div>
            </td>
          </tr>
        </table>
      </form>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
