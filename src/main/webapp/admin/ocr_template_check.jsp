<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/style.css?v=1506588453493" />
  <link rel="stylesheet" type="text/css" href="css/imgareaselect/imgareaselect-default.css" />
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script src="../js/vanadium-min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/jquery.imgareaselect.pack.js"></script>
  <script type="text/javascript">
    // Keep in sync with ocr_template_field_edit.jsp
    var maxWidth = 800;
    var maxHeight = 933;
    var imageScaleApplied = 1;
    var fields = new Array('${ot.fields.size()}');
    var controlFields = new Array('${ot.controlFields.size()}');

    function resizeCroped(id){
      $(id).width(Math.floor(2 * ($(id).width() * imageScaleApplied))); // is enought with resize width
    }

    function showSquare(oft_x1, oft_y1, oft_x2, oft_y2) {
      // Resize to image scale
      oft_x1 = Math.floor(oft_x1 * imageScaleApplied);
      oft_y1 = Math.floor(oft_y1 * imageScaleApplied);
      oft_x2 = Math.floor(oft_x2 * imageScaleApplied);
      oft_y2 = Math.floor(oft_y2 * imageScaleApplied);
      $('#ocr_template').imgAreaSelect({movable:false, resizable:false, x1: oft_x1, y1: oft_y1, x2: oft_x2, y2: oft_y2 });
      $('#ocr_template').imgAreaSelect({
        onSelectStart: function (img, selection) {
          $('#ocr_template').imgAreaSelect({remove:true });
        }
      });
    }

    $(document).ready(function() {
      $('#scroll').height($(window).height() - 21);
      $('#ocr_template').hide();
    });

    jQuery(window).load(function () {
      $('#loading').hide();
      // Init real width and height
      imageWidth = $('#ocr_template').width();
      imageHeight = $('#ocr_template').height();
      realImageWidth = imageWidth; // Used to calculate image scale apply

      // Resize image
      if (imageWidth > maxWidth) {
        imageHeight = Math.floor(imageHeight * (maxWidth / imageWidth));
        imageWidth = maxWidth;
      }

      if (imageHeight > maxHeight) {
        imageWidth = Math.floor(imageWidth * (maxHeight / imageHeight));
        imageHeight = maxHeight;
      }

      imageScaleApplied = imageWidth / realImageWidth; // always will be 1 or decimal value

      // scale croped images
      for (var i=0;i < fields.length; i++) {
        resizeCroped(fields[i]);
        $(fields[i]).show();
      }

      $('#show_hide').click(function() {
        if ($('#ocr_template').is(":visible")) {
          $('#ocr_template').hide();
        } else {
          $('#ocr_template').show();
        }
      });

      $('#ocr_template').width(imageWidth);
      $('#ocr_template').height(imageHeight);
      $('#ocr_template').show();
    });
  </script>
  <title>OCR template check</title>
</head>
<body>
  <c:choose>
    <c:when test="${u:isAdmin()}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="OCRTemplate">OCR Template</a>
        </li>
        <li class="path">
          <a href="OCRTemplate?action=fieldsList&ot_id=${ot.id}">OCR Template definition</a>
        </li>
        <li class="path">
          Check
        </li>
      </ul>
      <div id="scroll" style="width: 100%; height: 100%; overflow: auto;">
        <br/>
        <c:url value="OCRTemplate" var="urlImage">
          <c:param name="action" value="image"/>
        <c:param name="ot_id" value="${ot.id}"/>
        </c:url>
        <form action="OCRTemplate" method="post">
          <input type="hidden" name="action" value="${action}"/>
          <input type="hidden" name="ot_id" value="${ot.id}"/>
          <table width="100%" align="center" style="white-space: nowrap;">
            <tr>
              <td width="5"></td>
              <td valign="top" align="right">
                <div id="loading">Please wait while image is loading</div>
                <img id="ocr_template" src="${urlImage}" alt="Image template" />
              </td>
             <td width="5"></td>
             <td valign="top" width="100%">
               <table class="results-old" align="left" width="100%">
                 <tr class="fuzzy">
                  <td colspan="6" align="left">
                    <table id="info" style="white-space: nowrap;">
                      <tr>
                        <td valign="top">
                          <b>Template</b> ${ot.name}
                        </td>
                        <td>
                          <a id="show_hide" href="#"><img title="Show / Hide" alt="Show / Hide" src="img/action/examine.png"/></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                 <tr>
                   <th>Field</th>
                   <th>Class</th>
                   <th>Type</th>
                   <th>Zone</th>
                   <th>Data</th>
                 </tr>
                 <c:forEach var="field" items="${ot.fields}" varStatus="row">
                   <c:url value="OCRTemplate" var="urlCropImage">
                     <c:param name="action" value="downloadCroppedImage"/>
                     <c:param name="ot_id" value="${ot.id}"/>
                     <c:param name="otf_id" value="${field.id}"/>
                   </c:url>
                   <script type="text/javascript">
                     fields['${row.index}']='#croped_${field.id}';
                     $('#croped_${field.id}').hide();
                   </script>
                   <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                     <td>${field.name}</td>
                     <td>
                       <c:forEach var="parser" items="${parsers}" varStatus="row">
                         <c:choose>
                           <c:when test="${field.className == parser.getClass().getName()}">
                             ${parser.name}
                           </c:when>
                         </c:choose>
                       </c:forEach>
                     </td>
                     <td>Data Capture field</td>
                     <td valign="baseline">
                       <img style="cursor: pointer; cursor: hand;" onclick="javascript:showSquare(${field.x1},${field.y1},${field.x2},${field.y2});" id="croped_${field.id}" src="${urlCropImage}" alt="Cropped image" />
                     </td>
                     <td valign="top">
                      <table border="0" cellpadding="2" cellspacing="0">
                      <tr>
                        <td valign="top">Coordinates</td>
                        <td valign="top">:</td>
                        <td>x1:${field.x1} y1:${field.y1} x2:${field.x2} y2:${field.y2}</td>
                      </tr>
                      <tr>
                        <td valign="top">Property Group</td>
                        <td valign="top">:</td>
                        <td>${field.propertyGroupName}</td>
                      </tr>
                      <c:if test="${not empty field.pattern}">
                        <tr>
                          <td valign="top">Pattern</td>
                          <td valign="top">:</td>
                          <td>${field.pattern}</td>
                        </tr>
                      </c:if>
                      <c:if test="${field.rotation != 0}">
                        <tr>
                          <td valign="top">Rotation</td>
                          <td valign="top">:</td>
                          <td>${field.rotation}</td>
                        </tr>
                      </c:if>
                      <c:if test="${not empty field.custom}">
                        <tr>
                          <td valign="top">Custom</td>
                          <td valign="top">:</td>
                          <td>
                            <c:set var="newline" value="<%= \"\n\" %>" />
                            ${fn:replace(field.custom, newline, "<br/>")}
                          </td>
                        </tr>
                      </c:if>
                      <c:if test="${not empty field.ocr}">
                        <tr>
                          <td valign="top">OCR</td>
                          <td valign="top">:</td>
                          <td>${field.ocr}</td>
                        </tr>
                      </c:if>
                      <tr>
                        <td valign="top"><b>Result</b></td>
                        <td valign="top">:</td>
                        <td>
                         <c:forEach var="result" items="${resultsMap}">
                           <c:if test="${result.key == field.id}">
                             <b>${result.value}</b>
                           </c:if>
                         </c:forEach>
                       </td>
                      </tr>
                      </table>
                    </td>
                   </tr>
                 </c:forEach>

                 <c:forEach var="controlField" items="${ot.controlFields}" varStatus="row">
                   <c:url value="OCRTemplate" var="urlControlCropImage">
                     <c:param name="action" value="downloadCroppedImage"/>
                     <c:param name="ot_id" value="${ot.id}"/>
                     <c:param name="otcf_id" value="${controlField.id}"/>
                   </c:url>
                   <script type="text/javascript">
                     controlFields['${row.index}']='#croped_control_${field.id}';
                     $('#croped_control_${controlFields.id}').hide();
                   </script>
                   <tr class="${ot.fields.size()+row.index % 2 == 0 ? 'even' : 'odd'}">
                     <td>${controlField.name}</td>
                     <td>
                       <c:forEach var="controlParser" items="${controlParsers}" varStatus="row">
                         <c:choose>
                           <c:when test="${controlField.className == controlParser.getClass().getName()}">
                             ${controlParser.name}
                           </c:when>
                         </c:choose>
                       </c:forEach>
                     </td>
                     <td>Validation Control field</td>
                     <td valign="baseline">
                       <img style="cursor: pointer; cursor: hand;" onclick="javascript:showSquare(${controlField.x1},${controlField.y1},${controlField.x2},${controlField.y2});" id="croped_control_${controlField.id}" src="${urlControlCropImage}" alt="Cropped image" />
                     </td>
                     <td valign="top">
                      <table border="0" cellpadding="2" cellspacing="0">
                      <tr>
                        <td valign="top">Coordinates</td>
                        <td valign="top">:</td>
                        <td>x1:${controlField.x1} y1:${controlField.y1} x2:${controlField.x2} y2:${controlField.y2}</td>
                      </tr>
                      <tr>
                        <td valign="top">Order</td>
                        <td valign="top">:</td>
                        <td>${controlField.order}</td>
                      </tr>
                      <c:if test="${not empty controlField.value}">
                        <tr>
                          <td valign="top">Value</td>
                          <td valign="top">:</td>
                          <td>${controlField.value}</td>
                        </tr>
                      </c:if>
                      <c:if test="${not empty controlField.pattern}">
                        <tr>
                          <td valign="top">Pattern</td>
                          <td valign="top">:</td>
                          <td>${controlField.pattern}</td>
                        </tr>
                      </c:if>
                      <c:if test="${controlField.rotation != 0}">
                        <tr>
                          <td valign="top">Rotation</td>
                          <td valign="top">:</td>
                          <td>${controlField.rotation}</td>
                        </tr>
                      </c:if>
                      <c:if test="${not empty controlField.custom}">
                        <tr>
                          <td valign="top">Custom</td>
                          <td valign="top">:</td>
                          <td>
                            <c:set var="newline" value="<%= \"\n\" %>" />
                            ${fn:replace(controlField.custom, newline, "<br/>")}
                          </td>
                      </tr>
                      </c:if>
                      <c:if test="${not empty controlField.ocr}">
                        <tr>
                          <td valign="top">OCR</td>
                          <td valign="top">:</td>
                          <td>${controlField.ocr}</td>
                        </tr>
                      </c:if>
                      <tr>
                         <td valign="top"><b>Result</b></td>
                         <td valign="top">:</td>
                         <td>
                           <c:forEach var="controlResult" items="${controlResultsMap}">
                             <c:if test="${controlResult.key == controlField.id}">
                               <b>${controlResult.value}</b>
                             </c:if>
                           </c:forEach>
                         </td>
                       </tr>
                      </table>
                    </td>
                   </tr>
                 </c:forEach>
               </table>
             </td>
            </tr>
          </table>
        </form>
        <br/>
      </div>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
