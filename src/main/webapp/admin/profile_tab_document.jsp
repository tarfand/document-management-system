<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.tabs.documents"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.documents.properties"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfDocument.propertiesVisible}">
            <input name="prf_tab_document_properties_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_document_properties_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.documents.security"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfDocument.securityVisible}">
            <input name="prf_tab_document_security_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_document_security_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.documents.notes"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfDocument.notesVisible}">
            <input name="prf_tab_document_notes_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_document_notes_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.documents.versions"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfDocument.versionsVisible}">
            <input name="prf_tab_document_versions_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_document_versions_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.documents.versions.download"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfDocument.versionDownloadVisible}">
            <input name="prf_tab_document_version_download_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_document_version_download_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.documents.preview"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfDocument.previewVisible}">
            <input name="prf_tab_document_preview_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_document_preview_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.documents.property.groups"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.prfDocument.propertyGroupsVisible}">
            <input name="prf_tab_document_property_groups_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_document_property_groups_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
