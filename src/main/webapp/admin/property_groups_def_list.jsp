<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="../css/dataTables-1.10.10/jquery.dataTables-1.10.10.min.css" />
    <link rel="stylesheet" type="text/css" href="css/admin-style.css?v=21" />
  <script type="text/javascript" src="../js/utils.js"></script>
  <script type="text/javascript" src="../js/jquery-3.6.0.min.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables-1.10.10.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
      $('#results').dataTable({
              "bStateSave" : true,
              "iDisplayLength" : 10,
              "lengthMenu" : [ [ 10, 15, 20 ], [ 10, 15, 20 ] ],
              "fnDrawCallback" : function(oSettings) {
          dataTableAddRows(this, oSettings);
        }
      });
    });
  </script>
    <title>profile.property.groups</title>
</head>
<body>
  <c:choose>
    <c:when test="${u:isAdmin()}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="PropertyGroups">profile.property.groups</a>
        </li>
      </ul>
      <br/>
        <div style="width: 40%; margin-left: auto; margin-right: auto;">
            <table id="results" class="results">
        <thead>
        <tr>
          <th>profile.property.groups.group.name</th>
          <th width="75px">
            <a href="PropertyGroups?action=create"><img src="img/action/new.png" alt="profile.property.groups.new" title="profile.property.groups.new"/></a>
            <a href="PropertyGroups?action=refresh"><img src="img/action/refresh.png" alt="profile.property.groups.refresh" title="profile.property.groups.refresh"/></a>
          </th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="pg" items="${pgList}" varStatus="row">
          <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
            <td>${pg.name}</td>
            <td align="center">
              <a href="PropertyGroups?action=view&pgdId=${pg.id}"><img src="img/action/examine.png" alt="profile.property.groups.examine" title="profile.property.groups.examine"/></a>
              <a href="PropertyGroups?action=edit&pgdId=${pg.id}"><img src="img/action/edit.png" alt="profile.property.groups.edit" title="profile.property.groups.edit"/></a>
              <a href="PropertyGroups?action=delete&pgdId=${pg.id}"><img src="img/action/delete.png" alt="profile.property.groups.delete" title="profile.property.groups.delete"/></a>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
     </div>
    </c:when>
    <c:otherwise>
        <div class="error"><h3>error.access.by.admins</h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
