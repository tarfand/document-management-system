<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.dashboard"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.dashboard.user"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfDashboard.userVisible}">
            <input name="prf_dashboard_user_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_dashboard_user_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.dashboard.email"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfDashboard.mailVisible}">
            <input name="prf_dashboard_mail_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_dashboard_mail_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.dashboard.news"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfDashboard.newsVisible}">
            <input name="prf_dashboard_news_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_dashboard_news_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.dashboard.general"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfDashboard.generalVisible}">
            <input name="prf_dashboard_general_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_dashboard_general_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.dashboard.workflow"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfDashboard.workflowVisible}">
            <input name="prf_dashboard_workflow_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_dashboard_workflow_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.dashboard.keywords"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfDashboard.keywordsVisible}">
            <input name="prf_dashboard_keywords_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_dashboard_keywords_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
