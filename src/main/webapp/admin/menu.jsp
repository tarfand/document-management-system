<%@ page import="com.openkm.extension.servlet.admin.DocumentExpirationServlet" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>

<c:set var="isMultipleInstancesAdmin"><%=BaseServlet.isMultipleInstancesAdmin(request)%>
</c:set>
<c:set var="isDocumentExpiration"><%=DocumentExpirationServlet.isDocumentExpiration()%>
</c:set>
<!-- http://stackoverflow.com/questions/1708054/center-ul-li-into-div -->
<div style="text-align: center">
  <ul style="display: inline-block;">
    <li>
      <a target="frame" href="home.jsp" title="<%out.print(LanguageDAO.i18n("management.home"));%>">
        <img src="img/toolbar/home.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.home"));%> </p>
    </li>
    <c:if test="${isMultipleInstancesAdmin}">
      <li>
        <a target="frame" href="Config" title="<%out.print(LanguageDAO.i18n("management.config"));%>">
          <img src="img/toolbar/config.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.config"));%> </p>
      </li>
    </c:if>
    <li>
      <a target="frame" href="MimeType" title="<%out.print(LanguageDAO.i18n("management.mime.type"));%>">
        <img src="img/toolbar/mime.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.mime.type"));%> </p>
    </li>
    <li>
      <a target="frame" href="stats.jsp" title="<%out.print(LanguageDAO.i18n("management.stats"));%>">
        <img src="img/toolbar/stats.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.stats"));%> </p>
    </li>
    <c:if test="${isMultipleInstancesAdmin}">
      <li>
        <a target="frame" href="Scripting" title="<%out.print(LanguageDAO.i18n("management.scripting"));%>">
          <img src="img/toolbar/scripting.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.scripting"));%> </p>
      </li>
    </c:if>
    <li>
      <a target="frame" href="PropertyGroups" title="<%out.print(LanguageDAO.i18n("management.metadata"));%>">
        <img src="img/toolbar/properties.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.metadata"));%> </p>
    </li>
    <li>
      <a target="frame" href="Auth" title="<%out.print(LanguageDAO.i18n("management.users"));%>">
        <img src="img/toolbar/users.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.users"));%> </p>
    </li>
    <li>
      <a target="frame" href="Profile" title="<%out.print(LanguageDAO.i18n("management.profile"));%>">
        <img src="img/toolbar/profile.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.profile"));%> </p>
    </li>
    <c:if test="${isMultipleInstancesAdmin}">
      <li>
        <a target="frame" href="DatabaseQuery" title="<%out.print(LanguageDAO.i18n("management.databaseQuery"));%>">
          <img src="img/toolbar/database.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;">  <%out.print(LanguageDAO.i18n("management.databaseQuery"));%> </p>
      </li>
    </c:if>
    <li>
      <a target="frame" href="Report" title="<%out.print(LanguageDAO.i18n("management.reports"));%>">
        <img src="img/toolbar/report.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;">  <%out.print(LanguageDAO.i18n("management.reports"));%> </p>
    </li>
    <li>
      <a target="frame" href="ActivityLog"
         title="<%out.print(LanguageDAO.i18n("management.activity.log"));%>">
        <img src="img/toolbar/activity.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.activity.log"));%> </p>
    </li>
    <li>
      <a target="frame" href="Automation" title="<%out.print(LanguageDAO.i18n("management.automation"));%>">
        <img src="img/toolbar/automation.png">
      </a>
      <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.automation"));%></p>
    </li>
    <c:if test="${isDocumentExpiration}">
      <li>
        <a target="frame" href="DocumentExpiration"
           title="<%out.print(LanguageDAO.i18n("management.document.expiration"));%>">
          <img src="img/toolbar/expiration.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.document.expiration"));%></p>
      </li>
    </c:if>
    <c:if test="${isMultipleInstancesAdmin}">
      <li>
        <a target="frame" href="CronTab" title="<%out.print(LanguageDAO.i18n("management.cron.tab"));%>">
          <img src="img/toolbar/crontab.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.cron.tab"));%></p>
      </li>
      <li>
        <a target="frame" href="Omr" title="<%out.print(LanguageDAO.i18n("management.omr"));%>">
          <img src="img/toolbar/omr.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.omr"));%></p>
      </li>
      <li>
        <a target="frame" href="generate_thesaurus.jsp" title="">
          <img src="img/toolbar/thesaurus.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.thesaurus"));%> </p>
      </li>
      <li>
        <a target="frame" href="Language" title="<%out.print(LanguageDAO.i18n("management.languages").trim());%>">
          <img src="img/toolbar/language.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"><%out.print(LanguageDAO.i18n("management.languages"));%></p>
      </li>
      <li>
        <a target="frame" href="OCRTemplate"
           title="<%out.print(LanguageDAO.i18n("management.ocr.templates"));%>">
          <img src="img/toolbar/scanner.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"><%out.print(LanguageDAO.i18n("management.ocr.templates").trim());%> </p>
      </li>
      <li>
        <a target="frame" href="Repository?action=import" title="<%out.print(LanguageDAO.i18n("management.repository.import").trim());%>">
          <img src="img/toolbar/import.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"> <%out.print(LanguageDAO.i18n("management.repository.import").trim());%> </p>
      </li>
      <li>
        <a target="frame" href="Repository?action=export" title="<%out.print(LanguageDAO.i18n("management.repository.export").trim());%>">
          <img src="img/toolbar/export.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"><%out.print(LanguageDAO.i18n("management.repository.export").trim());%> </p>
      </li>
      <li>
        <a target="frame" href="utilities.jsp" title="<%out.print(LanguageDAO.i18n("management.utilities").trim());%>">
          <img src="img/toolbar/utilities.png">
        </a>
        <p style="vertical-align: top; font-size: 8px; inline-size: 50px;overflow-wrap: break-word;"><%out.print(LanguageDAO.i18n("management.utilities").trim());%> </p>
      </li>
    </c:if>
    <script type="text/javascript">
      // Identify if being loaded inside an iframe
      if (self == top) {
        document.write('<li>\n');
        document.write('<a href="logout.jsp" title="Exit"><img src="img/toolbar/exit.png"></a>\n');
        document.write('</li>\n');
      }
    </script>
    <c:if test="${isMultipleInstancesAdmin}">
      <li>
        <a target="frame" href="experimental.jsp">&nbsp;</a>
      </li>
    </c:if>
  </ul>
</div>

