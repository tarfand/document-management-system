<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page import="com.openkm.core.Config"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u"%>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="Shortcut icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css" href="../css/dataTables-1.10.10/jquery.dataTables-1.10.10.min.css" />
<link rel="stylesheet" type="text/css" href="css/admin-style.css" />
<script type="text/javascript" src="../js/utils.js"></script>
<script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="../js/jquery.dataTables-1.10.10.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#results').dataTable({
      "bStateSave" : true,
      "iDisplayLength" : 10,
      "lengthMenu" : [ [ 10, 15, 20 ], [ 10, 15, 20 ] ],
      "fnDrawCallback" : function(oSettings) {
        dataTableAddRows(this, oSettings);
      }
    });
  });
</script>
<title><%out.print(LanguageDAO.i18n("reports.parameters"));%></title>
<body>
  <c:set var="isAdmin"><%=request.isUserInRole(Config.DEFAULT_ADMIN_ROLE)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
      <li class="path"><a href="Report"><%out.print(LanguageDAO.i18n("reports"));%></a></li>
      <li class="path"><%out.print(LanguageDAO.i18n("reports.parameters"));%></li>
      </ul>
      <br />
      <div style="width: 60%; margin-left: auto; margin-right: auto;">
        <table id="results" class="results rtl-table">
          <thead>
            <tr>
          <th><%out.print(LanguageDAO.i18n("reports.label"));%></th>
            <th><%out.print(LanguageDAO.i18n("reports.name"));%></th>
            <th><%out.print(LanguageDAO.i18n("reports.width"));%></th>
            <th><%out.print(LanguageDAO.i18n("reports.height"));%></th>
            <th><%out.print(LanguageDAO.i18n("reports.field"));%></th>
            <th><%out.print(LanguageDAO.i18n("reports.other"));%></th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="fe" items="${params}" varStatus="row">
              <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
                <td>${fe.label}</td>
                <td>${fe.name}</td>
                <td>${fe.width}</td>
                <td>${fe.height}</td>
                <td>${fe.field}</td>
                <td>${fe.others}</td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
