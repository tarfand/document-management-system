<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.menu"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.fileVisible}">
            <input name="prf_menu_file_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.edit"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.editVisible}">
            <input name="prf_menu_edit_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_edit_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.tools"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.toolsVisible}">
            <input name="prf_menu_tools_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_tools_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.bookmarks"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.bookmarksVisible}">
            <input name="prf_menu_bookmarks_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_bookmarks_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.template"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.templatesVisible}">
            <input name="prf_menu_templates_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_templates_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.help"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.helpVisible}">
            <input name="prf_menu_help_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_help_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
