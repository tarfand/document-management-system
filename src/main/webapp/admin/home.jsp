<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.api.OKMRepository" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.util.WarUtils" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" href="../css/desktop.css" type="text/css" />
  <link rel="stylesheet" href="css/style.css" type="text/css" />
  <title><%out.print(LanguageDAO.i18n("home"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="home.jsp">نرم افزار مدیریت یکپارچه اسناد و دانش </a>
        </li>
      </ul>
      <br/>
      <table width="500px" class="form" style="margin-top: 25px;text-align: center;font-weight: bold">
        <tr><td><b style="color: #00aa00">نرم افزار مدیریت یکپارچه اسناد و دانش </b></td></tr>
        <tr><td nowrap="nowrap">نسخه: <%=WarUtils.getAppVersion() %></td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td><b>شناسه نصب</b></td></tr>
        <tr><td nowrap="nowrap"><%=OKMRepository.getInstance().getRepositoryUuid(null)%></td></tr>
      </table>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
