<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.menu.menu.file"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.create.folder"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.createFolderVisible}">
            <input name="prf_menu_file_create_folder_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_create_folder_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.find.folder"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.findFolderVisible}">
            <input name="prf_menu_file_find_folder_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_find_folder_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.find.document"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.findDocumentVisible}">
            <input name="prf_menu_file_find_document_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_find_document_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.similar.document"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.similarDocumentVisible}">
            <input name="prf_menu_file_similar_document_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_similar_document_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.go.folder"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.goFolderVisible}">
            <input name="prf_menu_file_go_folder_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_go_folder_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.download"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.downloadVisible}">
            <input name="prf_menu_file_download_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_download_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.download.as.pdf"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.downloadPdfVisible}">
            <input name="prf_menu_file_download_pdf_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_download_pdf_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.add.document"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.addDocumentVisible}">
            <input name="prf_menu_file_add_document_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_add_document_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.purge"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.purgeVisible}">
            <input name="prf_menu_file_purge_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_purge_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.purge.trash"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.purgeTrashVisible}">
            <input name="prf_menu_file_purge_trash_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_purge_trash_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.restore"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.restoreVisible}">
            <input name="prf_menu_file_restore_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_restore_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.start.workflow"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.startWorkflowVisible}">
            <input name="prf_menu_file_start_workflow_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_start_workflow_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.refresh"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.refreshVisible}">
            <input name="prf_menu_file_refresh_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_refresh_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.send.to.zip"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.exportVisible}">
            <input name="prf_menu_file_export_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_export_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.create.from.template"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.createFromTemplateVisible}">
            <input name="prf_menu_file_create_from_template_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_create_from_template_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.send.document.link"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.sendDocumentLinkVisible}">
            <input name="prf_menu_file_send_document_link_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_send_document_link_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.send.document.attachment"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.sendDocumentAttachmentVisible}">
            <input name="prf_menu_file_send_document_attachment_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_send_document_attachment_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.file.forward.email"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfFile.forwardMailVisible}">
            <input name="prf_menu_file_forward_mail_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_file_forward_mail_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
