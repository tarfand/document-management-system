<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>
<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.menu.tool"));%></legend>
  <table>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.tool.languages"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfTool.languagesVisible}">
            <input name="prf_menu_tool_languages_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_tool_languages_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.tool.skin"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfTool.skinVisible}">
            <input name="prf_menu_tool_skin_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_tool_skin_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.tool.debug"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfTool.debugVisible}">
            <input name="prf_menu_tool_debug_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_tool_debug_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.tool.administration"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfTool.administrationVisible}">
            <input name="prf_menu_tool_administration_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_tool_administration_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.tool.preference"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfTool.preferencesVisible}">
            <input name="prf_menu_tool_preferences_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_tool_preferences_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td nowrap><%out.print(LanguageDAO.i18n("profile.menu.tool.omr"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfTool.omrVisible}">
            <input name="prf_menu_tool_omr_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_tool_omr_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.menu.tool.convert"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfMenu.prfTool.convertVisible}">
            <input name="prf_menu_tool_convert_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_menu_tool_convert_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
