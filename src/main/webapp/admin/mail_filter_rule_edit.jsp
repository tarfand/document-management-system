<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="../css/chosen.css"/>
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="../js/vanadium-min.js"></script>
  <script type="text/javascript" src="../js/chosen.jquery.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('select#mfr_field').chosen({disable_search_threshold: 10});
      $('select#mfr_operation').chosen({disable_search_threshold: 10});
    });
  </script>
  <title><%out.print(LanguageDAO.i18n("email.filter.rules"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <c:url value="MailAccount" var="urlMailAccountList">
        <c:param name="ma_user" value="${ma_user}"/>
      </c:url>
      <c:url value="MailAccount" var="urlMailFilterList">
        <c:param name="action" value="filterList"/>
        <c:param name="ma_user" value="${ma_user}"/>
        <c:param name="ma_id" value="${ma_id}"/>
      </c:url>
      <c:url value="MailAccount" var="urlMailFilterRuleList">
        <c:param name="action" value="filterRuleList"/>
        <c:param name="ma_user" value="${ma_user}"/>
        <c:param name="ma_id" value="${ma_id}"/>
        <c:param name="mf_id" value="${mf_id}"/>
      </c:url>
      <ul id="breadcrumb">
        <li class="path">
          <a href="Auth"><%out.print(LanguageDAO.i18n("email.filter.users.list"));%></a>
        </li>
        <li class="path">
          <a href="${urlMailAccountList}"><%out.print(LanguageDAO.i18n("email.filter.email.accounts"));%></a>
        </li>
        <li class="path">
          <a href="${urlMailFilterList}"><%out.print(LanguageDAO.i18n("email.filter"));%></a>
        </li>
        <li class="path">
          <a href="${urlMailFilterRuleList}"><%out.print(LanguageDAO.i18n("email.filter.rules"));%></a>
        </li>
        <li class="path">
          <c:choose>
            <c:when test="${action == 'ruleCreate'}"><%out.print(LanguageDAO.i18n("email.filter.rules.create"));%></c:when>
            <c:when test="${action == 'ruleEdit'}"><%out.print(LanguageDAO.i18n("email.filter.rules.edit"));%></c:when>
            <c:when test="${action == 'ruleDelete'}"><%out.print(LanguageDAO.i18n("email.filter.rules.delete"));%></c:when>
          </c:choose>
        </li>
      </ul>
      <br/>
      <form action="MailAccount" id="form">
        <input type="hidden" name="action" id="action" value="${action}"/>
        <input type="hidden" name="persist" value="${persist}"/>
        <input type="hidden" name="ma_id" value="${ma_id}"/>
        <input type="hidden" name="mf_id" value="${mf_id}"/>
        <input type="hidden" name="mfr_id" value="${mfr.id}"/>
      <table class="form rtl-table" width="345px" align="center">
          <tr>
            <td nowrap="nowrap"><%out.print(LanguageDAO.i18n("email.filter.rules.field"));%></td>
            <td>
              <select name="mfr_field" id="mfr_field" style="width: 85px">
                <c:forEach var="fld" items="${fields}">
                  <c:choose>
                    <c:when test="${fld == mfr.field}">
                      <option value="${fld}" selected="selected">${fld}</option>
                    </c:when>
                    <c:otherwise>
                      <option value="${fld}">${fld}</option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </select>
            </td>
          </tr>
          <tr>
            <td nowrap="nowrap"><%out.print(LanguageDAO.i18n("email.filter.rules.operation"));%></td>
            <td>
              <select name="mfr_operation" id="mfr_operation" style="width: 100px">
                <c:forEach var="ope" items="${operations}">
                  <c:choose>
                    <c:when test="${ope == mfr.operation}">
                      <option value="${ope}" selected="selected">${ope}</option>
                    </c:when>
                    <c:otherwise>
                      <option value="${ope}">${ope}</option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </select>
            </td>
          </tr>
          <tr>
            <td nowrap="nowrap"><%out.print(LanguageDAO.i18n("email.filter.rules.value"));%></td>
            <td><input name="mfr_value" value="${mfr.value}"/></td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("email.filter.rules.active"));%></td>
            <td>
              <c:choose>
                <c:when test="${mfr.active}">
                  <input name="mfr_active" type="checkbox" checked="checked"/>
                </c:when>
                <c:otherwise>
                  <input name="mfr_active" type="checkbox"/>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <c:choose>
              <c:when test="${action == 'ruleCreate'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("email.filter.rules.create"));%>" class="yesButton"/></c:when>
              <c:when test="${action == 'ruleEdit'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("email.filter.rules.edit"));%>" class="yesButton"/></c:when>
              <c:when test="${action == 'ruleDelete'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("email.filter.rules.delete"));%>" class="yesButton"/></c:when>
              </c:choose>
            <input type="button" onclick="javascript:window.history.back()" value="<%out.print(LanguageDAO.i18n("email.filter.rules.cancel"));%>" class="noButton"/>

            </td>
          </tr>
        </table>
      </form>
      <br/>
      <div class="warn" style="text-align: center;" id="dest"></div>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
