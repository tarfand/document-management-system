<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page pageEncoding="utf-8"%>

<fieldset>
  <legend><%out.print(LanguageDAO.i18n("profile.tabs"));%></legend>
  <table>
  <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.default.tab"));%></td>
      <td>
        <select name="prf_tab_default" id="prf_tab_default" data-placeholder="Select tab" style="width: 100px">
          <c:choose>
            <c:when test="${prf.prfTab.defaultTab == 'desktop'}">
              <option value="desktop" selected="selected"><%out.print(LanguageDAO.i18n("profile.tabs.desktop"));%></option>
            </c:when>
            <c:otherwise><option value="desktop"><%out.print(LanguageDAO.i18n("profile.tabs.desktop"));%></option></c:otherwise>
          </c:choose>
          <c:choose>
            <c:when test="${prf.prfTab.defaultTab == 'search'}">
              <option value="search" selected="selected"><%out.print(LanguageDAO.i18n("profile.tabs.search"));%></option>
            </c:when>
            <c:otherwise><option value="search"><%out.print(LanguageDAO.i18n("profile.tabs.search"));%></option></c:otherwise>
          </c:choose>
          <c:choose>
            <c:when test="${prf.prfTab.defaultTab == 'dashboard'}">
              <option value="dashboard" selected="selected"><%out.print(LanguageDAO.i18n("profile.tabs.dashboard"));%></option>
            </c:when>
            <c:otherwise><option value="dashboard"><%out.print(LanguageDAO.i18n("profile.tabs.dashboard"));%></option></c:otherwise>
          </c:choose>
          <c:choose>
            <c:when test="${prf.prfTab.defaultTab == 'administration'}">
              <option value="administration" selected="selected"><%out.print(LanguageDAO.i18n("profile.tabs.administration"));%></option>
            </c:when>
            <c:otherwise><option value="administration"><%out.print(LanguageDAO.i18n("profile.tabs.administration"));%></option></c:otherwise>
          </c:choose>
        </select>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.desktop"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.desktopVisible}">
            <input name="prf_tab_desktop_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_desktop_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.search"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.searchVisible}">
            <input name="prf_tab_search_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_search_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.dashboard"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.dashboardVisible}">
            <input name="prf_tab_dashboard_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_dashboard_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <td><%out.print(LanguageDAO.i18n("profile.tabs.administration"));%></td>
      <td>
        <c:choose>
          <c:when test="${prf.prfTab.administrationVisible}">
            <input name="prf_tab_administration_visible" type="checkbox" checked="checked"/>
          </c:when>
          <c:otherwise>
            <input name="prf_tab_administration_visible" type="checkbox"/>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
</fieldset>
