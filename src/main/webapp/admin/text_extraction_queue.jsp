<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.servlet.admin.BaseServlet" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.openkm.com/tags/utils" prefix="u" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" href="css/style.css" type="text/css" />
  <link rel="stylesheet" type="text/css" href="css/fixedTableHeader.css" />
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="js/fixedTableHeader.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
          $('#results').dataTable({
              "bStateSave": true,
              "iDisplayLength": 10,
              "lengthMenu": [[10, 15, 20], [10, 15, 20]],
              "fnDrawCallback": function (oSettings) {
                  dataTableAddRows(this, oSettings);
              }
          });
          $('#trick').dataTable({
              "bStateSave": true,
              "iDisplayLength": 10,
              "lengthMenu": [[10, 15, 20], [10, 15, 20]],
              "fnDrawCallback": function (oSettings) {
                  dataTableAddRows(this, oSettings);
              }
          });


	});
  </script>
  <title><%out.print(LanguageDAO.i18n("management.text.extraction.queue"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=BaseServlet.isAdmin(request)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
        <a href="TextExtractionQueue"><%out.print(LanguageDAO.i18n("management.text.extraction.queue"));%></a>
        </li>
        <li class="action">
          <a href="stats.jsp">
            <img src="img/action/generic.png" alt="Generic" title="Generic" style="vertical-align: middle;"/>
          <%out.print(LanguageDAO.i18n("management.text.extraction.queue.statistics"));%>
          </a>
        </li>
        <li class="action">
          <a href="TextExtractionQueue">
          <img src="img/action/refresh.png" alt="Refresh" title="<%out.print(LanguageDAO.i18n("management.text.extraction.queue.refresh"));%>" style="vertical-align: middle;"/>
          <%out.print(LanguageDAO.i18n("management.text.extraction.queue.refresh"));%>
          </a>
        </li>
      </ul>
      <br/>
    <div style="width: 80%; margin-left: auto; margin-right: auto;">
      <table id="results" class="results">
        <thead>
          <tr class="fuzzy">
            <td colspan="4" style="text-align: center; font-weight: bold; font-size: 14px">
            <%out.print(LanguageDAO.i18n("management.text.extraction.queue.extractions.in.process"));%>
              <c:if test="${inProgress}">
                <span style="text-align: center; font-weight: bold; font-size: 12px">
                  (Running)
                </span>
              </c:if>
            </td>
          </tr>
        <tr><th>#</th><th><%out.print(LanguageDAO.i18n("management.text.extraction.queue.id"));%></th><th><%out.print(LanguageDAO.i18n("management.text.extraction.queue.path"));%></th><th><%out.print(LanguageDAO.i18n("management.text.extraction.queue.date"));%></th></tr>
        </thead>
        <tbody>
          <c:forEach var="work" items="${inProgressWorks}" varStatus="row">
            <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
              <td>${row.index + 1}</td>
              <td nowrap="nowrap">${work.docUuid}</td>
              <td>${work.docPath}</td>
            <td nowrap="nowrap"><u:formatDate persian="true"  calendar="${work.date}"/></td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>
      <br/>
    <div style="width: 80%; margin-left: auto; margin-right: auto;">
      <table id="trick" class="results" width="90%">
        <thead>
          <tr class="fuzzy">
            <td colspan="4" style="text-align: center; font-weight: bold; font-size: 14px">
            <%out.print(LanguageDAO.i18n("management.text.extraction.queue.pending.extractions"));%>
              <c:if test="${!inProgress}">
                <span style="text-align: center; font-weight: bold; font-size: 12px">
                  (آخرین اجرا:
                  <c:choose>
                    <c:when test="${lastExecution != null}">
                      <u:formatDate persian="true" calendar="${lastExecution}"/>
                    </c:when>
                    <c:otherwise>
                     نامشخص
                    </c:otherwise>
                  </c:choose>
                  )
                </span>
              </c:if>
            </td>
          </tr>
        <tr><th>#</th><th><%out.print(LanguageDAO.i18n("management.text.extraction.queue.id"));%></th><th><%out.print(LanguageDAO.i18n("management.text.extraction.queue.path"));%></th><th><%out.print(LanguageDAO.i18n("management.text.extraction.queue.date"));%></th></tr>
        </thead>
        <tbody>
          <c:forEach var="work" items="${pendingWorks}" varStatus="row">
            <tr class="${row.index % 2 == 0 ? 'even' : 'odd'}">
              <td>${row.index + 1}</td>
              <td nowrap="nowrap">${work.docUuid}</td>
              <td>${work.docPath}</td>
            <td nowrap="nowrap"><u:formatDate persian="true"  calendar="${work.date}"/></td>
            </tr>
          </c:forEach>
          <c:if test="${pendingSize > 0}">
            <tr class="fuzzy">
              <td colspan="4" style="text-align: center; font-weight: bold;">
              تعداد کل اسنادی که باید استخراج متن شوند: ${pendingSize}
              </td>
            </tr>
          </c:if>
        </tbody>
      </table>
    </div>
    </c:when>
    <c:otherwise>
    <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
