<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page import="com.openkm.core.Config" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="Shortcut icon" href="favicon.ico" />
  <link rel="stylesheet" type="text/css" href="css/admin-style.css" />
  <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
  <script src="../js/vanadium-min.js" type="text/javascript"></script>
  <title><%out.print(LanguageDAO.i18n("reports"));%></title>
</head>
<body>
  <c:set var="isAdmin"><%=request.isUserInRole(Config.DEFAULT_ADMIN_ROLE)%></c:set>
  <c:choose>
    <c:when test="${isAdmin}">
      <ul id="breadcrumb">
        <li class="path">
          <a href="Report"><%out.print(LanguageDAO.i18n("reports"));%></a>
        </li>
        <li class="path">
          <c:choose>
            <c:when test="${action == 'create'}"><%out.print(LanguageDAO.i18n("reports.create"));%></c:when>
            <c:when test="${action == 'edit'}"><%out.print(LanguageDAO.i18n("reports.edit"));%></c:when>
            <c:when test="${action == 'delete'}"><%out.print(LanguageDAO.i18n("reports.delete"));%></c:when>
          </c:choose>
        </li>
      </ul>
      <br/>
      <form action="Report" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="${action}"/>
        <input type="hidden" name="rp_id" value="${rp.id}"/>
        <table class="form rtl-table" width="425px">
          <tr>
            <td nowrap="nowrap"><%out.print(LanguageDAO.i18n("reports.name"));%></td>
            <td><input class=":required :only_on_blur" name="rp_name" value="${rp.name}"/></td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("reports.active"));%></td>
            <td>
              <c:choose>
                <c:when test="${rp.active}">
                  <input name="rp_active" type="checkbox" checked="checked"/>
                </c:when>
                <c:otherwise>
                  <input name="rp_active" type="checkbox"/>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td><%out.print(LanguageDAO.i18n("reports.file"));%></td>
            <td>
              <c:choose>
                <c:when test="${action == 'create'}">
                  <input class=":required :only_on_blur" type="file" name="file"/>
                </c:when>
                <c:otherwise>
                  <input type="file" name="file"/>
                </c:otherwise>
              </c:choose>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <input type="button" onclick="javascript:window.history.back()" value="<%out.print(LanguageDAO.i18n("reports.cancel"));%>" class="noButton"/>
              <c:choose>
                <c:when test="${action == 'create'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("reports.create"));%>" class="yesButton"/></c:when>
                <c:when test="${action == 'edit'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("reports.edit"));%>" class="yesButton"/></c:when>
                <c:when test="${action == 'delete'}"><input type="submit" value="<%out.print(LanguageDAO.i18n("reports.delete"));%>" class="yesButton"/></c:when>
              </c:choose>
            </td>
          </tr>
        </table>
      </form>
    </c:when>
    <c:otherwise>
      <div class="error"><h3><%out.print(LanguageDAO.i18n("error.access.by.admins"));%></h3></div>
    </c:otherwise>
  </c:choose>
</body>
</html>
