<%@ page isErrorPage="true" %>
<%@ page import="com.openkm.frontend.client.OKMException" %>
<%@ page import="com.openkm.util.FormatUtil" %>
<%@ page import="bsh.TargetError" %>
<%@ page import="com.openkm.dao.LanguageDAO" %>
<%@ page import="com.openkm.util.OKMDateUtil" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <style type="text/css">
    body {
      margin: 10px;
      background-color: #E5E5E1;
      color: #333333;
      font-size: 12px;
    }

    textarea, input, select, button {
      border: 1px solid #A5A596;
      font-size: 11px;
    }
  </style>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title><%=LanguageDAO.i18n("error.page.title")%>
  </title>
</head>
<body style="direction: rtl; text-align: right; font-family: 'B Nazanin',tahoma, Arial, Helvetica, sans-serif">
<table border="0" width="100%" align="center" style="padding-top: 125px">
  <tr>
    <td align="center">
      <table>
        <tr>
          <td colspan="2" align="center" style="padding-top: 25px;">
            <h2><%=LanguageDAO.i18n("error.page.application.error")%>
            </h2>
          </td>
        </tr>
        <%--    <tr>
              <td><b>Class:</b></td>
              <td><%=exception.getClass().getName() %></td>
            </tr>--%>
        <% if (exception instanceof OKMException) { %>
        <tr>
          <td><b><%=LanguageDAO.i18n("error.page.code")%>:</b></td>
          <td><%=LanguageDAO.i18n(((OKMException) exception).getCode()) %></td>
        </tr>
        <tr>
          <td><b><%=LanguageDAO.i18n("error.page.message")%>:</b></td>
          <td><%=FormatUtil.escapeHtml((exception).getMessage()) %>
          </td>
        </tr>
        <%--  <% } else if (exception instanceof TargetError) { %>
          <tr>
            <td><b>Text:</b></td>
            <td><%=((TargetError) exception).getErrorText() %></td>
          </tr>
          <tr>
            <td><b>Source:</b></td>
            <td><%=((TargetError) exception).getErrorSourceFile() %></td>
          </tr>
          <tr>
            <td><b>Line:</b></td>
            <td><%=((TargetError) exception).getErrorLineNumber() %></td>
          </tr>--%>
        <% } else { %>
        <tr>
          <td><b><%=LanguageDAO.i18n("error.page.message")%>:</b></td>
          <td><%=exception.getMessage() %>
          </td>
        </tr>
        <% } %>
        <tr>
          <td><b><%=LanguageDAO.i18n("error.page.application.date")%>:</b></td>
          <td><%= OKMDateUtil.gregorianToJalali(new java.util.Date()) %>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center"><input type="button"
                                                value="<%=LanguageDAO.i18n("error.page.return")%>"
                                                onclick="javascript:history.go(-1)"/></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
