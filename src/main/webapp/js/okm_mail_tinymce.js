function drawMailEditor(t_language, t_theme, t_skin, t_skin_variant, t_plugins, t_buttons1, t_buttons2,
		t_buttons3, t_buttons4, sendMailText, cancelText, searchDocumentText, searchFolderText, searchImageText) {
	var params = '{';
	params = params + '"script_url":"../js/tinymce3/tiny_mce.js"';
	params = params + ',"language":"' + t_language + '"';
	params = params + ',"theme":"' + t_theme + '"';
	params = params + ',"plugins":"' + t_plugins + '"';
	params = params + ',"theme_advanced_buttons1":"' + t_buttons1 + '"';
	params = params + ',"theme_advanced_buttons2":"' + t_buttons2 + '"';
	params = params + ',"theme_advanced_buttons3":"' + t_buttons3 + '"';
	params = params + ',"theme_advanced_buttons4":"' + t_buttons4 + '"';
	params = params + ',"extended_valid_elements":"pre[id|name|class]"';
	params = params + ',"remove_linebreaks":"true"';
	params = params + ',"theme_advanced_toolbar_location":"top"';
	params = params + ',"theme_advanced_toolbar_align":"left"';
	params = params + ',"theme_advanced_statusbar_location":"bottom"';
	params = params + ',"theme_advanced_resizing":"false"';
	params = params + ',"theme_advanced_resizing_use_cookie":"false"';
	params = params + ',"content_css":"../style/extension/htmlEditor"';
	params = params + ',"template_external_list_url":"lists/template_list.js"';
	params = params + ',"external_link_list_url":"lists/link_list.js"';
	params = params + ',"external_image_list_url":"lists/image_list.js"';
	params = params + ',"media_external_list_url":"lists/media_list.js"';
	params = params + ',"template_replace_values" : {"username" : "Some User","staffid" : "991234"}';
	
	if (t_skin != '') {
		params = params + ',"skin":"' + t_skin + '"';
		
		if (t_skin_variant != '') {
			params = params + ',"skin_variant":"' + t_skin_variant + '"';
		}
	}
	
	params = params +'}'; 
	var json = $.parseJSON(params); // create json object from string value
	var jsonFnObj = {
			setup : function (ed) {
				// Add a custom buttons
				ed.addButton('okm_sendMail', {
					title : sendMailText,
					image : '../img/tinymce/email_go.png',
					onclick : function() {
						jsSendMail(tinyMCE.get('okm_mail_tinymce').getContent());
					}
				});
				
				ed.addButton('okm_cancelSendMail', {
					title : cancelText,
					image : '../img/tinymce/cancel.png',
					onclick : function() {
						cancelMailEditor();
					}
				});
				
				ed.addButton('okm_searchDocument', {
					title : searchDocumentText,
					image : '../img/tinymce/document_find.gif',
					onclick : function() {						
						jsSearchDocumentPopup('3');
					}
				});
				
				ed.addButton('okm_searchFolder', {
					title : searchFolderText,
					image : '../img/tinymce/folder_find.gif',
					onclick : function() {
						jsSearchFolderPopup('2');
					}
				});				
			}
	};
    
    $.extend(json, jsonFnObj); // Extend json with function objects
    $('textarea.tinymce').tinymce(json);
}

function addDocumentToMailEditor(url, name) {
	var docLink = '<a href="'+url+'">'+name+'</a>';
	tinyMCE.execCommand('mceInsertContent', false, docLink);
	return false;
}

function addFolderToMailEditor(url, name) {
	var docLink = '<a href="'+url+'">'+name+'</a>';
	tinyMCE.execCommand('mceInsertContent', false, docLink);
	return false;
}

function cancelMailEditor(text) {
	// Displays an confirm box and an alert message will be displayed depending on what you choose in the confirm
	var text = jsI18n('maileditor.cancel');
	tinyMCE.activeEditor.windowManager.confirm(text, function(s) {
	   if (s) {
		   jsHideMailEditorPopup();
	   }
	});
}