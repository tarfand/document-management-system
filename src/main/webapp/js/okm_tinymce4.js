function drawHTMLEditor( content,language,theme,plugins,toolbar1,toolbar2,checkinText,cancelCheckoutText,searchDocumentText,searchFolderText,searchImageText) {
    document.getElementById('htmlEditor4').contentWindow.drawHTMLEditor4(content,language,theme,plugins,toolbar1,toolbar2,checkinText,cancelCheckoutText,searchDocumentText,searchFolderText,searchImageText);
}

function confirmCancelCheckout() {
    document.getElementById('htmlEditor4').contentWindow.confirmCancelCheckout4();
}

function getContentHTMLEditor() {
    return document.getElementById('htmlEditor4').contentWindow.getContentHTMLEditor();
}

function setContentHTMLEditor(content) {
    document.getElementById('htmlEditor4').contentWindow.setContentHTMLEditor(content);
}

function addDocumentHTMLEditor(uuid, name) {
    document.getElementById('htmlEditor4').contentWindow.addDocumentHTMLEditor(uuid, name);
    return false;
}

function addFolderHTMLEditor(uuid, name) {
    document.getElementById('htmlEditor4').contentWindow.addFolderHTMLEditor(uuid, name);
    return false;
}

function addImageHTMLEditor(src, params) {
    document.getElementById('htmlEditor4').contentWindow.addImageHTMLEditor(src, params);
    return false;
}

function addDownloadDocumentHTMLEditor(uuid, name) {
    document.getElementById('htmlEditor4').contentWindow.addDownloadDocumentHTMLEditor4(uuid, name);
    return false;
}
